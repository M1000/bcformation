/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(813);
	module.exports = __webpack_require__(2675);


/***/ },

/***/ 627:
/***/ function(module, exports) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var Cookie = /** @class */ (function () {
	    function Cookie() {
	    }
	    Cookie.get = function (name) {
	        var data = ("; " + document.cookie).split("; " + name + "=");
	        return data && data.length === 2 ? ((data.pop() || '').split(';').shift()) : (null);
	    };
	    Cookie.set = function (name, value, expires) {
	        document.cookie = name + "=" + value + "; expires=" + expires.toUTCString() + "; path=/";
	    };
	    return Cookie;
	}());
	exports.Cookie = Cookie;
	//# sourceMappingURL=Cookie.js.map

/***/ },

/***/ 813:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var Cookie_1 = __webpack_require__(627);
	exports.Cookie = Cookie_1.Cookie;
	var CKies_1 = __webpack_require__(885);
	exports.ckies = CKies_1.CKies;
	exports.CookieOptions = CKies_1.CookieOptions;
	exports.CookieType = CKies_1.CookieType;
	//# sourceMappingURL=index.js.map

/***/ },

/***/ 885:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	Object.defineProperty(exports, "__esModule", { value: true });
	var Cookie_1 = __webpack_require__(627);
	var CookieType;
	(function (CookieType) {
	    CookieType["NECESSARY"] = "necessary";
	    CookieType["FUNCTIONAL"] = "functional";
	    CookieType["PERFORMANCE"] = "performance";
	    CookieType["MARKETING"] = "marketing";
	})(CookieType = exports.CookieType || (exports.CookieType = {}));
	var CookieOptions;
	(function (CookieOptions) {
	    CookieOptions["ALLOW"] = "allow";
	    CookieOptions["DENY"] = "deny";
	})(CookieOptions = exports.CookieOptions || (exports.CookieOptions = {}));
	exports.CONFIG_EXPIRATION = 365 * 24 * 60 * 60 * 1000;
	var CKies = /** @class */ (function () {
	    function CKies() {
	    }
	    // Get expiration date for new cookies
	    CKies.getExpireDate = function () {
	        var date = new Date();
	        date.setTime(date.getTime() + exports.CONFIG_EXPIRATION);
	        return date;
	    };
	    // Get cookie name for CookieType
	    CKies.key = function (type) {
	        return "ckies_" + type;
	    };
	    // Check if CookieType should be used
	    CKies.use = function (type) {
	        if (type === CookieType.NECESSARY) {
	            return true;
	        }
	        return this.isOptIn() ? (Cookie_1.Cookie.get(this.key(type)) === CookieOptions.ALLOW) : (Cookie_1.Cookie.get(this.key(type)) !== CookieOptions.DENY);
	    };
	    // Deny usage for CookieType
	    CKies.deny = function (type) {
	        this.set(type, CookieOptions.DENY);
	    };
	    // Allow usage for CookieType
	    CKies.allow = function (type) {
	        this.set(type, CookieOptions.ALLOW);
	    };
	    // Wrapper to check if necesserary cookies can be used
	    CKies.useNecessary = function () {
	        return this.use(CookieType.NECESSARY);
	    };
	    // Wrapper to check if functional cookies can be used
	    CKies.useFunctional = function () {
	        return this.use(CookieType.FUNCTIONAL);
	    };
	    // Wrapper to check if performance cookies can be used
	    CKies.usePerformance = function () {
	        return this.use(CookieType.PERFORMANCE);
	    };
	    // Wrapper to check if marketing cookies can be used
	    CKies.useMarketing = function () {
	        return this.use(CookieType.MARKETING);
	    };
	    // Set cookie for CookieType
	    CKies.set = function (type, option) {
	        if (type !== CookieType.NECESSARY) {
	            Cookie_1.Cookie.set(this.key(type), option, this.getExpireDate());
	        }
	    };
	    // Check if mode is OPT_IN
	    CKies.isOptIn = function () {
	        /* tslint:disable */
	        return window.hasOwnProperty('CKIES_OPTIN') && window['CKIES_OPTIN'] === true;
	        /* tslint:enable */
	    };
	    return CKies;
	}());
	exports.CKies = CKies;
	//# sourceMappingURL=CKies.js.map

/***/ },

/***/ 2675:
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _library = __webpack_require__(813);
	
	window.ckies = _library.ckies;

/***/ }

/******/ });


/** WEBPACK FOOTER **
 ** ckies.js.2f2ba40710ea5042aa2f.js
 **/