/*** IMPORTS FROM imports-loader ***/
(function() {

/**
 * A lightweight classical inheritance emulation
 *
 * @version 1.3
 * @credits http://www.crockford.com/javascript/inheritance.html
 *          http://webreflection.blogspot.com/2009/02/on-javascript-inheritance-performance_28.html
 *          http://webreflection.blogspot.com/2010/02/javascript-override-patterns.html
 */

"use strict";

var slice = [].slice;
var noop = function(){};

function Class(_class) {

    /**
        * Create instance from class definition, call init function
        * @return {Object} inst
        */
    function instantiate() {
        var args = slice.call(arguments, 0),
            inst = null;
        // call all superclasses definitions
        for (var i = 0, l = _class.classes.length; i < l; ++i) {
            if (inst) {
                noop.prototype = inst;
                // add undefined array member at the 0 position only once
                if (i === 1) {
                    args.unshift(undefined);
                }
                // every subclass has super instance as first argument,
                // create new super object from current instance
                args[0] = new noop();
                // create new instance object
                inst = new noop();
                // if inst is not set, this is the first class we instantiate
            } else {
                inst = {};
            }
            _class.classes[i].apply(inst, args);
        }

        if (typeof inst.init === 'function') {
            inst.init();
        }

        return inst;
    }

    instantiate.extend = function extend(subclass) {
        var instantiate = Class(subclass);
        // merge classes array of superclass with classes array of subclass
        subclass.classes.unshift.apply(subclass.classes, _class.classes);
        return instantiate;
    };

    // add classes array to the class definition, this array we will pass by instantiation
    _class.classes = [_class];

    return instantiate;
}

module.exports = Class;


}.call(window));


/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/libs/Class/Class.js
 ** module id = 480
 ** module chunks = 96 97 229 232 234 235 241
 **/