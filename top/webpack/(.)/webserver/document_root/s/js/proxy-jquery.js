var jimdoData = require('jimdoData');
var $;

function shouldLoadJQueryCms() {
    if (jimdoData && jimdoData.isCMS) {
        try {
            require('jquery-cms');
            return true;
        } catch (e) {
            return false;
        }
    }

    return false;
}

if (shouldLoadJQueryCms()) {
    $ = require('jquery-cms');
} else {
    $ = require('jquery-web');
}

$.migrateMute = true;
module.exports = $;
require('jquery-migrate');



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/proxy-jquery.js
 ** module id = 2
 ** module chunks = 96 97 99 100 228 229 236 238 239 240 243
 **/