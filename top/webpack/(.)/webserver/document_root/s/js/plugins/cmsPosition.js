'use strict';

var $ = require('jquery');
var $doc, $body, $inner, paddingTop, paddingRight,

    refs = {top: [], right: []},
    validAligns = ['top', 'right'],
    tries = 0,
    lastHeight = 0,

    RECHECK_SPEED = 10,
    MAX_RECHECKS = 20;

function init() {
    $doc = $(document);
    $body = $('body');
    $inner = $('#cc-inner');

    paddingTop = parseInt($inner.css('paddingTop'), 10);
    paddingRight = parseInt($inner.css('paddingRight'), 10);
}


function handleTop(noScrolling) {
    var height = getHeightOfTallestReference();

    if (!$body) {
        init();
    }

    $inner.css({paddingTop: paddingTop + height});

    if (!noScrolling) {
        $doc.scrollTop($doc.scrollTop() + (height - lastHeight));
    }

    lastHeight = height;

    triggerEvent({top: height});

    // References may change their height using CSS transitions. Check a maximum of 10 times for height
    // differences to mimic the animation
    window.setTimeout(function() {
        if (height === getHeightOfTallestReference()) {
            tries++;
        } else {
            tries = 0;
        }

        if (tries < MAX_RECHECKS) {
            handleTop(noScrolling);
        } else {
            tries = 0;
        }
    }, RECHECK_SPEED);
}

function handleRight() {
    var width = getWidthOfWidestReference();

    if (!$body) {
        init();
    }

    $inner.css({paddingRight: paddingRight + width});

    triggerEvent({right: width});

    // References may change their width using CSS transitions. Check a maximum of 10 times for width
    // differences to mimic the animation
    window.setTimeout(function() {
        if (width === getWidthOfWidestReference()) {
            tries++;
        } else {
            tries = 0;
        }

        if (tries < MAX_RECHECKS) {
            handleRight();
        } else {
            tries = 0;
        }
    }, RECHECK_SPEED);
}

function getHeightOfTallestReference() {
    var current = 0,
        tallest = 0;

    $.each(refs.top, function(_, $reference) {
        if (!$reference.is(':visible')) {
            return true;
        }

        current = $reference.outerHeight() + parseInt($reference.css('top'), 10);

        if (current > tallest) {
            tallest = current;
        }
    });

    return tallest;
}

function getWidthOfWidestReference() {
    var current = 0,
        widest = 0;

    $.each(refs.right, function(_, $reference) {
        if (!$reference.is(':visible')) {
            return true;
        }

        current = $reference.outerWidth() + parseInt($reference.css('right'), 10);

        if (current > widest) {
            widest = current;
        }
    });

    return widest;
}

function triggerEvent(data) {
    $(window).trigger('cmsPosition', [data]);
}

module.exports = {
    add: function add($reference, align) {
        if (!align) {
            align = 'top';
        }

        if ($.inArray(align, validAligns) === -1) {
            throw new Error('invalid direction: ' + align);
        }

        refs[align].push($reference);

        return this;
    },
    adjust: function adjust(noScrolling, align) {
        if (!align) {
            align = 'top';
        }

        if ($.inArray(align, validAligns) === -1) {
            throw new Error('invalid direction: ' + align);
        }

        switch (align) {
            case 'top':
                handleTop(noScrolling);
                break;

            case 'right':
                handleRight();
        }

        return this;
    }
};



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/plugins/cmsPosition.js
 ** module id = 586
 ** module chunks = 1 2 96 97 232 234
 **/