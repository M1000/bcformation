(function($, slice) {
    "use strict";

    $.fn.actionController = function(controller, options) {
        return this.each(function() {

            var instance = $.data(this, 'actionController');

            if (typeof controller !== 'string') {
                if (instance) {
                    instance.destroy();
                }

                $.data(this, 'actionController', new ActionController($(this), controller, options));
            } else {
                if (instance) {
                    instance[controller](options);
                }
            }
        });
    };

    $.fn.actionController.defaults = {
        actionPrefix: '_',
        actionAttr: 'data-action',
        paramsAttr: 'data-params',
        defaultAction: 'action',
        editEventAttr: 'editAction',
        editEvents: 'click',
        events: 'click',
        handleEvent: null,
        context: 'element' // controller || element
    };

    function ActionController($container, controller, options) {
        this.enabled = true;
        this.$container = $container;
        this.controller = controller;
        var s = this.settings = $.extend({}, $.fn.actionController.defaults, options);

        $container.on(s.events, '[' + s.actionAttr + ']',  $.proxy(this, 'handler'));
    }

    ActionController.prototype = {
        destroy: function() {
            $(this.$container).off(this.events, '[' + this.settings.actionAttr + ']', this.handler);
            this.$container.removeData('actionController');
        },

        disable: function() {
            this.enabled = false;
        },

        enable: function() {
            this.enabled = true;
        },

        handler: function(e) {
            if (!this.enabled) {
                return;
            }
            var s = this.settings,
                $target = $(e.currentTarget),
                args = arguments,
                params = $target.attr(s.paramsAttr),
                actionSuffix = e.type.substr(0, 1).toUpperCase() + e.type.substr(1),
                actionName = s.actionPrefix + $.trim($target.attr(s.actionAttr)) + actionSuffix,
                action = this.controller[actionName],
                defaultAction = this.controller[s.actionPrefix + s.defaultAction + actionSuffix],
                context = s.context === 'controller' ? this.controller : $target[0];

            if (params) {
                params = $.map(params.split(','), function(param) {
                    return $.trim(param);
                });
                args = slice.call(args, 0).concat(params);
            }

            if (defaultAction && $.isFunction(defaultAction) && defaultAction.apply(context, args) === false) {
                return false;
            }

            if (action && $.isFunction(action)) {
                if ($.isFunction(s.handleEvent)) {
                    try {
                        s.handleEvent.call(this, e);
                    } catch(ex) {}
                }
                return action.apply(context, args);
            }
        }
    };

})(require('jquery'), Array.prototype.slice);



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/plugins/actionController/jquery.actionController.js
 ** module id = 146
 ** module chunks = 96 97 232 234 235 241
 **/