/*
 * flash 1.3 - Plugin for jQuery
 *
 * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
 * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
 *
 * Depends:
 *   jquery.js
 *
 *  Copyright (c) 2010 Oleg Slobodskoi (jsui.de)
 */

/* global ActiveXObject */
;(function($){
    "use strict";

$.fn.flash = function( method, options ) {
    if ( typeof method !== 'string' ) {
        options = method;
        method = 'init';
    }

    var ret = null;
    this.each(function(){
        var instance = $.data(this, 'flash') || $.data( this, 'flash', flash( this, options) );
        ret = instance[method](options);
    });
    return ret || this;
};

var timestamp =  (new Date()).getTime();

$.fn.flash.defaults = {
    swf: null,
    version: '8.0.0',
    params: {
        allowfullscreen: true,
        allowscriptaccess: 'always',
        quality: 'best',
        wmode: 'transparent',
        bgcolor: 'transparent',
        flashvars: null,
        menu: false
    },
    attr: {
        type: 'application/x-shockwave-flash',
        width: 'auto',
        height: 'auto'
    },
    error: $.noop
};

function flash( elem, options ) {

    var self = {},
        s = $.extend(true, {}, $.fn.flash.defaults, options),
        $originContent,
        flashElem;

    self.init = function() {
        if ( !detectVersion( s.version ) ) {
            return s.error.call(this, 'wrong flash version');
        }

        // id is needed by ie if using external interface
        if(!s.attr.id) {
            s.attr.id = 'flash-' + timestamp++;
        }


        //serialize flashvars if object is given
        if($.isPlainObject(s.params.flashvars)) {
            s.params.flashvars = $.param(s.params.flashvars);
        }

        // save original html
        $originContent = $(elem).html();

        var flash = '';
        if ($.browser.msie) {
            s.params.movie = s.swf;
            //create param elements
            $.each(s.params, function(name, val){
                if(val) {
                    flash+='<param name="'+name+'" value="'+val+'"/>';
                }
            });
            flash = '<object '+ toAttr(s.attr) +' classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000">'+flash+'</object>';
        } else {
            s.attr.src = s.swf;
            flash = '<embed' + toAttr(s.params) + toAttr(s.attr) + '/>';
        }

        elem.innerHTML = flash;
        flashElem = elem.childNodes[0];
    };

    self.destroy = function() {
        $(elem).removeData('flash').html($originContent);
    };

    self.get = function() {
        return flashElem;
    };

    self.update = function(src) {
        if ($.browser.msie) {
            $('[name="movie"]', elem).prop('value', src);
        } else {
            elem.childNodes[0].src = src;
        }
    };

    return self;
}

function detectVersion ( v )
{
    var descr, pv, maxVersion = 10;

    //thats NS, Mozilla, Firefox
    if (typeof navigator.plugins['Shockwave Flash'] === 'object') {
        descr = navigator.plugins['Shockwave Flash'].description;
        descr = descr.replace(/^.*\s+(\S+\s+\S+$)/, "$1");
        pv = [
            descr.replace(/^(.*)\..*$/, "$1"),
            descr.replace(/^.*\.(.*)\s.*$/, "$1"),
            /r/.test(descr) ? descr.replace(/^.*r(.*)$/, "$1") : 0
        ];
    }

    //thats IE
    else if ( typeof ActiveXObject === 'function')
    {
        var ao;
        for(var i = maxVersion; i >= 2; i--)
        {
            try {
                ao = new ActiveXObject('ShockwaveFlash.ShockwaveFlash.' + i);
                if ( typeof ao === 'object' ) {
                    descr = ao.GetVariable('$version');
                    break;
                }
           } catch(e){

           }
        }

        pv = descr.split(' ');
        if(pv) {
            pv = pv[1];
            if(pv) {
                pv = pv.split(',');
            }
        }
    }



    if ( !pv && v ) {
        return false;
    }

    function toInt (arr) {
       return $.map(arr, function(n){
          return parseInt(n,10);
        });
    }

    v = toInt( v.split('.') );
    pv = toInt( pv );

    return !!(pv[0] > v[0] || (pv[0] == v[0] && pv[1] > v[1]) || (pv[0] == v[0] && pv[1] == v[1] && pv[2] >= v[2]));


}

function toAttr(obj) {
    var str = '';
    for (var key in obj ) {
        str += ' ' + key + '="' + obj[key] + '"';
    }
    return str;
}


})(require('jquery'));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/plugins/flash/jquery.flash.js
 ** module id = 619
 ** module chunks = 52 53 96 97 232
 **/