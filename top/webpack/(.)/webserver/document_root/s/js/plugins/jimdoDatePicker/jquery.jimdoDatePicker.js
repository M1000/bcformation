require('jquery-ui/ui/core.js');
require('jquery-ui/ui/datepicker.js');
require('jquery-ui/themes/base/datepicker.css');
require('../xLazyLoader/jquery.xLazyLoader.js');

(function($, jimdoData) {
    "use strict";

    /* Invoke the datepicker functionality with jimdo specials (the language).

     @param  options  string - a command, optionally followed by additional parameters or
     Object - settings for attaching new datepicker functionality
     @return  jQuery object */
    var defaults = {
        buttonImage: jimdoData.cdnUrl + 's/img/cms/icons/cal.gif',
        buttonImageOnly: true,
        buttonText: null,
        showAnim: 'slideDown',
        showOn: 'button',
        success: $.noop
    };

    $.fn.jimdoDatePicker = function(options) {
        // FIXME: this will always load the base localization, e.g. Portuguese in Brazil will still load Portuguese :(
        var language = jimdoData.cmsLanguage.split('_')[0].toLowerCase(),
            o = $.extend({}, defaults, options),
            dp = $(this).datepicker(o);

        if (language === 'en') {
            //we don't need to load localization scripts, so call success callback right away
            o.success(dp);

            return dp;
        }

        var requireContext = require.context('jquery-ui/ui/i18n/', true, /datepicker-(de|en|fr|es|ru|it|pt|ja|nl|da|sv)\.js$/i);
        requireContext('./datepicker-' + language + '.js')(function() { o.success(dp); });

        return dp;
    };

})(require('jquery'), require('jimdoData'));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/plugins/jimdoDatePicker/jquery.jimdoDatePicker.js
 ** module id = 620
 ** module chunks = 2 28 96 97 232
 **/