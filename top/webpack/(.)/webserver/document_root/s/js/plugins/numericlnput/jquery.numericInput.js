require('../actionController/jquery.actionController.js');

(function($) {
    'use strict';

    $.fn.numericInput = function(options) {
        var args = Array.prototype.slice.call(arguments),
            method = args.shift();

        return this.each(function(index, element) {
            var instance = $.data(element, 'numericInput') || $.data(element, 'numericInput', new NumericInput(element, options));

            // private underscore (_) methods are NOT callable from outside
            if (method && typeof method === 'string' && method.charAt(0) !== '_' && $.isFunction(instance[method])) {
                instance[method].apply(instance, args);
            }
        });
    };

    $.fn.numericInput.defaults = {
        change: null,
        maxValueExceeded: null,
        decimals: 0,
        decimalSymbol: '',
        disabled: false,
        maxValue: 1000000,
        minValue: 0,
        mouseHold: true,
        present: null,
        steps: 1,
        thousandSymbol: ''
    };

    /** @const **/
    var INFINITY_SYMBOL = '∞';
    /** @const **/
    var MIN_VALUE_LENGTH = 2;

    var currentCharWidth;

    function getCharacterWidth(fontSize) {
        if (!currentCharWidth) {
            var $element = $('<span id="cc-numeric-input-font-size" style="display: none; font-size: ' + fontSize + ';">0</span>').appendTo('body');
            currentCharWidth = $element.width();
            $element.remove();
        }

        return currentCharWidth;
    }

    function NumericInput() {
        if ($.isFunction(this.init)) {
            this.init.apply(this, arguments);
        }
    }

    NumericInput.prototype = {

        init: function(element, optionsPassedIn) {
            var defaults = $.fn.numericInput.defaults,
                options;

            this.$element = $(element);
            this.$input = this.$element.find('input:first');
            this.$label = this.$element.closest('label');

            options = {
                decimals: parseInt(this.$input.data('decimals'), 10) || defaults.decimals,
                decimalSymbol: this.$input.data('decimal-symbol') || defaults.decimalSymbol,
                disabled: this.$input.is(':disabled'),
                maxValue: parseInt(this.$input.data('max-value'), 10) || defaults.maxValue,
                minValue: parseInt(this.$input.data('min-value'), 10) || defaults.minValue,
                mouseHold: this.$input.data('mouse-hold') || defaults.mouseHold,
                steps: parseInt(this.$input.data('steps'), 10) || defaults.steps,
                thousandSymbol: this.$input.data('thousand-symbol') || defaults.thousandSymbol
            };

            this.options = $.extend({}, defaults, options, optionsPassedIn);

            this.value = this.options.minValue;
            this.value = this._normalizeAndReturnNumericValue(this.$input.val());

            this.setInputMaxlength();
            this._presentValue(this.value);
            this._bindEventsToElement();

            if (this.options.disabled) {
                this.disable();
            } else {
                this.enable();
            }
        },

        enable: function() {
            this.$element.actionController('enable');
            this.$label.removeClass('cc-state-disabled');
            this.$input.prop('disabled', false);
        },

        disable: function() {
            this.$element.actionController('disable');
            this.$label.addClass('cc-state-disabled');
            this.$input.prop('disabled', true);
        },

        getValue: function() {
            return this.value;
        },

        setMinimum: function(minimum) {
            this.options.minValue = parseInt(minimum, 10);
        },

        getMinimum: function() {
            return this.options.minValue;
        },

        setMaximum: function(maximum) {
            this.options.maxValue = parseInt(maximum, 10);
        },

        getMaximum: function() {
            return this.options.maxValue;
        },

        setInputMaxlength: function() {
            var valueToApply = this.options.maxValue + '';
            if (valueToApply.length < MIN_VALUE_LENGTH) {
                valueToApply = new Array(MIN_VALUE_LENGTH+1).join('0');
            }
            valueToApply = this._formatDecimals(valueToApply);

            this.$input
                .css('width', this._getMaximumInnerWidth(valueToApply) + 'px')
                .attr('maxlength', valueToApply.length);
        },

        _getMaximumInnerWidth: function(value) {
            var innerWidth = ('' + value).length * getCharacterWidth(this.$input.css('font-size'));

            // Workaround to make sure that padding is applied in chrome so the
            // width is calculated correctly with border box. We had this problem
            // with chrome 33 and jquery 1.8.1 but this works fine in firefox 27.
            if (this.$input.css('box-sizing') === 'border-box') {
                return innerWidth + (this.$input.outerWidth() - this.$input.width());
            }

            return innerWidth;
        },

        changeValue: function(newValue) {
            this.value = this._normalizeAndReturnNumericValue(newValue);
            this._presentValue(this.value);

            if (this.maxValueExceededShouldBeCalled && $.isFunction(this.options.maxValueExceeded)) {
                this.options.maxValueExceeded.call(this.$input[0], this.options.maxValue);
            }

            // Optional call the given on change handler and provide the new value
            if ($.isFunction(this.options.change)) {
                this.options.change.call(this.$input[0], this.value);
            }
        },

        modifyValue: function(direction) {
            this._modifyValueByDirection(direction);
        },

        _presentValue: function(valueToPresent) {
            var numericValue = valueToPresent;

            if (valueToPresent === Infinity) {
                valueToPresent = INFINITY_SYMBOL;
            } else {
                valueToPresent = this._formatDecimals(valueToPresent);
            }

            if ($.isFunction(this.options.present)) {
                valueToPresent = this.options.present.call(this.$input[0], valueToPresent, numericValue);
            }

            this.$input.val(valueToPresent);
        },

        _bindEventsToElement: function() {
            this.$element
                .actionController(this, {
                    events: 'click change mousedown mouseup mouseout keydown keyup',
                    context: 'controller'
                });
        },

        _arrowMousedown: function(e, direction) {
            var self = this;
            // sometimes the mouseup event wont be on the arrow but somewhere in the page because of a dom change that the numeric triggered.
            // we listen once on the whole bode
            $('body').one('mouseup', function(){
                self._clearMouseHoldDefer();
            });
            this._modifyValueByDirection(direction, 300);
        },

        _arrowMouseup: function() {
            this._clearMouseHoldDefer();
        },

        _arrowMouseout: function() {
            this._clearMouseHoldDefer();
        },

        _inputKeyup: function() {
            this._clearMouseHoldDefer();
        },

        _inputChange: function() {
            this.changeValue(this.$input.val());
        },

        _inputKeydown: function(e) {
            var direction;

            switch (e.keyCode) {
                case 38:
                    direction = 'up';
                    break;

                case 40:
                    direction = 'down';
                    break;
                case 13:
                case 10:
                    this.changeValue(this.$input.val());
                    return true;
                default:
                    return true;
            }

            this._modifyValueByDirection(direction);
        },

        _infiniteClick: function(e) {
            e.preventDefault();
            this.changeValue(Infinity);
        },

        _modifyValueByDirection: function(direction, repeat) {
            var self = this,
                modifyValueBy = 0,
                newValue;

            if (this.$input.is(':disabled')) {
                return;
            }

            this.value = this._normalizeAndReturnNumericValue(this.$input.val());

            if (direction === 'down') {
                modifyValueBy = (-1 * this.options.steps);
            } else if (direction === 'up') {
                modifyValueBy = (+1 * this.options.steps);
            }

            if (this.value === Infinity) {
                newValue = this.options.minValue;
            } else {
                newValue = this.value + modifyValueBy;
            }

            this.changeValue(newValue);

            if (this.options.mouseHold && repeat) {
                this.mouseHoldTimeoutToken = setTimeout(function() {
                    self._modifyValueByDirection(direction, 50);
                }, repeat);
            }
        },

        _normalizeAndReturnNumericValue: function(newValue) {
            var minValue = this.options.minValue,
                maxValue = this.options.maxValue;
            this.maxValueExceededShouldBeCalled = false;

            if (newValue === Infinity || newValue === INFINITY_SYMBOL) {
                return Infinity;
            }

            if (newValue === '') {
                return Math.max(0, minValue);
            }

            newValue = this._parseNumeric(newValue);

            if (isNaN(newValue)) {
                return this.value;
            }

            if (newValue < minValue) {
                return minValue;
            }


            if (maxValue && newValue > maxValue) {
                this.maxValueExceededShouldBeCalled = true;
                return maxValue;
            }

            return newValue;
        },

        _parseNumeric: function(value) {
            if (typeof value === "number") {
                return value;
            }

            if (this.options.decimals > 0) {
                return parseFloat(
                    this
                        ._removeThousandSymbols(value)
                        .replace(this.options.decimalSymbol, '.')
                );
            }

            return parseInt(this._removeThousandSymbols(value), 10);
        },

        _formatDecimals: function(value) {
            if (this.options.decimals > 0) {
                var splitValue = (value + '').split('.'),
                    decimalValue = splitValue[1] || '',
                    numZeroes = Math.max(this.options.decimals - decimalValue.length + 1, 1);

                return splitValue[0] +
                    this.options.decimalSymbol +
                    decimalValue.substr(0, 2) +
                    (new Array(numZeroes)).join('0');
            }

            return value;
        },

        _clearMouseHoldDefer: function() {
            clearTimeout(this.mouseHoldTimeoutToken);
        },

        _removeThousandSymbols: function(value) {
            if (this.options.thousandSymbol) {
                // Use split and join instead of regex because the thousand symbol could be anything, so it's unclear
                // if it would need escaping
                return (value + '').split(this.options.thousandSymbol).join('');
            }

            return value;
        }
    };
})(require('jquery'));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/plugins/numericInput/jquery.numericInput.js
 ** module id = 587
 ** module chunks = 1 29 96 97 232
 **/