/*
 * xSlider 1.1 - Plugin for jQuery
 *
 * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
 * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
 *
 * Depends:
 *   jquery.js
 *
 *  Copyright (c) 2008 Oleg Slobodskoi (ajaxsoft.de)
 */

require('jquery-ui/ui/slider.js');

;(function($, undefined){
    "use strict";

$.fn.xSlider = function( options ) {
    var	args = Array.prototype.slice.call(arguments, 1),
        ret = null;
    this.each(function() {
        var instance = $.data(this, 'xSlider') || $.data( this, 'xSlider', new xSlider(this, options));
        ret = instance[ typeof options === 'string' ? options : 'init' ].apply(instance, args);
    });
    return ret === undefined ? this : ret;
};

$.fn.xSlider.defaults = {
    slider: '.x-slider',
    speed: 500,
    value: 0,
    axis: 'x',
    cycle: true,
    next: null,
    prev: null,
    slide: function(){},
    afterSlide: function(){}
};

function xSlider ( elem, options )
{
    var self = this,
        s = $.extend({}, $.fn.xSlider.defaults, options),
        $elem = $(elem),
        sliderSize = getElemSize(),
        $next, $prev,
        animationComplete = false,
        animating,
        $window = $(window);

    self.value = 0;

    this.init = function()
    {
        self.update();

        if ( self.$slider.length < 1 ) {
            return;
        }

        var value = valueToNum(s.value);
        $elem.addClass('x-slider-wrapper').wrapInner('<div class="x-slider-scroll-sections"></div>')
        // prevent scrolling if not with xSlider, e.g. chrome issue by tabbing or hash values
        .bind('scroll', function(){
            if(!animating) {
                $elem.scrollLeft(self.value*$elem.innerWidth());
            }
        });

        $window.bind('resize.xslider', function() {
            self.resize();
        });

        // scroll to the initial slider
        if (valueToNum(value) > 0) {
            self.slide(value);
        }

        // add active class
        self.$slider.eq(value).addClass('x-slider-active');

        if ( s.next ) {
            $next = $(s.next).bind('click', function(){
                self.slide('next');
                return false;
            });
        }
        if ( s.prev ) {
            $prev = $(s.prev).bind('click', function(){
                self.slide('prev');
                return false;
            });
        }
    };


    this.slide = function( value, c )
    {
        value = valueToNum(value);

        if (animating) {
            return;
        }


        if ( s.cycle ) {
            value = value<0 ? self.$slider.length-1 : value>= self.$slider.length ? 0 : value;
        } else {
            if ( value<0 || value >= self.$slider.length ) {
                callback('afterSlide', c);
                return;
            }
        }

        // set class to active slider
        self.$slider.removeClass('x-slider-active').eq(value).addClass('x-slider-active');

        callback('slide');
        animating = true;
        $elem.addClass('x-slider-sliding').animate({scrollLeft: value*$elem.innerWidth()}, s.speed, function(){
            $elem.removeClass('x-slider-sliding');
            animating = false;
            animationComplete = true;
            callback('afterSlide', c);
        });
        self.value = value;


    };

    this.slideToActive = function() {
        // XXX: Fixes width: 0 / height: 0 on upgrade teasers that are initiated before the are visible. Find a better
        // solution without requiring fixed sizes
        self.$slider.css({width: 'auto', height: 'auto'});
        self.resize();
        self.slide(s.value);
    };

    /**
     * Returns if the slider has completed the animation.
     * Use this for testing via Cucumber/Selenium/Webdriver.
     */
    this.isAnimationComplete = function() {
        return animationComplete;
    };

    this.resize = function( size ) {
        sliderSize = size || getElemSize();
        self.$slider.css(sliderSize);

        $elem.scrollLeft(self.value*$elem.innerWidth());
    };

    this.destroy = function() {
        if($next) {
            $next.unbind('click');
        }
        if($prev) {
            $prev.unbind('click');
        }
        $elem.removeClass('x-slider-wrapper').unbind('scroll').removeData('xSlider');
        $window.unbind('resize.xslider');
    };

    this.update = function() {
        self.$slider = $elem.find(s.slider).css(sliderSize);
    };

    // add a new slider, insert it at the end, and scroll if callback is setted
    this.add = function( slider, callback ) {
        var $slider = $(slider).addClass('x-slider').css(sliderSize).appendTo( $('.x-slider-scroll-sections', elem) );
        self.$slider = self.$slider.add($slider);
        if(callback) {
            self.slide($slider[0], callback);
        }
    };

    // get the content of any slider
    this.get = function( value ) {
        return self.$slider.eq( valueToNum(value) ).html();
    };

    function valueToNum( value ) {
        //thats selector string or "prev" || "next"
        if (typeof value === 'string')
        {
            var $slider = self.$slider.filter( value );

            if ( $slider.length>0 ) {
                value = valueToNum( $slider[0] );
            } else if ( value === 'prev') {
                value = self.value-1;
            } else if ( value === 'next' ) {
                value = self.value+1;
            } else {
                value = 0;
            }
        } else if (typeof value === 'object') {
            //thats a dom elem
            value = self.$slider.index(value);
        }
        return value;
    }

    function getElemSize() {
        return { width: $elem.width(), height: $elem.height() };
    }

    function callback( name, c ) {
        $elem.trigger(name, [self]);
        s[name].apply(elem, [$.Event(name),self]);
        if($.isFunction(c)) {
            c.apply(elem,[$.Event(name), self]);
        }
    }



}



})(require('jquery'));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/plugins/xSlider/jquery.xSlider.js
 ** module id = 577
 ** module chunks = 1 2 53 96 97 232 234 241
 **/