/**
 * @class message
 * @author Oleg Slobodskoi aka Kof
 * Show message near any element on the page
 */

(function($){
    "use strict";

$.fn.message = function( msg, options ) {

    return this.each(function() {
        if ( $.data(this, 'message') ) {
            return;
        }
        $.data( this, 'message', new message(this, msg, options) ).init();
    });
};

/* defaults */
$.fn.message.defaults = {
    message: null,
    closable: true,
    status: 'success', //error || alert || success || ok || warning || confirm
    autoHide: 5000,
    addClass: '',
    /*jshint multistr: true */
    template: '\
        <div>\
            <span class="x-message-close x-message-action">x</span>\
            <p class="x-message-content">${message}</p>\
            ${buttons}\
        </div>',
    buttonsTpl: '\
        <br /><button type="button" class="x-message-button-ok x-message-action" selected="selected">${ok}</button>\
        <button type="button" class="x-message-button-abort x-message-action">${abort}</button>\
    ',
    speed: 300,
    width: 'auto',
    maxHeight: 150,
    top: null,
    left: null,
    onClose: $.noop,
    onClick: $.noop,
    labels: {
        ok: "Ok",
        abort: "Abort"
    },
    insertMethod: 'insertBefore',
    resetOffset: false //reset the offset so that it won´t be outside the visible area
};

function message( target, message, options )
{

    if (typeof message === 'object') {
        options = message;
        message = null;
    }

    var s = $.extend(true, {}, $.fn.message.defaults, options);
    var self = this,
        $message,
        $target = $(target),
        height,
        width,
        left,
        position,
        buttons = '',
        timeout;

    this.init = function()
    {
        message = message || s.message;
        if ( !message ) {
            return this;
        }

        if ($target.data('place-error')) {
            s.insertMethod = $target.data('place-error');
        }

        if ($target[0].nodeName=='FORM') {
            var $submit = $('[type="submit"]', $target);
            $target = $submit.length ? $submit : $target;
        }

        if (s.status == 'confirm') {
            buttons += s.buttonsTpl
                .replace('${ok}', s.labels.ok)
                .replace('${abort}', s.labels.abort);

            s.autoHide = null;
        }

        $message = $( s.template.replace('${message}', message || s.message).replace('${buttons}', buttons) )
            .addClass('x-message message-'+s.status+' '+s.addClass)
            [s.insertMethod]($target);

        s.closable && $message.addClass('x-message-closable');
        position = $target.position();
        height = $message.outerHeight(true);
        width = $message.outerWidth();
        if ( height > s.maxHeight ) {
            height = s.maxHeight;
            /* block because the content should be scrollable */
            $message.css('height', height).find('.x-message-content').css('display','block');
        }

        //check if there is enought place and if not - move left a bit
        left = s.left || position.left;
        var wWidth = $(window).width();
        if ( left + width > wWidth ) {
            left = left - ( left+width-wWidth );
        }

        var topOffset = s.top || (position.top - 2*height + $target.closest('form').scrollTop());
        if (s.resetOffset && topOffset + height < 0) {
            topOffset = -1 * height;
        }

        $message
            .css({
                display: 'none',
                visibility: 'visible',
                top: topOffset,
                left: left,
                width: s.width
            })
            .animate({top: '+='+height, opacity: 'show'}, s.speed, function(){
                if (s.autoHide) {
                    timeout = setTimeout(remove, s.autoHide);
                }
            })

            .click(function(e) {
                //only trigger click callback if target is not(!) the close icon
                $(e.target).is('.x-message-close') || s.onClick.call($message[0], e);
                remove(e);
            });

    };

    this.destroy = function() {
        $message.remove();
        $.removeData(target, 'message');
    };

    function remove(e)
    {
        if ( s.status == 'confirm' && !$(e.target).hasClass('x-message-action')) {
            return false;
        }

        clearTimeout(timeout);

        var status = e && e.target && /ok/.test(e.target.className) ? 'ok' : 'abort';
        if ( s.onClose.apply($message[0], [e, status, self]) === false ) {
            return false;
        }

        self.destroy();
        return false;
    }
}

})(require('jquery'));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/plugins/message/jquery.message.js
 ** module id = 492
 ** module chunks = 16 17 18 96 97 232 235 241
 **/