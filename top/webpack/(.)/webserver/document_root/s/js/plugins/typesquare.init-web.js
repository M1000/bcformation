(function (jimdoData) {
    'use strict';

    if (jimdoData.dmp && jimdoData.dmp.typesquareFontsAvailable) {
        require.ensure([], function (require) {
            require('./typesquare.init.js');
        });
    }

})(require('jimdoData'));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/plugins/typesquare.init-web.js
 ** module id = 3700
 ** module chunks = 96
 **/