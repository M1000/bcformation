require('./jquery.content-box.css');

/**
 * contentBox - plugin for jQuery
 *
 * @version 1.1
 * @author Oleg Slobodskoi
 * @website jsui.de
 * @depends
 *   - jquery.js (1.3)
 *   - jquery.flash.js (optional if paramsAttr {type: 'flash'})
 *
 * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
 * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
 */

;(function($) {
    "use strict";

$.fn.contentBox = function ( options ) {
    return this.each(function() {
        if(!$.data(this, 'contentBox')) {
            $.data( this, 'contentBox', new contentBox(this, options)).init();
        }
    });
};

/* defaults */
$.fn.contentBox.defaults = {
    item: 'a',
    pathAttr: 'href',
    paramsAttr: null, // possible values in param attr: {type: 'flash', other flash settings}
    speed: 400,
    keyNavigation: true,
    closeOnDocumentClick: true,
    alwaysCenter: true,
    cycle: false,
    width: 400,
    minHeight: 200,
    minWidth: 200,
    maxHeight: 500,
    flash: {
        version: "6.0.0",
        bgcolor: "#fff"
    }
};


function contentBox ( container, options )
{
    var s = $.extend({}, $.fn.contentBox.defaults, options);

    /*jshint multistr: true */
    var template = '\
        <div class="content-box">\
            <div class="content-box-shadow"></div>\
            <div class="content-box-content-wrapper">\
                <div class="content-box-content">#items#</div>\
            </div>\
            <a href="" class="content-box-close"></a>\
            <a href="" class="content-box-next"><span/></a>\
            <a href="" class="content-box-prev"><span/></a>\
        </div>\
    ';

    var newItemTpl = '<div class="content-box-item"><div class="content-box-item-content"></div></div>';

    var self = this,
        $container = $(container),
        $cb,
        $cbc,
        $nextBtn,
        $prevBtn,
        $scroller,
        $currentItem,
        $currentContent,
        $items,
        $contents = [],
        items = 0,
        current = 0,
        width,
        height,
        maxHeight,
        nextUrl,
        prevUrl,
        scrollTimeout,
        oHeight,
        ready
    ;

    this.init  = function ()
    {
        $container.bind('click', function(e) {
            if(!$cb) {
                create();
            }
            $currentItem = $(e.target).closest(s.item);
            current = $items.index($currentItem[0]);

            var url = $.trim($currentItem.attr(s.pathAttr));
            if ( !url ) {
                return;
            }

            if(!ready) {
                $cb.css( getTargetPos($currentItem[0], e) );
            }
            getMaxHeight();
            change();

            if ( ready ) {
                return false;
            }


            $cb.bind('click', function(e) {
                var elem = e.target.className ? e.target : e.target.parentNode;
                if ( $(elem).hasClass('content-box-disabled') ) {
                    $cb.addClass('content-box-animating');
                    shake(2, 150, 40);
                    return false;
                }
                if ( /content-box/.test(elem.className) ) {
                    var act = (elem.className).split('-').pop();
                    if($.isFunction(self[act])) {
                        self[act]();
                    }
                    return false;
                }
            });

            $(window).bind('resize scroll', moveToCenter);
            $(document).bind('keydown', keyNavigation).bind('click', docClick);
            ready = true;
            return false;
        });


    };

    this.next = function() {
        change(1);
    };

    this.prev = function() {
        change(-1);
    };

    this.close = function() {
        $(window).unbind('resize scroll',moveToCenter);
        $(document).unbind('keydown',keyNavigation).unbind('click', docClick);
        $cb.unbind('click').animate($.extend(getTargetPos( $items.eq(current)[0] ),{opacity: 'hide'}), s.speed);
        ready = false;
    };

    function create() {
        var contentItems = '';
        $items = $(s.item+'['+s.pathAttr+']', container)
        .each(function(){
            contentItems+= newItemTpl;
        });
        $cb = $(template.replace('#items#',contentItems)).appendTo(document.body);

        $contents = $('.content-box-item-content',$cb);
        $cbc = $('.content-box-content', $cb);
        $nextBtn = $('.content-box-next', $cb);
        $prevBtn = $('.content-box-prev', $cb);
        $scroller = $('.content-box-content-wrapper', $cb).scrollLeft(0);
        //there is only 1 item => hide back and next buttons and remove the padding
        if($items.length<= 1) {
            $cb.addClass('content-box-no-prev-next');
        }

        oHeight = $cb.height();

    }

    function change( index ) {
        current = index ? current+index : current;
        $currentItem = $items.eq(current);
        $currentContent = $contents.eq(current);

        handles();

        setSizePos(function(){
            $cb.show().addClass('content-box-animating');
            $scroller.animate({scrollLeft: $currentContent[0].offsetLeft}, s.speed, function(){
                // if not loaded jet
                if ( !$contents[current].innerHTML ) {
                    $scroller.addClass('content-box-loading');
                    var params = s.paramsAttr ? $.trim($currentItem.attr(s.paramsAttr)) : null;
                    if ( params ) {
                        params = eval('(' + params + ')');
                        if (typeof params === 'object' && params.type == 'flash' && $.fn.flash) {
                            delete params.type;
                            $currentContent.flash($.extend({swf: $currentItem.attr(s.pathAttr)}, s.flash, params));
                            _ready();
                        }
                    } else {
                        $.get($currentItem.attr(s.pathAttr), function(res){
                            $currentContent.html(res);
                            _ready();
                        });
                    }
                } else {
                    setSizePos();
                }

                function _ready() {
                    $scroller.removeClass('content-box-loading');
                    setSizePos();
                }

            });
        });
    }

    function handles() {
        // button next
        var $elem = $items.eq(current+1);
        if ($elem.length) {
            nextUrl = $elem.attr(s.pathAttr);
        }
        $nextBtn[$elem.length ? 'removeClass' : 'addClass']('content-box-disabled');
        // button prev
        $elem = $items.eq(current-1);
        if ( $elem.length ) {
            prevUrl = $elem.attr(s.pathAttr);
        }
        $prevBtn[$elem.length ? 'removeClass' : 'addClass']('content-box-disabled');
    }

    function setSizePos( callback ) {
        //hide the scrollbar to det the right width
        $cb.addClass('content-box-animating');

        var pad = {
            vert: $currentContent.outerHeight()-$currentContent.height(),
            hor: $currentContent.outerWidth()-$currentContent.width()
        };

        var $contentElems = $currentContent.children();
        width = $contentElems.length === 1 && parseInt($contentElems.css('width'),10);
        width = width||s.width;
        $currentContent.width(width);

        var cHeight = $currentContent.css('height','auto').height();
        if ( cHeight ) {
            cHeight = cHeight>maxHeight ? maxHeight-pad.vert : cHeight;
            cHeight = cHeight>s.minHeight ? cHeight : s.minHeight;
            $currentContent.height(cHeight);
        }
        height = cHeight ? cHeight+ pad.vert : oHeight;

        var css = {
            width: width+pad.hor,
            height: height,
            left: ($(window).width()-width)/2 + $(window).scrollLeft(),
            top: ($(window).height()-height)/2 + $(window).scrollTop()
        };

        $cb.animate(css, s.speed, function(){
            $cb.removeClass('content-box-animating');
            $(callback);
        });

    }

    function getTargetPos( elem, e ) {
        var pos;

        if ( e ) {
            pos = {
                left: e.clientX + $(window).scrollLeft(),
                top: e.clientY + $(window).scrollTop()
            };
        } else {
            pos = $(elem).offset();
        }

        return {
            left: pos.left - 20,
            top: pos.top - 20,
            width: 20,
            height: 20
        };
    }

    function shake( times, speed, distance, dir, timesNow ) {
        dir = dir||'+';
        timesNow = !timesNow ? 1 : timesNow+1;
        dir = dir=='+' ? '-' : '+';
        $cb.animate({left: dir+'='+distance}, speed, function(){
            if(timesNow < times*2) {
                shake(times, speed, distance, dir, timesNow);
            } else {
                $cb.removeClass('content-box-animating');
            }
        });
    }

    function keyNavigation( e ) {
        /* 39 -> 37 <- 27 esc */
        if ( s.keyNavigation && !$cb.hasClass('content-box-animating') ) {
            if(e.keyCode === 39) {
                $nextBtn.click();
            } else if(e.keyCode === 37) {
                $prevBtn.click();
            } else if(e.keyCode === 27) {
                self.close();
            }
        }
    }

    function moveToCenter(e) {
        if ( s.alwaysCenter ){
            clearTimeout(scrollTimeout);
            scrollTimeout = setTimeout(setSizePos, 300);
        }
        if(e.type == 'resize') {
            getMaxHeight();
        }
    }

    function docClick( e ) {
        if(s.closeOnDocumentClick && !$(e.target).parents('.content-box').length && $items.index(e.target) === -1) {
            self.close();
        }
    }

    function getMaxHeight() {
        var wndHeight = $(window).height()-40;
        maxHeight = s.maxHeight>wndHeight ? wndHeight : s.maxHeight;
    }

}

})( jQuery );



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/plugins/contentBox/jquery.contentBox.js
 ** module id = 680
 ** module chunks = 96 97 232
 **/