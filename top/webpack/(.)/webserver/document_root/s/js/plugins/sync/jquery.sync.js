/**
 * ajax queue, priority driven requests, delayed requests
 *
 * @todo:
 *  - abort current low prio request if the same url called during request?
 *  - call the callbacks queued - if the server data don't depends on queue,
 *    we could use non queued requests, but get queued callbacks. This will speed up the queue.
 *  - several tries by errors?
 *
 */
(function($) {
    "use strict";

    var queue = [],
        lastHighPrio = 0,
    // delayed requests timeouts
        delays = {},
        requests = {},
        pending = false,
        pause = false;

    $.sync = function(ajaxOptions, options) {
        var s = $.extend({}, $.sync.defaults, options),
            ajax = $.extend({}, s.ajaxOptions, ajaxOptions),
        // save user complete callback reference
            _complete = ajax.complete;

        if (s.oneAtATime && requests[ajax.url]) {
            return;
        }

        requests[ajax.url] = true;

        // this request ist out of queue
        if (!s.queue) {

        } else if (s.delay && s.delay > 0) {
            clearTimeout(delays[ajax.url]);
            delays[ajax.url] = setTimeout(add, s.delay);
        } else {
            add();
        }

        function add() {
            // clear timeout of delayed request to the same url
            clearTimeout(delays[ajax.url]);
            // add request to the queue
            if (s.prio === 'high') {
                queue.splice(lastHighPrio, 0, send);
                lastHighPrio++;
            } else {
                queue.push(send);
            }
            // run first request if there is no pending requests
            if(!pause && !pending) {
                queue[0]();
            }
        }


        function send() {
            pending = true;
            ajax.complete = complete;
            // data can be a function to enable the query creation at the time the request will be fired
            if ($.isFunction(ajax.data)) {
                ajax.data = ajax.data();
            }
            $.ajax(ajax);
            // remove it from the queue
            queue.shift();
            // if it was high prio request - reduce the index
            lastHighPrio = s.prio === 'high' ? lastHighPrio - 1 : lastHighPrio;
        }

        function complete() {
            // call user complete callback, because we have overwritten it
            if(typeof _complete === 'function') {
                _complete.apply(this, arguments);
            }
            pending = false;
            delete requests[ajax.url];
            if(!pause && queue.length) {
                queue[0]();
            }
        }
    };

    $.extend($.sync, {
        pause: function() {
            pause = true;
        },
        run: function() {
            pause = false;
            if(!pending && queue && typeof queue[0] === 'function') {
                queue[0]();
            }
        },
        defaults: {
            // true, if only one request with this url can be fired at once
            oneAtATime: false,
            // if you set it to false the request will be set as a first one and called immediately
            queue: true,
            // time to wait before adding to queue in ms
            delay: 300,
            // high priority will be always handled first
            prio: 'high', // low
            // set ajax options for all requests from this plugin
            ajaxOptions: {
                dataType: 'json'
            }
        }
    });

})(require('jquery'));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/plugins/sync/jquery.sync.js
 ** module id = 274
 ** module chunks = 1 2 96 97 232 234 241
 **/