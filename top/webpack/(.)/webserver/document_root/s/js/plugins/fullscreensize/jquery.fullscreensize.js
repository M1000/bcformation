
(function($, window) {
    "use strict";

    function Fullscreensize() {
        var self = this;

        if ($.isFunction(self.init)) {
            self.init.apply(self, arguments);
        }
    }

    Fullscreensize.prototype = {

        /**
         * Initialization method
         * @public
         */
        init: function(element, opts) {
            var self = this,
                options = {};

            $.extend(options, $.fn.fullscreensize.defaults, opts);
            self.options = options;

            self.$element = $(element);
            self.resize();
        },

        /**
         * Resize the element to full screen
         * @public
         */
        resize: function() {
            var self = this,
                $window = $(window),
                $element = self.$element,
                wHeight = $window.height(),
                wWidth = $window.width(),
                factor = self.options.factor,
                elementHeight = $element.height(),
                elementWidth = $element.width(),
                roundedHeight,
                roundedWidth,
                heightRatio,
                widthRatio,
                newWidth,
                newHeight;

            heightRatio = wHeight / elementHeight;
            widthRatio = wWidth / elementWidth;

            if (widthRatio >= heightRatio) {
                newWidth = widthRatio * elementWidth;
                newHeight = widthRatio * elementHeight;
            } else {
                newWidth = heightRatio * elementWidth;
                newHeight = heightRatio * elementHeight;
            }

            // ~~ truncates a number (e.g. removes fractions)
            roundedHeight = ~~(newHeight * factor);
            roundedWidth = ~~(newWidth * factor);

            $element
                .height(roundedHeight)
                .width(roundedWidth);

            self._positionImageInCenter();
        },

        _positionImageInCenter: function() {
            var self = this,
                $element = self.$element,
                imgHeight = $element.height(),
                imgWidth = $element.width(),
                $window = $(window),
                wHeight = $window.height(),
                wWidth = $window.width(),
                marginHeight,
                marginWidth;

            if (imgHeight > wHeight) {
                marginHeight =  ((imgHeight - wHeight) / 2) * -1;
                $element.css('margin-top', marginHeight);
            }

            if (imgWidth > wWidth) {
                marginWidth =  ((imgWidth - wWidth) / 2) * -1;
                $element.css('margin-left', marginWidth);
            }
        }

    };

    $.fn.extend({

        fullscreensize: function(options) {

            var args = Array.prototype.slice.call(arguments),
                method = args.shift();

            return this.each(function(index, element) {

                var instance = $.data(element, 'fullscreensize') || $.data(element, 'fullscreensize', new Fullscreensize(element, options));

                if (method && typeof method === 'string' && method.charAt(0) !== '_' && $.isFunction(instance[method])) {
                    instance[method].apply(instance, args);
                }

            });
        }
    });

    $.fn.fullscreensize.defaults = {
        factor: 1.00
    };

}(jQuery, window));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/plugins/fullscreensize/jquery.fullscreensize.js
 ** module id = 1226
 ** module chunks = 96 97 232
 **/