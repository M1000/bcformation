require('./jquery.pinterest.css');

(function($, window, undefined) {
    "use strict";

    function Pinterest() {
        var self = this;

        if ($.isFunction(self.init)) {
            self.init.apply(self, arguments);
        }
    }

    Pinterest.prototype = {

        /**
         * Initialization method
         * @public
         */
        init: function(opts, element) {

            var self = this,
                options = {};

            self.defaults = $.fn.pinterest.defaults;

            $.extend(true, options, self.defaults, opts);

            self.options = options;

            //Root element of the plugin (the image)
            self.$element = $(element);

            self.pinterestLink = undefined;

            self.enable();
        },

        /**
         * tries to return the title of the image,
         * if no title set, using the alt tag
         * @private
         * @returns {String}
         */
        _getDescription: function() {
            var self = this,
                $element = self.$element,
                alt = $element.attr('alt'),
                title = $element.attr('title');

            if (title) {
                return encodeURI(title);
            }

            if (alt) {
                return encodeURI(alt);
            }

            return '';
        },

        /**
         * Returns the url of the media
         * @private
         * @returns {String}
         */
        _getMediaUrl: function() {
            var self = this,
                $element = self.$element;

            return encodeURI($element.attr('src'));
        },

        /**
         * Returns the url of the location where the image is shown
         * @private
         * @returns {String}
         */
        _getLocationUrl: function() {
            return encodeURI(window.location.href);
        },

        /**
         * Returns the url for pinterest, scheme: http://pinterest.com/pin/create/bookmarklet/?media={MEDIA_URL}&url={URL}&description={DESC}
         * @private
         * @returns {String}
         */
        _getPinterestUrl: function() {
            var self = this,
                urlTemplate = 'http://pinterest.com/pin/create/bookmarklet/?media={MEDIA_URL}&url={URL}&description={DESC}';

            return urlTemplate
                .replace('{MEDIA_URL}', self._getMediaUrl())
                .replace('{URL}', self._getLocationUrl())
                .replace('{DESC}', self._getDescription());
        },

        /**
         * @private
         */
        _togglePinterestLink: function(value) {
            var self = this,
                options = self.options,
                $overlay = self.$element.closest(options.wrapper).find('.' + options.overlayClass);

            $overlay.toggle(value);
        },

        /**
         * Binds events to the element
         * @private
         */
        _bindEvents: function() {
            var self = this,
                options = self.options,
                $wrapper = self.$element.closest(options.wrapper),
                $pinterestLink = $wrapper.find('.' + options.linkClass);

            $wrapper
                .bind('mouseover.pinterest mouseleave.pinterest', function(e) {
                    self._togglePinterestLink(e.type === 'mouseover');
                });

            $pinterestLink
                .bind('click.pinterest', function(e) {
                    if(e) {
                        e.preventDefault();
                    }
                    self._openLinkAsPopup();
                });
        },

        /**
         * Unbinds events
         * @private
         */
        _unbindEvents: function() {
            var self = this,
                options = self.options,
                $wrapper = self.$element.closest(options.wrapper),
                $pinterestLink = $wrapper.find('.' + options.linkClass);

            $wrapper
                .unbind('mouseover.pinterest')
                .unbind('mouseleave.pinterest');

            $pinterestLink
                .unbind('click.pinterest');
        },

        /**
         * Sets the link to pinterest
         * @private
         */
        _setPinterestLink: function() {
            var self = this;

            self.pinterestLink =  self._getPinterestUrl();
        },

        /**
         * Opens the link to pinterest as popup
         * @private
         */
        _openLinkAsPopup: function() {
            var self = this;

            window.open(self.pinterestLink, 'Pinterest', 'width=900,height=500,resizable=yes').focus();
        },

        /**
         * Enable the plugin
         * @public
         */
        enable: function() {
            var self = this;

            self._unbindEvents();
            self._setPinterestLink();
            self._bindEvents();
        },

        /**
         * Disable the plugin
         * @public
         */
        disable: function() {
            var self = this;

            self._unbindEvents();
            self._togglePinterestLink(false);
        }

    };

    $.fn.extend({

        pinterest: function(options) {

            var args = Array.prototype.slice.call(arguments),
                method = args.shift();

            return this.each(function(index, element) {

                var instance = $.data(element, 'pinterest') || $.data(element, 'pinterest', new Pinterest(options, element));

                // private underscore (_) methods are NOT callable from outside
                if (method && typeof method === 'string' && method.charAt(0) !== '_' && $.isFunction(instance[method])) {
                    instance[method].apply(instance, args);
                }

            });
        }
    });

    $.fn.pinterest.defaults = {
        linkClass: 'cc-pinterest-link',
        overlayClass: 'cc-pinterest-overlay',
        wrapper: '.cc-imagewrapper'
    };

}(jQuery, window));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/plugins/pinterest/jquery.pinterest.js
 ** module id = 468
 ** module chunks = 96 97 232
 **/