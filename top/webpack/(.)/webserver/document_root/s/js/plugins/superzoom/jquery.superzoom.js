require('./jquery.superzoom.css');
require('imagesloaded/jquery.imagesloaded.js');

(function($) {

    'use strict';

$.fn.superzoom = function($container, opts) {
    var $this = $(this),
        $stage, $image, $thumb, $lens,
        stageWidth, stageHeight,
        thumbWidth, thumbHeight,
        imageWidth, imageHeight,
        lensWidth, lensHeight,
        options = {},
        THE_INCREDIBLE_NOT_KNOWING_WHY_MAGIC_FIX_NUMBER = 5, // this is added to the needed size for the image on stage - WHY? because it works
        isInit = false;

    $.extend(true, options, $.fn.superzoom.defaults, opts);

    /**
     * Thumb setup
     * -----
     * The thumbnail image hovering over which will display the large image. Do everything else only
     * after it is done loading, to avoid size calculation race conditions
     */
    $thumb = $('img', $this);

    $thumb.imagesLoaded(function() {
        thumbWidth = $thumb.width();
        thumbHeight = $thumb.height();

        if (!$thumb.parent().is('span')) {
            $thumb.wrap('<span></span>');
        }

        /**
         * Stage setup
         * -----
         * The stage for the large image, completely overlapping the container passed to this plugin
         */
        stageWidth = $container.width();
        stageHeight = $container.height();

        if (options.maxStageHeight < stageHeight) {
            stageHeight = options.maxStageHeight;
        }

        if (options.maxStageWidth < stageWidth) {
            stageWidth = options.maxStageWidth;
        }

        $('.sz-stage', $container).remove();
        $stage = $('<div class="sz-stage"></div>')
            .width(stageWidth)
            .height(stageHeight)
            .appendTo($container);

        // re-calculate width of sz-stage to fit into $container
        // (if it has css like padding/border/margin applied)
        var blingbling = $stage.outerWidth(true) - $stage.width();
        if (blingbling > 0) {
            stageWidth = stageWidth - blingbling;
            $stage.width(stageWidth);
        }

        /**
         * Image setup
         * -----
         * The large image inside the stage
         */
        $image = $('<img />')
            .attr('src', $this.attr('href'))
            .appendTo($stage);

        $('.sz-lens', $this).remove();
        $lens = $('<span class="sz-lens"></span>')
            .appendTo($('span', $this));

        function init() {
            isInit = true;

            var tmp = new Image();
            tmp.src = $this.attr('href');

            tmp.onload = function() {
                imageWidth = tmp.width + THE_INCREDIBLE_NOT_KNOWING_WHY_MAGIC_FIX_NUMBER;
                imageHeight = tmp.height + THE_INCREDIBLE_NOT_KNOWING_WHY_MAGIC_FIX_NUMBER;
                setupLens();
            };
        }

        /**
         * Lens setup
         * -----
         * The transparent rectangle over the thumbnail image marking the currently zoomed area
         */
        function setupLens() {
            lensWidth = thumbWidth * (stageWidth / imageWidth);
            lensHeight = thumbHeight * (stageHeight / imageHeight);
            lensWidth = lensWidth > thumbWidth ? thumbWidth : lensWidth;
            lensHeight = lensHeight > thumbHeight ? thumbHeight : lensHeight;

            $lens.width(lensWidth)
                .height(lensHeight);

            // Add two pixels to the computed size for the border
            lensWidth += 2;
            lensHeight += 2;
        }

        /**
         * Mouseover functionality
         * ---
         * Show and hide the lens and stage and pick the correct position for the lens and the large image
         * depending on mouse position
         */
        $this
            .addClass('sz-thumb')
            .hover(function() {
                if (!isInit) {
                    init();
                }
                $stage.show();
                $lens.show();

                return false;
            }, function() {
                $stage.hide();
                $lens.hide();

                return false;
            })
            .mousemove(function(e) {
                var mousex = e.pageX - $thumb.offset().left,
                    mousey = e.pageY - $thumb.offset().top,
                    left = mousex - lensWidth / 2,
                    top = mousey - lensHeight / 2,
                    maxLeft = thumbWidth - lensWidth,
                    maxTop = thumbHeight - lensHeight,
                    imageLeft, imageTop;

                left = left < 0 ? 0 : left;
                top = top < 0 ? 0 : top;
                left = left > maxLeft + 1 ? maxLeft + 1 : left;
                top = top > maxTop + 1 ? maxTop + 1 : top;

                $lens.css({
                    left: left,
                    top: top
                });

                imageLeft = (left / thumbWidth) * -imageWidth;
                imageTop = (top / thumbHeight) * -imageHeight;
                imageLeft = lensWidth >= thumbWidth ? stageWidth / 2 - imageWidth / 2 : imageLeft;
                imageTop = lensHeight >= thumbHeight ? stageHeight / 2 - imageHeight / 2 : imageTop;

                $image.css({
                    left: imageLeft,
                    top: imageTop
                });

                return false;
            })
            .click(function() {
                return false;
            });
    });

    return $this;
};

$.fn.superzoom.defaults = {
    maxStageWidth: 500,
    maxStageHeight: 400
};


})(require('jquery'));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/plugins/superzoom/jquery.superzoom.js
 ** module id = 682
 ** module chunks = 96 97 232
 **/