require('./jquery.browserMessage.css');

/**
 * displays browser messages in chrome-browser/chromium style after OR without test
 * apply on given element
 * message is shown, if one given test returns true
 *
 *
 * IE 6 detection:
 * return !window.addEventListener && !window.XMLHttpRequest && window.attachEvent;
 *
 * Events:
 *   - test.browserMessage
 *   - show.browserMessage
 *   - hide.browserMessage
 *   - destroy.browserMessage
 *
 * Example call including all possible options:
 *
 *           $('#content').browserMessage({
 *              tests: { //conditions for showing bar or not. first successful test (whom returns true) wins when autoRun is set
 *                  // ie6
 *                  ie6: {
 *                      test: function() {
 *
 *                          return !window.addEventListener && !window.XMLHttpRequest && window.attachEvent;
 *                      },
 *                      message: 'Your browser is way too old' // message wich is schown in message bar
 *                  }
 *              },
 *              autoRun: true, // true to start tests immediately | false to start test manually later
 *              autoHide: 5000, // time in mSec after message bar is removed | false to prevent automatic removal
 *              template: '<div class="{{messageClass}} ">\    // complete html templae for browser bar
 *                              <div class="{{containerClass}} {{sliceClass}} {{backgroundClass}}">\
 *                                  <div class="{{sliceClass}} {{iconClass}}"> </div>\
 *                                  <div class="{{textClass}}"></div>\
 *                                  <div class="{{sliceClass}} {{closeClass}}"></div>\
 *                              </div>\
 *                          </div>',
 *              messageClass: 'cc-browser-message', // text container
 *              iconClass: 'cc-browser-icon', // left icon
 *              textClass: 'cc-browser-text', // container for message
 *              closeClass:'cc-browser-close', // close button
 *              closeHoverClass: 'cc-browser-close-hover' //since ie6 is unable to do css hover, this extra class is needed
 *              sliceClass: 'cc-browser-slice', // contains all icons and bg
 *              containerClass: 'cc-browser-container', // message bar container class
 *              backgroundClass: 'cc-browser-slice-bg', // container background
 *
 *          });
 *
 *          $('#container').browserMessage('test' 'ie6');               // run a specific test
 *          $('#container').browserMessage('hide');                   // hide message
 *          $('#container').browserMessage('show', 'Welcome to Jimdo'); // show message
 *          $('#container').browserMessage('destroy'); // remove message bar and unbind events
 *
 *
 * @depends jquery.js, Mustache.js, jquery.browserMessage.css
 * @author rkowalski
 */



(function($) {
    "use strict";

    function BrowserMessage() {
        var self = this;

        if ($.isFunction(self.init)) {
            self.init.apply(self, arguments);
        }
    }

    BrowserMessage.prototype = {

        /**
         * Initialization function called by constructor
         * @private
         * @param {Object} opts options
         * @param {Object} element rootElement
         */
        init: function(opts, element) {
            var self = this,
                options = {};

            self.defaults = $.fn.browserMessage.defaults;

            $.extend(true, options, self.defaults, opts);

            self.options = options;

            //Root element where browsermessage is shown
            self.$element = $(element);

            //The jQuery elements. which are created lazy later
            self.$messageElement = undefined;
            self.$closeButton = undefined;
            self.$textNode = undefined;

            if (options && options.tests && options.autoRun) {
                self._runAllTests();
            }
        },

        /**
         * unbinds all events and removes message
         * @public
         * @event destroy
         */
        destroy: function() {
            var self = this,
                $messageElement = self._getMessageElement(),
                $element =  self.$element;

            self._clearAutoHideTimeout();
            self._unbindAll();
            $messageElement.remove();

            $element.removeData('browserMessage');
            $element.trigger('destroy.browserMessage');
        },

        /**
         * test for feature, and show a message box if test succeeds
         * @public
         * @param {String} index
         * @returns {Boolean} result
         */
        test: function(index) {
            var self = this,
                entry,
                result = false,
                options = self.options;

            if (options && options.tests && options.tests[index]) {
                entry = options.tests[index];

                if ($.isFunction(entry.test)){
                     /*
                     * inject the element-reference into our test which will be "this" there
                     */
                    result = entry.test.apply(self.$element);
                    if (result) {
                        self.show(entry.message);
                    }
                }
            }

            self.$element.trigger('test.browserMessage');

            return result;
        },

        /**
         * show/add messagebox
         * @public
         * @param {String} message
         * @event show
         */
        show: function(message) {
            var self = this,
                $messageElement = self._getMessageElement(),
                $element = self.$element,
                options = self.options,
                messageString = self._getMessageAsString(message);

            if (!$messageElement.length) {
                $messageElement = self._createMessageElement();
            }

            if (messageString) {
                self._getTextNode().html(message);
            }

            if ($messageElement.length) {

                self._messageBoxEffectShow();

                if(options.autoHide){
                    self._startAutoHideTimeout(options.autoHide);
                }

            }
            $element.trigger('show.browserMessage');
        },

        /**
         * hide messagebox
         * @public
         * @event hide
         */
        hide: function() {
            var self = this,
                $element = self.$element;

            self._clearAutoHideTimeout();
            self._messageBoxEffectHide();

            $element.trigger('hide.browserMessage');
        },

        /**
         * Returns the message string,
         * if message is a function, evaluate function to get messagestring
         * @private
         * @param message
         */
        _getMessageAsString: function (message) {
            var self = this,
                messageString,
                $element = self.$element;

            if ($.isFunction(message)) {
                messageString = message.apply($element);
            } else {
                messageString = message;
            }

            return messageString;
        },

        /**
         * Adding-effect for message box
         * @private
         *
         */
        _messageBoxEffectShow: function() {
            var self = this,
                $messageElement = self._getMessageElement(),
                $closeButton = self._getCloseButton();

            if ($messageElement.length) {
                $closeButton.show();

                $messageElement
                    .stop()
                    .css('margin-top', $messageElement.height()*-1)
                    .show()
                    .animate({'margin-top': 0}, 500, 'linear', function() {
                        self._scrollToElement($messageElement);
                    });
            }
        },

        /**
         * Removing-effect for message box
         * @private
         */
        _messageBoxEffectHide:function() {
            var self = this,
                $messageElement = self._getMessageElement(),
                $closeButton = self._getCloseButton();

            if ($messageElement.length) { // hide messagebox
                $closeButton.hide();

                $messageElement
                    .stop()
                    .animate({'margin-top': $messageElement.height()*-1}, 500, 'linear', function() {
                        $messageElement.hide();
                    });
            }
        },

        /**
         * Scrolls to an element, if out of viewport
         * @private
         * @param {jQuery} $element
         */
        _scrollToElement: function($element) {
            var self = this;

            /* scroll to message if outer screen */
            if (self._isNotInViewport($element)) {

                $('html,body').animate({ scrollTop: $element.offset().top }, 'fast', 'linear');
            }
        },

        /**
         * Checks is Element is not in above the viewport
         * returns true if element is not in viewport
         * @private
         * @param {jQuery} $element
         */
        _isNotInViewport: function($element) {
            var top = $(document).scrollTop();

            if ($element.length) {
                return top >= $element.offset().top + $element.height();
            }
        },

        /**
         * clears our Timeout
         * @private
         */
        _clearAutoHideTimeout: function() {
            var self = this;

            if (self.timeOutId) { // if a timeout for removing is set, remove the timeout
                clearTimeout(self.timeOutId);
                delete self.timeOutId;
            }
        },

        /**
         * set Timeout on removing element
         * @private
         * @param {Number} milliSeconds
         */
        _startAutoHideTimeout: function(milliSeconds) {
            var self = this,
                defaults = self.defaults;

            milliSeconds = parseInt(milliSeconds, 10);
            if (!milliSeconds) {
                milliSeconds = defaults.autoHide;
            }

            self._clearAutoHideTimeout();
            self.timeOutId = setTimeout(function(){
                self.hide();
            }, milliSeconds);
        },

        /**
         * run all tests
         * @private
         */
        _runAllTests: function() {
            var self = this,
                index,
                options = self.options,
                tests = options.tests,
                hasOwn = Object.prototype.hasOwnProperty;

            if (options && tests) {
                for (index in tests) {
                    if (hasOwn.call(tests, index) && self.test(index)) {

                        break;
                    }
                }
            }
        },

        /**
         * build messagebox html
         * @private
         * @param {String} message
         */
        _buildHtml: function(message) {
            var self = this;

            return $.mustache(
                self.options.template,
                {
                    message: message,

                    messageClass: self.options.messageClass,
                    iconClass: self.options.iconClass,
                    textClass: self.options.textClass,
                    closeClass:self.options.closeClass,
                    sliceClass: self.options.sliceClass,
                    containerClass: self.options.containerClass,
                    backgroundClass: self.options.backgroundClass
                }
            );
        },

        /**
         * creates message elements
         * @private
         * @returns {Object}
         */
        _createMessageElement: function() {
            var self = this,
                $element = self.$element;

            $element.prepend(
                self._buildHtml()
            );

            self._bindEventsToCloseButton();

            return self._getMessageElement();
        },

        /**
         * bind events on button-click
         * @private
         */
        _bindEventsToCloseButton: function() {
            var self = this,
                $button = self._getCloseButton(),
                closeHoverClass = self.options.hoverClass;

            $button
                .bind('click.browserMessage mouseover.browserMessage mouseout.browserMessage', function(e) {
                    e.preventDefault();

                    switch(e.type){
                        case 'click':

                            self.hide();
                        break;
                        case 'mouseover':

                            $button.addClass(closeHoverClass);
                        break;
                        case 'mouseout':

                            $button.removeClass(closeHoverClass);
                        break;

                    }
                });
        },

        /**
         * unbinds events on button
         * @private
         */
        _unbindAll: function() {
            var self = this,
            $button = self._getCloseButton();

            $button.unbind('.browserMessage');
        },

        /**
         * return the textual element
         * @private
         * @returns {jQuery}
         */
        _getTextNode: function() {
            var self = this,
                $textNode = self.$textNode;

            if (!$textNode || !$textNode.length) {
                $textNode = self._getMessageElement().find('.' + self.options.textClass + ':first');
            }

            return $textNode;
        },

        /**
         * returns the messagebox element lazy
         * @private
         * @returns {jQuery} $messageElement
         */
        _getMessageElement: function() {
            var self = this,
                $messageElement = self.$messageElement;

            if (!$messageElement || !$messageElement.length) {
                $messageElement = self.$element.find('.' + self.options.messageClass + ':first');
            }

            return $messageElement;
        },

        /**
         * return the close-button element
         * @private
         * @returns {jQuery}
         */
        _getCloseButton: function() {
            var self = this,
                $closeButton = self.$closeButton;

            if (!$closeButton || !$closeButton.length) {
                $closeButton =  self._getMessageElement().find('.' + self.options.closeClass + ':first');
            }

            return $closeButton;
        }

    };

    $.fn.extend({

        browserMessage: function(options) {

            var args = Array.prototype.slice.call(arguments),
                method = args.shift();

            return this.each(function(index, element) {

                var instance = $.data(element, 'elementOverlays') || $.data(element, 'elementOverlays', new BrowserMessage(options, element));

                // private underscore (_) methods are NOT callable from outside
                if (method && typeof method === 'string' && method.charAt(0) !== '_' && $.isFunction(instance[method])) {
                    instance[method].apply(instance, args);
                }

            });
        }
    });

    $.fn.browserMessage.defaults = {
        /*jshint multistr: true */
        template: '<div class="{{messageClass}} ">\
                       <div class="{{containerClass}} {{sliceClass}} {{backgroundClass}}">\
                           <div class="{{sliceClass}} {{iconClass}}"> </div>\
                           <div class="{{textClass}}"></div>\
                           <div class="{{sliceClass}} {{closeClass}}"></div>\
                        </div>\
                   </div>',

        messageClass: 'cc-browser-message',
        iconClass: 'cc-browser-icon',
        textClass: 'cc-browser-text',
        closeClass:'cc-browser-close',
        hoverClass: 'cc-state-hover',
        sliceClass: 'cc-browser-slice',
        containerClass: 'cc-browser-container',
        backgroundClass: 'cc-browser-slice-bg',

        tests: [

        ],
        autoRun: true,
        autoHide: 15000
    };


}(require('jquery')));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/plugins/browserMessage/jquery.browserMessage.js
 ** module id = 576
 ** module chunks = 96 97 232 238 239 240
 **/