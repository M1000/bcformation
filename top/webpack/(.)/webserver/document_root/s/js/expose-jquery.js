// Exposes jQuery as cc_JimdoData_Constants::JD_ENV_JQUERY_NS on window
(function(window, jimdoData, $) {
    "use strict";

    var jQueryNamespace = 'jQuery';

    // remove global jQuery variables ($, jQuery), to avoid users utilizing it
    $.noConflict(true);

    if (jimdoData && jimdoData[jQueryNamespace]) {
        if (!window[jimdoData[jQueryNamespace]]) {
            window[jimdoData[jQueryNamespace]] = $;
        }
    } else {
        // we don't have jimdoData, use legacy name
        if (!window.jimdoGen002) {
            window.jimdoGen002 = $;
        }
    }

})(window, require('jimdoData'), require('jquery'));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/expose-jquery.js
 ** module id = 503
 ** module chunks = 96 97 228 238 239 240
 **/