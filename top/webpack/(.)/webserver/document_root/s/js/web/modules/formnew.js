require('../scripts/bootstrap.js');
require('../../plugins/jimdoDatePicker/jquery.jimdoDatePicker.js');
require('../../plugins/numericInput/jquery.numericInput.js');

(function($, jimdoData, window){
    "use strict";

$.modules.formnew = function (opts) {
    if (opts.withinCms) {
        return false;
    }

    var $form,
        $formFields;

    $form = $(opts.selector).actionController({
        _formButtonClick: submitForm
    }, {events: 'submit click'});

    $(function() {
        $formFields = $form.find('.cc-m-form-view-element');

        $form.find('.cc-m-input-numeric').numericInput();

        var $calendarInputs = $form.find('.cc-m-form-calendar input');
        if ($calendarInputs.attr('type') === 'text' || !browserHasDateInputSupport()) {
            $calendarInputs.jimdoDatePicker({}, true);
        }
    });

    function submitForm(e) {
        e.preventDefault();

        var $button = $(this),
            data = [],
            captcha = null,
            $form = $button.parents('form'),
            $loading = $form.find('.cc-m-form-loading').show(),
            $formDataPrivacyCheckbox = $form.find('[name="dataprivacy"]');

        if ($formDataPrivacyCheckbox.attr('checked') === undefined) {
            var inputWrapper = $formDataPrivacyCheckbox.eq(0).closest('.cc-m-form-view-input-wrapper');
            var errorMessage = $formDataPrivacyCheckbox.data('error-message');

            inputWrapper.message(errorMessage, {status: 'error'});
            $loading.hide();
            return;
        }

        // Iterate over all form elements and build a data object from them
        $formFields.each(function(i, elem) {
            var $input = $(elem).find(':input'),
                field = opts.structure[i];

            if (!$input) {
                return true;
            }

            if ($input.attr('name') === 'captcha') {
                captcha = $.trim($input.val());
            }

            if (!field) {
                return true;
            }

            switch (field.type) {
                case 'select':
                    var options = [];
                    $input.find('option').each(function(_, option) {
                        if(option.selected) {
                            options.push($.trim(option.value));
                        }
                    });

                    data.push(options);

                    break;

                case 'radio':
                case 'checkgroup':
                    var selected = [];
                    $input.each(function(_, option) {
                        // We still need to check for the span contents until a publish-all happened (#40992, #41168)
                        var value = option.value !== "on" ? option.value : $(option).next('span').text();

                        option.checked && selected.push(value);
                    });

                    data.push(selected);

                    break;

                case 'check':
                    data.push($input[0].checked);

                    break;

                case 'calendar':
                    var dateValue = $.trim($input.val());
                    if ($input.attr('type') === 'text' || !browserHasDateInputSupport()) {
                        dateValue = $.datepicker.formatDate('yy-mm-dd', $input.datepicker("getDate"));
                    }
                    data.push(dateValue);
                    break;

                default:
                    data.push($.trim($input.val()));
            }
        });

        var postData = {
            moduleId: opts.moduleIdObfuscated,
            data: JSON.stringify(data),
            structure: JSON.stringify(opts.structure)
        };

        if (captcha !== null) {
            postData.captcha = captcha;
        }

        $.sync({
            url: '/app/module/form/submit/',
            type: 'post',
            dataType: 'json',
            data: postData,
            success: function(r) {
                if (r.status === 'success') {
                    var $wnd = $(window),
                        top = $form.offset().top - $wnd.height() / 2;

                    if (top < $wnd.scrollTop()) {
                        $('body, html').animate({scrollTop: top}, 500);
                    }

                    $form.add($loading).slideUp(500, function() {
                        $form.html(r.payload.confirmationText).slideDown(200);
                    });

                    if (window.CookieControl.isCookieAllowed("fb_analytics")
                        && jimdoData.isFacebookPixelIdEnabled
                        && jimdoData.isFacebookPixelIdEnabled === true
                        && typeof fbq !== 'undefined'
                    ) {
                        fbq('track', 'Lead');
                    }
                } else {
                    var scroll = true;

                    $.each(r.errors, function(name, msg) {
                        var $elem = $form.find('[name="' + name + '"]').eq(0).closest('.cc-m-form-view-input-wrapper');

                        if (scroll && $elem.length) {
                            scrollTo($elem.offset().top - 70);
                            scroll = false;
                        }

                        $elem.message(msg, {status: 'error'});
                    });

                    refreshCaptcha();
                    $loading.hide();
                }
            },
            error: function() {
                $button.message(jimdoData.tr.form.badRequest, {status: 'error'});

                refreshCaptcha();
                $loading.hide();
            }
        });
    }

    function scrollTo(top) {
        if ($(window).scrollTop() > top) {
            $('html, body').animate({scrollTop: top}, 300, 'swing');
        }
    }

    function refreshCaptcha() {
        $('.captcha .refresh', opts.selector).click();
    }

    function browserHasDateInputSupport() {
        var input = document.createElement('input'),
            notADateValue = 'not-a-date';

        input.setAttribute('type','date');
        input.setAttribute('value', notADateValue);

        return !(input.value === notADateValue);
    }
};

})(require('jquery'), require('jimdoData'), window);



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/modules/formnew.js
 ** module id = 1231
 ** module chunks = 96 97 232
 **/