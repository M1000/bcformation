require('../scripts/registry.js');
require('../../plugins/sync/jquery.sync.js');

var Cookies = require('js-cookie');

(function ($, _) {
    "use strict";

    $.reg('ShoppingCartFactory', function() {

        var VERSION = 1;

        this.create = function() {
            var persistence = this.createPersistence(VERSION);

            var clearCartKey = 'shop-cart-clear';
            if (Cookies.get(clearCartKey)) {
                persistence.clear();
                Cookies.remove(clearCartKey, {path: '/'});
            }

            return $.factory('ShoppingCart', [persistence]);
        };

        this.createPersistence = function(version) {
            var localStoragePersistence = $.factory('LocalStoragePersistence', [version, window]);

            if (localStoragePersistence.test()) {
                return localStoragePersistence;
            }

            return $.factory('CookiePersistence', [version]);
        };
    });

    $.reg('LocalStoragePersistence', function ($, version, window) {

        this.key = function () {
            return 'jimdo-cart-v' + version;
        };

        this.get = function () {
            return JSON.parse(window.localStorage.getItem(this.key()));
        };

        this.set = function (data) {
            return window.localStorage.setItem(this.key(), JSON.stringify(data));
        };

        this.clear = function () {
            window.localStorage.removeItem(this.key());
        };

        this.test = function() {
            try {
                var key = 'i_am_on_a_boat';
                window.localStorage.setItem(key, key);
                window.localStorage.removeItem(key);
                return true;
            } catch (e) {
                return false;
            }
        };
    });

    $.reg('CookiePersistence', function ($, version) {

        var options = {
            path: '/'
        };

        this.key = function () {
            return 'jimdo-cart-v' + version;
        };

        this.get = function () {
            return JSON.parse(Cookies.get(this.key()) || null);
        };

        this.set = function (data) {
            var key = this.key();
            Cookies.set(key, JSON.stringify(data), options);
            this._assertDataIsEqualTo(data);
        };

        this.clear = function () {
            Cookies.remove(this.key(), options);
        };

        this.test = function() {
            return true;
        };

        this._assertDataIsEqualTo = function(data) {
            if(!_.isEqual(this.get(), data)) {
                throw new Error('QuotaExceededError: Could not write cookie');
            }
        };
    });

    /**
     * localStorage-based implementation of shopping cart
     * @see cartSpec.js
     */
    $.reg('ShoppingCart', function ($, persistence) {
        this.getCart = function () {
            return persistence.get() || {};
        };

        this._setCart = function (cart) {
            persistence.set(cart);
        };

        this._getIndexOfProductById = function (items, productId, variantId) {
            var index, currentProduct;

            for (index = 0; index < items.length; ++index) {
                currentProduct = items[index].product;

                if (currentProduct &&
                    currentProduct.productId === productId &&
                    currentProduct.variantId === variantId) {
                    return index;
                }
            }

            return -1;
        };

        this._parseAmount = function (amount) {
            return parseInt(amount, 10);
        };

        this.getCartItemCount = function() {
            return this.getCartItems().length;
        };

        this.getCartItems = function () {
            var cart = this.getCart();
            return cart.cartItems || [];
        };

        this.clear = function () {
            persistence.clear();
        };

        this.addProduct = function (product, amount) {
            var cart = this.getCart();
            cart.cartItems = cart.cartItems || [];

            var indexOfProduct = this._getIndexOfProductById(cart.cartItems, product.productId, product.variantId);

            amount = this._parseAmount(amount);

            if (indexOfProduct > -1) {
                cart.cartItems[indexOfProduct].product = product;
                cart.cartItems[indexOfProduct].amount += amount;
            }
            else {
                cart.cartItems.push({
                    product: product,
                    amount: amount
                });
            }

            this._setCart(cart);
        };

        this.removeProduct = function(product) {
            var cart = this.getCart();

            cart.cartItems = cart.cartItems || [];

            var indexOfProduct = this._getIndexOfProductById(cart.cartItems, product.productId, product.variantId);

            if (indexOfProduct > -1) {
                cart.cartItems.splice(indexOfProduct, 1);
                this._setCart(cart);
            }
        };

        this.getAmountForProduct = function(product) {
            var cartItem = this._getCartItemByProductIdTuple(product);

            if (!cartItem) {
                return 0;
            }

            return cartItem.amount;
        };

        this.setAmountForProduct = function (product, amount) {
            var cart = this.getCart();

            cart.cartItems = cart.cartItems || [];

            var indexOfProduct = this._getIndexOfProductById(cart.cartItems, product.productId, product.variantId);

            if (indexOfProduct > -1) {
                amount = this._parseAmount(amount);
                cart.cartItems[indexOfProduct].amount = amount;
                this._setCart(cart);
            }
        };

        this.getProduct = function(productIdTuple) {
            var cartItem = this._getCartItemByProductIdTuple(productIdTuple);

            return cartItem && cartItem.product;
        };

        this._getCartItemByProductIdTuple = function(productIdTuple) {
            var cartItems = this.getCartItems();
            var indexOfProduct = this._getIndexOfProductById(cartItems, productIdTuple.productId, productIdTuple.variantId);

            if (indexOfProduct <= -1) {
                return null;
            }

            return cartItems[indexOfProduct];
        };
    });

})(require('jquery'), require('lodash'));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/shop/cart.js
 ** module id = 440
 ** module chunks = 96 97 232
 **/