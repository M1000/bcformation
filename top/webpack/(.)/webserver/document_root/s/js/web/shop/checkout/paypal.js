require('./paymentform.js');
require('../../scripts/bootstrap.js');
require('../../../plugins/sync/jquery.sync.js');

(function ($, jimdoData) {
    'use strict';

    var tr = jimdoData.tr.shop,
        $form;

    function goToPaypal() {
        var $submitStepOne = $('#cc-checkout-submit-2');
        var $coOptions = $('.cc-checkout-paymentoption');

        // Disable the submit button while the PayPal token is being fetched
        $submitStepOne.val(tr.checkoutSubmit.wait).attr('disabled', 'disabled');
        var country_and_state = $form.find('select').serializeArray();

        $.sync({
            url: '/j/checkout/doexpresscheckout',
            type: 'get',
            data: country_and_state,
            success: function(r) {
                if (r.status == 'success') {
                    // We have a token, so redirect to PayPal
                    window.location = r.paypalUrl;
                } else {
                    // Something went wrong, so display an error message and enable the submit button again
                    $coOptions.message(r.message, {status: 'error', autoHide: null});
                    $submitStepOne.val(tr.checkoutSubmit.next).removeAttr('disabled');
                }
            }
        });
    }

    $.modules.checkoutpaypal = function (params) {
        $form = $.modules.checkoutpaymentform.initForm();
        $.modules.checkoutpaymentform.bindPaymentMethod(32, goToPaypal);
    };
})(require('jquery'), require('jimdoData'));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/shop/checkout/paypal.js
 ** module id = 1537
 ** module chunks = 96 97
 **/