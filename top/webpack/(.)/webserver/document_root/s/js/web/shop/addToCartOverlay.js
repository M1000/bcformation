require('../scripts/registry.js');
require('./currencyFormatter.js');
require('../../libs/mustache/jquery.mustache.js');
require('../../plugins/actionController/jquery.actionController.js');


(function($, window, jimdoData) {
    "use strict";


    $.reg('AddToCartOverlayFactory', function ($) {
        this.create = function() {
            var currencyFormatter = $.factory('CurrencyFormatter', [jimdoData.shop.currencyLocale, jimdoData.shop.currencyFormat]);
            var cart = $.factory('ShoppingCartFactory').create();
            var windowLocation = window.location;

            var checkoutButtonAction = $.factory('CheckoutButtonAction', [cart, windowLocation]);


            return $.factory('AddToCartOverlay', [currencyFormatter, checkoutButtonAction]);
        };
    });

    $.reg('AddToCartOverlay', function ($, currencyFormatter, checkoutButtonAction) {
        var timeoutId;
        var $overlay;
        var $body = $('body');
        // new layout templates do set base styles (i.e. font-family) on the .jtpl-main element and no longer on the body
        var $templateBase = $('.jtpl-main:first').length > 0 ? $('.jtpl-main:first') : $body;


        /** @const */
        var ADD_TO_CART_RESPONSE_COLLAPSE_TIME_OUT = 10000;

        /** @const */
        var ADD_TO_CART_SLIDE_SPEED = 200;

        /** @const */
        var ADD_TO_CART_OPEN_IDENTIFIER_CLASS = 'j-shop-addtocard-is-open';


        /** @const */
        var OVERLAY_TEMPLATE = '<div class="cc-shop-addtocard-response j-shop-addtocard-response j-shop-addtocard-{{status}}">\
            <div class="j-shop-addtocard-response--message">\
                <p class="j-shop-addtocard-response--content">\
                    {{message}}\
                </p>\
            </div> <!-- j-shop-addtocard-response__message -->\
            <div class="j-shop-addtocard-response--item">\
                {{#imageUrl}}\
                    <img class="j-shop-addtocard-response--item-image" src="{{{imageUrl}}}"/>\
                {{/imageUrl}}\
                <div class="j-shop-addtocard-response--item-details">\
                    <p class="j-shop-addtocard-response--item-title">\
                        {{name}}\
                    </p>\
                    <p class="j-shop-addtocard-response--item--variant" data-item-field="variant">\
                        {{variantName}}\
                    </p>\
                    <p class="j-shop-addtocard-response--item--variant">\
                        {{shortDescription}}\
                    </p>\
                    <p class="j-shop-addtocard-response--item-price">\
                        {{priceFormatted}}\
                    </p> <!-- j-shop-addtocard-response__item-price -->\
                    <p class="j-shop-addtocard-response--item--variant">\
                        <small>{{basePrice}}</small>\
                    </p>\
                    <a rel="nofollow" href="{{productInfoLink.href}}" class="j-shop-addtocard-response--item--variant">\
                        {{{productInfoLink.label}}}\
                    </a>\
                </div> <!-- j-shop-addtocard-response__item-details -->\
            </div> <!-- j-shop-addtocard-response__item -->\
            <div class="j-shop-addtocard-response--actions">\
                <a class="j-shop-addtocard-response--backward is-close"\
                    href="javascript:;" data-action="close">\
                    {{continueShoppingText}}\
               </a>\
                <a class="j-shop-addtocard-response--forward is-checkout "\
                   href="javascript:;" data-action="checkout">\
                    <span class="j-cart-loading-spinner icon-web-loading-spinner"></span>\
                    <span class="j-shop-addtocard-response--forward-button-text">{{checkoutButtonText}}</span>\
               </a>\
               <a class="j-shop-addtocard-response--forward is-reload"\
                   href="javascript:;" data-action="reload">\
                   {{reloadPageText}}\
               </a>\
            </div>\
            <!-- j-shop-addtocard-response__actions -->\
        </div>';

        function clearOverlayCloseTimeout() {
            if (timeoutId) {
                window.clearTimeout(timeoutId);
                timeoutId = null;
            }
        }
        function texts() {
            return $.extend({
                checkoutButtonText: jimdoData.shop.checkoutButtonText
            }, jimdoData.tr.shop.addToCartOverlay);
        }

        this.init = function() {
        };

        this.success = function(product) {
            var partial = {
                    status: 'success',
                    message: texts().productInsertedText
                },
                markup = this._render(product, partial);

            this._show(markup);
        };

        this.error = function(product, message) {
            var partial = {
                    status: 'error',
                    message: message
                },
                markup = this._render(product, partial);

            this._show(markup);
        };

        this._render = function(product, partial) {
            return $.mustache(
                OVERLAY_TEMPLATE,
                $.extend({
                    priceFormatted: currencyFormatter.format(product.price),
                    basePrice: product.basicPrice > 0 ? product.basicPriceFormatted + ' / ' + product.basicPriceUnit : '',
                }, product, texts(), partial)
            );
        };

        this._show = function(markup) {
            var self = this;
            // in case the overlay is still there when the user tries to add another product
            $('.cc-shop-addtocard-response').remove();

            //hide element by default, so it can be animated on insert
            $overlay = $(markup).hide();

            // add and animate
            $body.addClass(ADD_TO_CART_OPEN_IDENTIFIER_CLASS);
            $templateBase.prepend($overlay);
            $overlay.slideDown(ADD_TO_CART_SLIDE_SPEED);

            $overlay.actionController({
                    _closeClick: function() {
                        self.close();
                    },
                    _reloadClick: this._reload,
                    _checkoutClick: function() {
                        clearOverlayCloseTimeout(); // clear timeout so overlay does not close, while still indicating progress (spinner)
                        uiAddProgressIndicator($(this));
                        checkoutButtonAction.goToCheckout();
                    }
                },
                {events: 'change click'}
            );

            clearOverlayCloseTimeout();
            timeoutId = window.setTimeout(function () {
                self.close();
            }, ADD_TO_CART_RESPONSE_COLLAPSE_TIME_OUT);


            function uiAddProgressIndicator($element) {
                $element.addClass('is-loading');
            }
        };

        this.close = function() {
            if (!$overlay || !$overlay.length) { // attempted closing although overlay was never shown before
                return;
            }
            clearOverlayCloseTimeout();
            $overlay.slideUp(ADD_TO_CART_SLIDE_SPEED, function() {
                $body.removeClass(ADD_TO_CART_OPEN_IDENTIFIER_CLASS);
                $(this).remove();
            });
        };

        this._reload = function() {
            window.location.reload();
        };
    });

})(require('jquery'), window, require('jimdoData'));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/shop/addToCartOverlay.js
 ** module id = 490
 ** module chunks = 96 97 232
 **/