require('../scripts/registry.js');

(function($, jimdoData, window) {
    "use strict";

    $.reg('CheckoutButtonAction', function ($, cart, windowLocation) {
        this.goToCheckout = function () {
            var cartData = cart.getCart();

            if (window.CookieControl.isCookieAllowed("fb_analytics")
                && jimdoData.isFacebookPixelIdEnabled
                && jimdoData.isFacebookPixelIdEnabled === true
                && typeof fbq !== 'undefined'
            ) {
                fbq('track', 'InitiateCheckout');
            }

            if (!cart.getCartItems().length) {
                windowLocation.href = '/j/checkout';
                return;
            }

            $.sync({
                url: '/j/start-checkout',
                type: 'post',
                contentType: 'application/json',
                data: JSON.stringify(cartData),
                complete: function(jqXHR) {
                    var redirectUrl = jqXHR.getResponseHeader('Location');

                    if (redirectUrl) {
                        windowLocation.href = redirectUrl;
                    }
                },
                dataType: 'json'
            });
        };
    });

})(require('jquery'), require('jimdoData'), window);



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/shop/checkoutButtonAction.js
 ** module id = 491
 ** module chunks = 96 97 232
 **/