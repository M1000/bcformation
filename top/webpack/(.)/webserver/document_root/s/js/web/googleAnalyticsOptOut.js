window.gaOptOut = function (gaUas) {
    var uas = gaUas || [];
    var optOutText = document.querySelector('.jtpl-ga-opt-out-text');
    var optOutNotification = document.querySelector('.jtpl-ga-opt-out-notification');

    if (window.CookieControl.isCookieAllowed('ga') || window.CookieControl.isCookieAllowed('jimdo_analytics')) {
        try {
            for (var i = 0; i < uas.length; i++) {
                var disableString = 'ga-disable-' + uas[i];
                document.cookie = disableString + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
                window[disableString] = true;
            }
            optOutNotification.className = optOutNotification.className.replace('hidden', ' ');
            optOutText.className = optOutNotification.className + 'hidden';
        } catch (err) {
            console.error('Could not add Google OptOut Cookies', err); // jshint ignore:line
        }
    }
};



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/googleAnalyticsOptOut.js
 ** module id = 3702
 ** module chunks = 96
 **/