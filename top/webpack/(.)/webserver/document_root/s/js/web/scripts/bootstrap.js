"use strict";
/**
 * Lazy loading of js and css files for web/cms modules and activating them
 */
require('../../plugins/xLazyLoader/jquery.xLazyLoader.js');

(function($, window){

    // Main object for all modules
    $.modules = {};

    var _jblob = window._jimBlob = window._jimBlob || [];

    var jimdoData = require('jimdoData'),
        $wnd = $(window),
        p = {},
        webAssetsReady = require('../scripts/webAssetsReady');

    if (jimdoData.isCMS) {
        p =  getPackagesConfiguration();
    } else {
        $(function() {
            var $likePlaceholder = $('.j-fb-placeholder-button');

            $likePlaceholder.on('click', function() {
                var language = jimdoData.cmsLanguage || 'en_US';
                var src = 'https://connect.facebook.net/' + language + '/all.js#xfbml=1&version=v2.5&appId=113869198637480'
                $likePlaceholder.parent().append("<script type='text/javascript' src='" + src + "'/>");
                $likePlaceholder.remove();
            });
        });
    }


    /**
     * @returns List of packages with their dependencies
     */
    function getPackagesConfiguration() {
        var packages = {};

        /* Facebook */
        packages.facebook = {
            'default': {
                js: (function() {
                    var language = jimdoData.cmsLanguage || 'en_US';
                    return [
                            'https://connect.facebook.net/' + language + '/all.js#xfbml=1&version=v2.5&appId=113869198637480'
                    ];
                })()
            }
        };

        return packages;
    }


    /*
     * Register module, lazy load dependences and run it.
     */
    $wnd.bind('register', function( e, type, name, params){

        if ( !params.variant ) {
            params.variant = 'default';
        }
        if ( p[name] && p[name][params.variant] ) {
            $.xLazyLoader($.extend({},p[name][params.variant], {
                success: function(){
                    queueManager.removeQueue(params, name);
                    // params.noLazyLoadInit - this param is for lazy load external js files that han mo module name
                    if (typeof $.modules[name] !== 'undefined') {
                        $.modules[name](params);
                    }
                },

                error: function(){
                    queueManager.removeQueue(params);
                    // add to the error array
                    queueManager.errs.push(name + '_LOAD_ERROR');
                },
                jsKey: jimdoData.cacheJsKey,
                cssKey: jimdoData.cacheCssKey
            }));
        }
        else{
            if (typeof $.modules[name] !== 'undefined') {
                $.modules[name](params);
            }
            queueManager.removeQueue(params);
        }
    });


    $.angularInitializationData = (function() {
        var angularParams = {};

        return {
            setValue: function(id, params) {
                if (id && params && params.initializationData) {
                    angularParams[id] = params.initializationData;
                }
            },
            getValue: function(id) {
                return angularParams[id];
            }
        };
    })();

    function regModule( type_name, params ) {
        var moduleId = params.id,
            elem;

        // remove script tag because it is confusing in case of drag&drop
        if (moduleId) {

            elem = document.getElementById('cc-m-reg-' + moduleId);
            if(elem && elem.parentNode) {
                elem.parentNode.removeChild( elem );
            }
        }

        // adding module to the queue
        queueManager.addModule(params, type_name);

        $.angularInitializationData.setValue(moduleId, params);

        type_name = type_name.split('_');
        $wnd.triggerHandler('register', [type_name[0], type_name[1], params]);
    }

    function executeRegModuleBuffer() {
        /**
         * Evaluate the regModule buffer on next tick
         * The buffer script itself is always included above the web.js
         */
        setTimeout(function () {
            var regBuf = window.__regModuleBuffer;
            if (regBuf) {
                for (var i = 0, len = regBuf.length; i < len; i += 1) {
                    var args = regBuf[i];
                    window.regModule.apply(window.regModule, args);
                }
            }
        });
    }

    // when async web.css should be used, wait for it to be loaded before executing module js
    // needed because some modules depend on styles provided by web.css
    // XXX: remove window.loadCss after a week (quickfix)
    if (jimdoData && !jimdoData.isCMS && !jimdoData.mobile && window.loadCss) {
        webAssetsReady.onReady(function () {
            window.regModule = regModule;
            executeRegModuleBuffer();
        });
    } else {
        window.regModule = regModule;
        executeRegModuleBuffer();
    }


    var queueManager = {
        modulesQueue:{},
        errs:[],
        uniqeIdAdd:1,
        uniqeIdRemove:1,
        checkLoop:0,
        loopInterval:200,
        loopsToCheck:40,

        addModule: function(params, type_name) {
            //unique id of the module
            var id = this.getId(params);
            // adding it to the queue
            this.modulesQueue[id] = type_name;
        },

        checkAllModulesLoaded: function() {
            // before first module added to the queue its empty, we might think it finished before even started
            var modulesStartedToRegisterToQueue = false,
                queueEmpty = true;
            this.checkLoop ++;

            for (var module in this.modulesQueue) {
                // ok, we have modules in queue
                modulesStartedToRegisterToQueue = true;
                // if the module not set to false - it didnt load
                if (this.modulesQueue[module]) {
                    // so we did not finish with loading all modules, out.
                    queueEmpty = false;
                    break;
                }
            }

            // if the queue empty after was full, we are done.
            if (queueEmpty && modulesStartedToRegisterToQueue) {

                // stop the loop
                this.checkLoop = 60;

                // all finished, fire event
                $(document).trigger('allModulesLoaded');
                return;
            }

            // if still looping
            if (this.checkLoop < this.loopsToCheck) {
                var self = this;

                setTimeout(function(){
                    self.checkAllModulesLoaded();
                }, this.loopInterval, self);

                return;
            }

            // full queue more than loopsToCheck times, we have a problem. fire event and report errors
            if (!queueEmpty) {

                for (var badModule in this.modulesQueue) {
                    if (this.modulesQueue[badModule]) {
                        this.errs.push(this.modulesQueue[badModule] + '_' + this.loopsToCheck + '_TIMES_NOT_LOADED');
                    }
                }

                // finished badly, fire event
                $(document).trigger('allModulesLoaded');

                if (this.errs.length){
                    if (jimdoData.isCMS) {
                        _jblob.push(['event', 'General Errors', 'unloaded modules', this.errs.toString()]);
                    }
                }
            }
        },

        getId: function(params, rm) {
            // trying to get unique the if
            var id = params.id || params.moduleId || (params.data ? params.data.id : 0) || 0;

            // if no unique id, lets give it one
            if (id === 0){
                id = rm ? this.uniqeIdRemove ++ : this.uniqeIdAdd ++;
            }

            return id.toString();
        },

        removeQueue: function(params) {
            //unique id of the module, telling that its the removal part
            var id = this.getId(params, true);
            // if its in the list of the registered modules, remove it, else, somehow its a module that loaded but did not registered
            if (this.modulesQueue[id]){
                this.modulesQueue[id] = false;
            } else {
                this.errs.push(name + '_CALLED_REMOVE_NOT_IN_LIST');
            }
            return id;
        }
    };

    // start checking for modules load on dom ready.
    $(function(){
        queueManager.checkAllModulesLoaded();
    });

})(require('jquery'), window);



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/scripts/bootstrap.js
 ** module id = 145
 ** module chunks = 96 97 232
 **/