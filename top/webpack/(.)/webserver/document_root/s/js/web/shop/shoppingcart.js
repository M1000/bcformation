// New template
require('./cart.js');
require('numeral/numeral.js');
require('./currencyFormatter.js');
require('./checkoutButtonAction.js');
require('./addToCartOverlay.js');
require('./newTemplateCartUI.esnext.js');
require('./cartCalculator.js');
require('./addToCartEventHandler.js');
// Old template
require('../scripts/registry.js');

(function($, jimdoData) {
    "use strict";

    function createShoppingCart() {
        return $.factory('ShoppingCartFactory', []).create();
    }

    function createAddToCartEventHandler(lazyLoadParams) {
        var shoppingCart = createShoppingCart();
        var currencyFormatter = $.factory('CurrencyFormatter', [jimdoData.shop.currencyLocale, jimdoData.shop.currencyFormat]);
        var checkoutButtonAction = $.factory('CheckoutButtonAction', [shoppingCart, window.location]);

        var addToCartOverlay = $.factory('AddToCartOverlayFactory').create();
        var newTemplateCartUI = $.factory('NewTemplateCartUI', [currencyFormatter, lazyLoadParams.tr.cart, checkoutButtonAction]);
        var cartCalculator = $.factory('CartCalculator', []);

        return $.factory('AddToCartEventHandler', [shoppingCart, addToCartOverlay, newTemplateCartUI, cartCalculator]);
    }

$.modules.shoppingcart = function (lazyLoadParams) {

    var cartItems,
        cartEventHandler;

    cartItems = createShoppingCart().getCartItems();
    cartEventHandler = createAddToCartEventHandler(lazyLoadParams);
    cartEventHandler.showShoppingCart(cartItems);
};

})(require('jquery'), require('jimdoData'));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/shop/shoppingcart.js
 ** module id = 1241
 ** module chunks = 96 97 232
 **/