var $ = require('jquery');

/**
 * we add an id to web.css when it is loaded async when the onload handler fires
 * also the media attribute is set to `all` onload in order to evaluate the css
 * this is the moment where we think that the styles are applied and dependent
 * javascript should be executed
 *
 * @param {function} callback
 */
function onWebAssetsReady(callback) {
    // wait for web.css to be loaded
    var $webCss = $('#jimdo_web_css[media=all]');

    // invoke callback and stop polling
    if ($webCss.length) {
        return callback();
    }

    // check again on next tick
    window.setTimeout(function() {
        onWebAssetsReady(callback);
    });
}

module.exports = {
    onReady: onWebAssetsReady
};



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/scripts/webAssetsReady.js
 ** module id = 1240
 ** module chunks = 96 97 232
 **/