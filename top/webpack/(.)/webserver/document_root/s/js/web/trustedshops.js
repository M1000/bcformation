(function ($) {
    "use strict";

    $(document).ready(function() {
        var $el = $('.reviews');
        if ($el.length === 1 && $el.attr('id').indexOf('tsbadge') === 0) {
            $el.attr('style', $el.attr('style') + '; z-index: 100 !important;');
        }
    });
})(require('jquery'));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/trustedshops.js
 ** module id = 3714
 ** module chunks = 96
 **/