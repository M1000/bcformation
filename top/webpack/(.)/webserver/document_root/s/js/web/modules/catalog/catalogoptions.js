jQuery.reg('CatalogOptions', function($) {
    "use strict";

    var self = this;

    this.fixMargin = function($container, margin) {
        var $products = $('.cc-webview-product', $container),
            columns;

        if (!$products.length) {
            return self;
        }

        margin = parseInt(margin, 10);

        $products.css('margin-right', margin);

        if (!$container.hasClass('cc-catalog-galleryview')) {
            return self;
        }

        columns = countNumberOfProductsPerRow(
            $container.width(),
            $products.outerWidth(),
            margin
        );

        $products.filter(':nth-child(' + columns + 'n)').css('margin-right', 0);

        return self;
    };

    this.initSlider = function($container) {
        var $products, $wrapper, $gutter, $handles, $leftHandle, $rightHandle,
            contentWidth, productWidth, productMargin, slideWidth, gutterWidth, columns, margin,
            animating = false,
            handleWidth = 32,
            inactiveClass = 'cc-catalog-slidehandle-inactive';

        if (!$container.hasClass('cc-catalog-sliderview')) {
            return self;
        }

        $products = $('.cc-webview-product', $container);
        $wrapper = $('.cc-catalog-wrapper', $container).css('margin', 0);
        $gutter = $('.cc-catalog-gutter', $container).width('auto').css('left', '0');
        $handles = $('.cc-catalog-slidehandle', $container);

        fixWidth($container);

        $leftHandle = $handles.filter('.cc-catalog-slidehandle-left');
        $rightHandle = $handles.filter('.cc-catalog-slidehandle-right');
        contentWidth = $container.width() - handleWidth * 2;
        productWidth = $products.outerWidth();
        productMargin = parseInt($products.css('margin-right'), 10);
        gutterWidth = (productWidth + productMargin) * $products.length;

        columns = countNumberOfProductsPerRow(contentWidth, productWidth, productMargin);

        $leftHandle.addClass(inactiveClass);
        $rightHandle.toggleClass(inactiveClass, $products.length <= columns);

        slideWidth = columns * (productWidth + productMargin);

        margin = contentWidth - (slideWidth - productMargin);

        $wrapper.css('margin', '0 ' + (margin / 2 + handleWidth) + 'px');
        $gutter.width(gutterWidth + Math.max(0, margin));

        $container
            .actionController('destroy')
            .actionController({
                _slideClick: function(e, dir) {
                    if (animating || $(this).hasClass(inactiveClass)) {
                        return;
                    }

                    animating = true;

                    $gutter.animate({left: dir + '=' + slideWidth}, function() {
                        var left = parseInt($gutter.css('left'), 10),
                            leftRule = left >= 0,
                            rightRule = left <= slideWidth - gutterWidth;

                        $leftHandle.toggleClass(inactiveClass, leftRule);
                        $rightHandle.toggleClass(inactiveClass, rightRule);

                        animating = false;
                    });
                }
            }, {events: 'click'});

        return self;
    };

    function countNumberOfProductsPerRow(contentWidth, productWidth, margin) {
        var size = 0,
            columns = 0;

        while (size < contentWidth) {
            size += productWidth;

            if (size > contentWidth) {
                break;
            }

            columns++;
            size += margin;
        }

        return Math.max(1, columns);
    }

    function fixWidth($container) {
        // XXX: Work around a Firefox 16 bug. When a row of floating content is exactly as wide as the container, the last item may
        // wrap to the next row for some reason. This behaviour is suppressed if the container's width is set explicitly.
        $container.width('auto').width($container.width());
    }
});



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/modules/catalog/catalogoptions.js
 ** module id = 624
 ** module chunks = 5 96 97 232
 **/