import Cookies from 'js-cookie';

const ONE_YEAR = 365;
const cookiePrefix = 'ckies_';

// eslint-disable-next-line import/prefer-default-export
export class CookieControl {

    deny(key) {
        Cookies.remove(cookiePrefix + key);
    }

    allow(key) {
        Cookies.set(cookiePrefix + key, 'allow', { path: '/', expires: ONE_YEAR });
    }

    isCookieAllowed(key) {
        const cookieKey = cookiePrefix + key;
        const cookieValue = Cookies.get(cookieKey);
        return cookieValue === 'allow';
    }

    isPOWrAllowed() {
        return this.isCookieAllowed('powr');
    }

    showCookieSettings(cookieKey) {
        if (typeof this.showCookieSettingsHandler === 'function') {
            this.showCookieSettingsHandler(cookieKey);
        }
    }

    onShowCookieSettings(showCookieSettingsHandler) {
        this.showCookieSettingsHandler = showCookieSettingsHandler;
    }
}



/** WEBPACK FOOTER **
 ** ./webserver/document_root/s/js/web/modules/cookieControl/CookieControl.esnext.js
 **/