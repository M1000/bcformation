(function($, _) {
    "use strict";

    /**
     * @see cartCalculatorSpec.js
     */
    $.reg('CartCalculator', function() {

        this.countProducts = function(cartItems) {
            var count = 0;

            for (var i = 0; i < cartItems.length; i++) {
                count += cartItems[i].amount;
            }

            return count;
        };

        this.calculateSubtotal = function(cartItems) {
            return _.reduce(cartItems, function(sum, item) {
                return sum + item.product.price * item.amount;
            }, 0);
        };
    });

})(require('jquery'), require('lodash'));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/shop/cartCalculator.js
 ** module id = 685
 ** module chunks = 96 97 232
 **/