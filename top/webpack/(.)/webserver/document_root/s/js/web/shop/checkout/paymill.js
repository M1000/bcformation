require('./paymentform.js');
require('plugins/xLazyLoader/jquery.xLazyLoader.js');

(function ($, window) {
    "use strict";

// The naming must be PAYMILL_PUBLIC_KEY, it is used in the paymill bridge
    window.PAYMILL_PUBLIC_KEY = '';

    var messages,
        $form,
        $nextStepButton;

    /* global paymill */
    function retrieveToken() {

        if (!$("#paymill_token").val()) {

            paymill.createTokenViaFrame({
                amount_int: $form.find('.card-amount-int').val(),
                currency:   $form.find('.card-currency').val()
            }, function(error, result) {

                if (error) {
                    // Token could not be created, handle error.
                    $nextStepButton.prop("disabled", false);
                } else {
                    $nextStepButton.prop("disabled", true);
                    var token = result.token;

                    // Token in das Formular einfügen, damit es an den Server übergeben wird
                    $form.append("<input type='hidden' id='paymill_token' name='paymill_token' value='" + token + "'/>");
                    $.modules.checkoutpaymentform.submit();
                }
            });
        }
    }

    $.modules.checkoutpaymill = function (params) {
        $(function() {
            $form = $.modules.checkoutpaymentform.initForm();
            $nextStepButton = $('#cc-checkout-submit-2');

            // submitting the form before the iframe is initialized causes token errors, when paymil is chosen as payment method
            // therefore we disable the button, until the paymill bridge js is loaded
            $nextStepButton.prop('disabled', true);

            $.modules.checkoutpaymentform.bindPaymentMethod(256, function(e) {
                e.preventDefault();
            });

            $.xLazyLoader({
                js: ["https://bridge.paymill.com/dss3"],
                jsKey: null,
                success: function() {
                    messages = params.messages;
                    window.PAYMILL_PUBLIC_KEY = params.publicKey;
                    $.modules.checkoutpaymentform.bindPaymentMethod(256, retrieveToken);
                    // Embed the credit card frame.
                    paymill.embedFrame('credit-card-fields', {
                        lang: params.paymillLanguageCode
                    }, function(error) {
                    });
                },
                error: function() {

                    $.modules.checkoutpaymentform.bindPaymentMethod(256, function() {
                        $.modules.checkoutpaymentform.submit();
                    });
                },
                complete: function () {
                    $nextStepButton.prop('disabled', false);
                }
            });
        });
    };
    module.exports = $.modules.checkoutpaymill;
})(require('jquery'), window);



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/shop/checkout/paymill.js
 ** module id = 1536
 ** module chunks = 96 97
 **/