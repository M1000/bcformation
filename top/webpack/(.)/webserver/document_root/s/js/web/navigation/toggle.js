require('jquery')(function($) {
  'use strict';
  $.each([0,1,2], function(index, i) {
    var navActiveItem = $('.jmd-nav__list-item-' + i + '> .jmd-nav__toggle-button');
    navActiveItem.on('click', function() {
      var currentId = $(this).parent().attr('id');
      $('.jmd-nav__list-item-'+i+':not(#'+currentId+')').removeClass('jmd-nav__item--last-opened');
      $(this).parent().toggleClass('jmd-nav__item--last-opened');
    });
  });
});



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/navigation/toggle.js
 ** module id = 1531
 ** module chunks = 96 232
 **/