require('../../webpackInitPublicPath.js');
require('../../plugins/message/jquery.message.js');
require('../../plugins/xLazyLoader/jquery.xLazyLoader.js');

(function($, window, jimdoData) {
    "use strict";

    var _jblob = window._jimBlob = window._jimBlob || [];

    // Globally available logging function for cucumber tests - empty function for everyone else
    window._jmdlg = $.noop;

    $.ajaxSetup({
        timeout: 60000,
        error: function(r) {
            if(r.channel) {
                window._jmdlg(r.responseText, r.channel.name);
            }

            onLoadError(r.status);
        }
    });

    (function(){
        var $ajax = $.ajax,
            data = $.param({
                cstok: jimdoData.cstok,
                websiteid: jimdoData.websiteId,
                pageid: jimdoData.pageId
            });

        $.ajax = function(s) {
            var contentType = s.contentType || '';
            var isContentTypeFormUrlEncoded;

            // add default data to each post request
            if ((s.type || $.ajaxSettings.type).toUpperCase() === 'POST') {

                // default s.contentType is application/x-www-form-urlencoded
                // sometimes it also contains a charset e.g. application/x-www-form-urlencoded; charset=UTF-8
                // so if it starts with application/x-www-form-urlencoded, we consider it a form url encoded request
                isContentTypeFormUrlEncoded = contentType.split(';')[0] === 'application/x-www-form-urlencoded';

                // so if contentType is not set or form url encoded, add custom jimdo params
                if (!s.contentType || isContentTypeFormUrlEncoded) {
                    if (typeof s.data === 'object') {
                        s.data = $.param(s.data);
                    }
                    s.data = s.data ? s.data + '&' : '';
                    s.data += data;
                }
            }
            return $ajax(s);
        };

    })();

    // setup xLazyLoader
    $.extend($.xLazyLoader.defaults,{
        error: function(e){
            if (jimdoData.isCMS) {
                _jblob.push(['event', 'General Errors', 'xLazyloader error', String(e)]);
            }
        },
        jsKey: jimdoData.cacheJsKey,
        cssKey: jimdoData.cacheCssKey
    });

    // setup message
    $.extend($.fn.message.defaults, {
        labels: {
            ok: jimdoData.tr.common.ok,
            abort: jimdoData.tr.common.abort
        }
    });

    // Don't show alert if load error caused by page reload
    var windowUnloaded;
    $(window).unload(function(){
        windowUnloaded = true;
    });

    // report error message
    function onLoadError(status) {
        if(windowUnloaded) {
            return;
        }
        if (jimdoData.isCMS) {
            _jblob.push(['event', 'General Errors', 'timeout 60 secs', status]);
        }
    }

    (function() {
        // Some console log to file functionality for Cucumber tests (custom bogus user agent)
        if (!jimdoData.isCucumber) {
            return;
        }

        // Only in CMS for now
        if (!jimdoData.isCMS) {
            return;
        }

        $(function() {
            window.loaded_for_cucumber = true;
        });

        /* global netscape, Components */
        window._jmdlg = function(message, title) {
            netscape.security.PrivilegeManager.enablePrivilege('UniversalBrowserAccess UniversalXPConnect');

            var file = Components.classes['@mozilla.org/file/local;1'].createInstance(Components.interfaces.nsILocalFile);
            // _jmdlgPath comes from Cucumber
            file.initWithPath(window._jmdlgPath);

            if (!file.exists()) {
                file.createUnique(Components.interfaces.nsIFile.NORMAL_FILE_TYPE, -1);
            }

            var ostream = Components.classes['@mozilla.org/network/file-output-stream;1'].createInstance(Components.interfaces.nsIFileOutputStream);
            // Append to file
            ostream.init(file, 0x10, -1, 0);

            title = title || '';
            var msg = '[' + (new Date()).toLocaleString() + '] ' + title + '\n' + message + '\n';

            var converter = Components.classes['@mozilla.org/intl/converter-output-stream;1'].createInstance(Components.interfaces.nsIConverterOutputStream);
            converter.init(ostream, 'UTF-8', 0, 0);
            converter.writeString(msg);
            converter.close();
        };
    })();

    // add ie classes to body
    $(function(){
        if($.browser.msie) {
            $(document.body).addClass( 'ie ie-'+parseInt($.browser.version, 10) );
        }
    });

    // isMobile - returns true/false if we are in a mobile view mode
    jimdoData.isMobile = function() {
        return !!jimdoData.mobile;
    };

    jimdoData.isDebug = /debug/.test(location.search);

    // Tell the CMS preview window our current URL (cross domain policy forbids it from accessing it directly)
    window.top.postMessage(
        {
            location: window.location.href,
            pageId: jimdoData.pageId
        },
        '*'
    );

})(require('jquery'), window, require('jimdoData'));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/scripts/setup.js
 ** module id = 594
 ** module chunks = 96 97 232 235
 **/