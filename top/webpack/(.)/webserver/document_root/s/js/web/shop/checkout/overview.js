(function($) {
    "use strict";

    $.modules.checkoutoverview = function () {
        var controller = {
            _processOrderSubmit: function() {
                var error = false;
                var $container = $('#cc-checkout-termsoftrade');

                ['agb', 'selldigitalgoods', 'sellservices', 'withdrawal', 'dataPrivacyForShipping'].forEach(function(value) {
                    var $item = $container.find('label[for="' + value + '"]');
                    var $input = $item.find('input#' + value);

                    if ($input) {
                        var isUnchecked = $input.prop('checked') === false;
                        $item.toggleClass('error-missing', isUnchecked);

                        if (!error && isUnchecked) {
                            error = true;
                        }
                    }
                });

                if (error !== false) {
                    return false;
                }

                // prevent form from being submitted multiple times
                $('.cc-checkout-btn, .j-checkout__button').prop('disabled', true);
            }
        };

        $('#cc-checkout-wrapper').actionController(controller, {events: 'submit'});
    };

    $(document).ready(function() {
        // scroll warning message into view if it exists
        if ($('.j-checkout__warning-message').length) {
            $(document).scrollTo('.j-checkout__warning-message', {duration: 'slow'});
        }
    });

})(require('jquery'));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/shop/checkout/overview.js
 ** module id = 1535
 ** module chunks = 96 97
 **/