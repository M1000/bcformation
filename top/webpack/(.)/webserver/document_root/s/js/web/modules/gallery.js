require('../../plugins/pinterest/jquery.pinterest.js');
require('bxslider');

(function($, jimdoData, EnlargeableGallery){
    "use strict";

    var $window = $(window);

    $.extend( $.modules, {
        gallery: gallery,
        flickr: gallery
    });

    function gallery ( opts ) {
        var pinterestOption = opts.options && opts.options.pinterest && opts.options.pinterest == '1',
            $module = $(opts.selector),
            // This feature used to always be enabled, which means if the parameter does not exist it should be treated
            // as enabled
            isEnlargeable = typeof opts.enlargeable === 'undefined' || opts.enlargeable == 1;

        switch ( opts.variant ) {
            case 'cool':
                var coolSize = opts.coolSize,
                    coolPadding = opts.coolPadding,
                    moduleWidth = $module[0].getBoundingClientRect().width;

                $module.on('galleryresize', function(e, payload) {
                    if (payload && typeof payload.coolSize !== 'undefined' && typeof payload.coolPadding !== 'undefined') {
                        coolSize = payload.coolSize;
                        coolPadding = payload.coolPadding;
                    }

                    calculateCoolSize($module, coolSize, coolPadding);
                });

                $window.on('resize', function() {
                    if (moduleWidth !== $module[0].getBoundingClientRect().width) {
                        moduleWidth = $module[0].getBoundingClientRect().width;
                        calculateCoolSize($module, coolSize, coolPadding);
                    }
                });

                calculateCoolSize($module, coolSize, coolPadding);

                if (isEnlargeable) {
                    new EnlargeableGallery($module, {
                        sharing: pinterestOption ? ['pinterest'] : null
                    });
                }

                break;
            case 'stack':
                var stackCount = opts.stackCount,
                    stackPadding = opts.stackPadding,
                    moduleWidth = $module[0].getBoundingClientRect().width;

                $module.on('galleryresize', function(e, payload) {
                    if (payload && typeof payload.stackCount !== 'undefined' && typeof payload.stackPadding !== 'undefined') {
                        stackCount = payload.stackCount;
                        stackPadding = payload.stackPadding;
                    }

                    calculateStacks($module, stackCount, stackPadding);
                });

                $window.on('resize', function() {
                    if (moduleWidth !== $module[0].getBoundingClientRect().width) {
                        moduleWidth = $module[0].getBoundingClientRect().width;
                        calculateStacks($module, stackCount, stackPadding);
                    }
                });

                calculateStacks($module, stackCount, stackPadding);

                if (isEnlargeable) {
                    new EnlargeableGallery($module, {
                        sharing: pinterestOption ? ['pinterest'] : null
                    });
                }

                break;
            case 'slider':
                var autostart = opts.autostart,
                    pause = opts.pause,
                    showSliderThumbnails = opts.showSliderThumbnails;

                $module.on('galleryresize', function() {
                    initSlider($module, autostart, pause, showSliderThumbnails);
                });

                $window.on('resize', function() {
                    initSlider($module, autostart, pause, showSliderThumbnails);
                });

                initSlider($module, autostart, pause, showSliderThumbnails);

                if (isEnlargeable) {
                    var gallery = new EnlargeableGallery($module, {
                        sharing: pinterestOption ? ['pinterest'] : null,
                        doNotBindClickHandler: true,
                        sliderPause: pause * 1000
                    });

                    $('.cc-m-gallery-slider-fullscreen', $module).click(function() {
                        gallery.openGallery($('[aria-hidden="false"] a', $module).data('index'));
                    });
                }

                break;
            case 'flickr':
                new EnlargeableGallery($module, {
                    sharing: pinterestOption ? ['pinterest'] : null
                });
                break;
            // gallery
            default:
                if (isEnlargeable) {
                    new EnlargeableGallery($module, {
                        sharing: pinterestOption ? ['pinterest'] : null
                    });
                }
        }
    }

    function calculateCoolSize($module, coolSize, coolPadding) {
        var $images = $('img', $module),
            numImages = $images.length,
            // Use bounding client rect because it considers decimal points which are relevant in containers with
            // relative widths (concerns some templates and the columns module)
            moduleWidth = $module[0].getBoundingClientRect().width,
            minHeight = getMinHeight(coolSize),
            maxHeight = getMaxHeight(coolSize),
            imagesPerRow = getImagesPerRow(coolSize, moduleWidth),
            currentImagesPerRow = imagesPerRow,
            i = 0,
            tooTall = false,
            noFullWidthForLastRow = false,
            previousHeight;

        coolPadding = parseInt(coolPadding, 10);

        while (i < numImages) {
            var ratioSum = 0,
                rowWidth = 0,
                $imagesInRow = $(),
                $image,
                height;

            for (var j = 0; j < currentImagesPerRow; j++) {
                $image = $images.eq(i + j);

                if (!$image.length) {
                    currentImagesPerRow = j;
                    break;
                }

                ratioSum += $image.data('orig-width') / $image.data('orig-height');
                $imagesInRow = $imagesInRow.add($image);
            }

            height = calculateHeight(moduleWidth, currentImagesPerRow, coolPadding, ratioSum);

            // If the images would end up too tall, try again with more images in the row - unless we're on the last
            // row, in that case use the height of the previous row (or the maximum height, if there is no previous row)
            // and leave the rest of the horizontal space unused.
            if (height > maxHeight) {
                if ($image.length) {
                    tooTall = true;
                    currentImagesPerRow++;
                    continue;
                } else {
                    height = previousHeight || maxHeight;
                    noFullWidthForLastRow = true;
                }
            }

            // If the images would end up too small, try again with fewer images in the row - unless we already tried
            // that and the images were too tall, in that case simply accept the smaller than minimum height.
            if (height < minHeight) {
                if (!tooTall && currentImagesPerRow > 1) {
                    currentImagesPerRow--;
                    continue;
                }
            }

            $imagesInRow.each(function(key) {
                var $image = $(this);

                $image
                    .height(height)
                    // Round down width manually because otherwise rounding errors will inevitably occur, making it
                    // impossible to keep all rows the same width.
                    .width(Math.floor($image.data('orig-width') / $image.data('orig-height') * height))
                    .css('margin-bottom', coolPadding);

                if (key < $imagesInRow.length - 1) {
                    $image.css('margin-right', coolPadding);
                }

                rowWidth += $image.outerWidth(true);

                // If due to rounding the images don't perfectly align to the right, add the difference to the last
                // image in the row, stretching it slightly but keeping all rows the same width.
                var widthDifference = moduleWidth - rowWidth;
                if (key === $imagesInRow.length - 1 && !noFullWidthForLastRow && widthDifference !== 0) {
                    $image.width($image.width() + widthDifference);
                }
            });

            i += currentImagesPerRow;
            currentImagesPerRow = imagesPerRow;
            previousHeight = height;
            tooTall = false;
        }

        $module.trigger('galleryresizedone');

        // If during calculation the module width changed, do it all again. This can happen in variable width layouts,
        // where due to the row-wise nature of the calculation a scrollbar might appear in the process, causing the
        // width to change.
        if ($module[0].getBoundingClientRect().width !== moduleWidth) {
            calculateCoolSize($module, coolSize, coolPadding);
        }
    }

    function calculateHeight(containerWidth, imagesPerRow, padding, ratioSum) {
        var result;

        // The container width represents how many pixels the images can take up horizontally. Subtract the space that
        // will be taken up by the padding between the images.
        containerWidth -= (imagesPerRow - 1) * padding;

        // Divide the container width by the sum of ratios of all the images that will go in the row to get the height
        // that all images must share to fit exactly into the container.
        result = containerWidth / ratioSum;

        // Round down the resulting height, otherwise images might break to the next line due to browser rounding errors.
        return Math.floor(result);
    }

    function getMinHeight(coolSize) {
        var minHeight;

        switch (parseInt(coolSize, 10)) {
            case 1:
                minHeight = 80;
                break;

            case 2:
                minHeight = 120;
                break;

            default:
                minHeight = 200;
                break;
        }

        return minHeight;
    }

    function getMaxHeight(coolSize) {
        var maxHeight;

        switch (parseInt(coolSize, 10)) {
            case 1:
                maxHeight = 200;
                break;

            case 2:
                maxHeight = 300;
                break;

            default:
                maxHeight = 400;
                break;
        }

        return maxHeight;
    }

    function getImagesPerRow(coolSize, moduleWidth) {
        var medianWidth;

        switch (parseInt(coolSize, 10)) {
            case 1:
                medianWidth = 125;
                break;

            case 2:
                medianWidth = 250;
                break;

            default:
                medianWidth = 400;
                break;
        }

        return Math.max(1, Math.floor(moduleWidth / medianWidth));
    }

    function calculateStacks($module, stackCount, stackPadding) {
        stackPadding = parseInt(stackPadding, 10);

        var $items = $('.cc-m-gallery-stack-item', $module),
            // Use bounding client rect because it considers decimal points which are relevant in containers with
            // relative widths (concerns some templates and the columns module)
            moduleWidth = $module[0].getBoundingClientRect().width,
            $columns = createColumns($module, $items, stackCount, stackPadding),
            columnWidth = $columns.width(),
            columnHeights = initColumnHeights(stackCount),
            i, j;

        for (i = 0; i < $items.length; i++) {
            var columnIndex = getSmallestColumn(columnHeights),
                $column = $columns.eq(columnIndex).show(),
                $item = $items.filter('[data-sort="' + i + '"]'),
                $image = $('img', $item),
                imageHeight = Math.floor($image.data('orig-height') / $image.data('orig-width') * columnWidth);

            $image.height(imageHeight);

            $item
                .css('margin-bottom', stackPadding)
                .appendTo($column);

            columnHeights[columnIndex] += imageHeight + stackPadding;
        }

        $module.trigger('galleryresizedone');

        // If during calculation the module width changed, do it all again. This can happen in variable width layouts,
        // where due to the row-wise nature of the calculation a scrollbar might appear in the process, causing the
        // width to change.
        if ($module[0].getBoundingClientRect().width !== moduleWidth) {
            calculateStacks($module, stackCount, stackPadding);
        }
    }

    function createColumns($module, $items, stackCount, stackPadding) {
        var $columns = $('.cc-m-gallery-stack-column', $module),
            moduleWidth = Math.floor($module[0].getBoundingClientRect().width),
            columnWidth = moduleWidth / stackCount - stackPadding * (stackCount - 1) / stackCount,
            i;

        if ($columns.length) {
            $items.unwrap();

            // In case there are empty columns, the unwrap will leave them alone, so simply remove what's left
            $columns.remove();
        }

        for (i = 0; i < stackCount; i++) {
            $('<div class="cc-m-gallery-stack-column"></div>')
                .width(columnWidth)
                .css('margin-right', i < stackCount - 1 ? stackPadding : '')
                .appendTo($module);
        }

        return $('.cc-m-gallery-stack-column', $module);
    }

    function initColumnHeights(stackCount) {
        var columnHeights = [],
            i;

        for (i = 0; i < stackCount; columnHeights[i++] = 0);

        return columnHeights
    }

    function getSmallestColumn(columnHeights) {
        var smallestColumn = 0;

        for (var i = 0; i < columnHeights.length; i++) {
            if (columnHeights[i] < columnHeights[smallestColumn]) {
                smallestColumn = i;
            }
        }

        return smallestColumn;
    }

    function initSlider($module, autostart, pause, showSliderThumbnails) {
        var $list = $('ul', $module),
            $images = $('img', $list),
            $thumbnailContainer = $('.cc-m-gallery-slider-thumbnails-wrapper', $module),
            $pager = $('.cc-m-gallery-slider-thumbnails', $thumbnailContainer),
            $thumbnails = $('a', $thumbnailContainer),
            thumbnailWidth = $thumbnails.first().outerWidth(true),
            moduleWidth = $module[0].getBoundingClientRect().width,
            smallestHeight = $window.height() - 100,
            slider = $list.data('bxSlider');

        if (!$images.length) {
            $list.data('bxSlider', null);
            return;
        }

        if (slider) {
            // Destroying the slider completely removes the custom pager - work around this by cloning and reinserting it
            slider.destroySlider();
            $thumbnailContainer.scrollLeft(0);

            $module.off('contextmenu');
        }

        $module.on('contextmenu', function() {
            // Our users sleep better at night if a right click does nothing
            return false;
        });

        $images
            .each(function(key) {
                var $image = $images.eq(key),
                    height = $image.data('orig-height') / $image.data('orig-width') * moduleWidth,
                    title = $image.attr('alt');

                smallestHeight = Math.min(height, smallestHeight);

                if (title) {
                    // Implement our own captions because bxslider insists on title attributes (we don't want them because
                    // of the tooltips they create)
                    $image.after('<div class="bx-caption"><span>' + title + '</span></div>');
                }
            })
            .height(smallestHeight);

        $pager.width(thumbnailWidth * $thumbnails.length);

        var enlargeIcon = $('.cc-m-gallery-slider-fullscreen');
        var toggleEnlargeIconFor = function (element) {
            var rel = $(element).find('a').attr('rel');

            if(rel) {
                enlargeIcon.show();
                $('.bx-controls-auto').css('margin-left', '-43px');
            } else {
                enlargeIcon.hide();
                $('.bx-controls-auto').css('margin-left', '-15px');
            }
        };

        $list.bxSlider({
            slideMargin: 2,
            auto: true,
            autoStart: autostart == 1,
            autoControls: true,
            autoControlsCombine: true,
            captions: true,
            nextText: '',
            onSlideBefore: function ($newItem, __, newIndex) {
                setCaptionSize($newItem, moduleWidth);

                $thumbnailContainer.animate({
                    scrollLeft: newIndex * thumbnailWidth - moduleWidth / 2 + thumbnailWidth / 2
                }, 200);
            },
            onSlidePrev: function($slideElement) {
                toggleEnlargeIconFor($slideElement);
            },
            onSlideNext: function($slideElement) {
                toggleEnlargeIconFor($slideElement);
            },
            onSliderLoad: function() {
                var elem = $list.getCurrentSlideElement();
                toggleEnlargeIconFor(elem);
            },
            pager: showSliderThumbnails == 1,
            pagerCustom: showSliderThumbnails == 1 ? $pager : null,
            pause: pause * 1000,
            prevText: '',
            startText: '',
            stopAutoOnClick: true,
            stopText: ''
        });

        // bxSlider duplicates the first and the last image, including the surrounding markup. Remove the rel attribute
        // to exclude them from the lightbox
        $('.bx-clone a', $list).attr('rel', '');

        slider = $list.data('bxSlider');

        setCaptionSize(slider.getCurrentSlideElement(), moduleWidth);

        $module.trigger('galleryresizedone');
    }

    function setCaptionSize($item, moduleWidth) {
        var MARGIN = 12,
            captionWidth = Math.min(
                moduleWidth - MARGIN * 2,
                Math.max(300, $('img', $item).width() - MARGIN * 2)
            );

        $('.bx-caption', $item)
            .width(captionWidth)
            .css('margin-left', (moduleWidth - captionWidth) / 2);
    }
})(require('jquery'), require('jimdoData'), require('./enlargeable.js'));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/modules/gallery.js
 ** module id = 1232
 ** module chunks = 96 97 232
 **/