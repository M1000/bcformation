'use strict';

require('../../plugins/actionController/jquery.actionController.js');
var Cookies = require('js-cookie');
var cmsPosition = require('plugins/cmsPosition');
var $ = require('jquery');
var jimdoData = require('jimdoData');

var CC_COOKIE_LAW_HIDDEN_CLASS = 'cc-cookie-law-hidden';

function hideCookieLaw () {
    var ONE_YEAR = 365;
    var $cookieLaw = $('#cc-cookie-law');

    $cookieLaw.addClass(CC_COOKIE_LAW_HIDDEN_CLASS);
    Cookies.set('cookielaw', 'dontshow', {path: '/', expires: ONE_YEAR});
    cmsPosition.adjust();
}

$.modules.cookielaw = function() {
    if(jimdoData.isJimdoMobileApp) {
        return;
    }
    var $cookieLaw = $('#cc-cookie-law');
    $cookieLaw.actionController(new Controller(), {events: 'click'});

    if (!Cookies.get('cookielaw')) {
        $cookieLaw.removeClass(CC_COOKIE_LAW_HIDDEN_CLASS).show();

        cmsPosition
            .add($cookieLaw)
            .adjust(true);
    }

    function Controller() {
        this._enableClick = function() {
            hideCookieLaw();
            window.ckies.allow('functional');
            window.ckies.allow('performance');
            window.ckies.allow('marketing');
        };

        this._denyClick = function() {
            hideCookieLaw();
            window.ckies.deny('functional');
            window.ckies.deny('performance');
            window.ckies.deny('marketing');
        };
    }
};

$.modules.cookielaw.hideCookieLaw = hideCookieLaw;

module.exports = $.modules.cookielaw;


/** WEBPACK FOOTER **
 ** ./webserver/document_root/s/js/web/footer/cookielaw.esnext.js
 **/