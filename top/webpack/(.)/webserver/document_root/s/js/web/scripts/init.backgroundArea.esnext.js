/* eslint no-param-reassign: ["error", { "props": false }] */

require('@jimdo/jquery-background-area');
const $ = require('jquery');
const jimdoData = require('jimdoData');
const transformImageUrl = require('modules/transformImageUrl');
const targetBackgroundAreas = require('target-background-areas').default;

module.exports = function init(background) {
    let $bgArea = null;
    let currentBackground = background;
    const slideShowChangedCallbacks = $.Callbacks();
    const CHANGE_SLIDE_SHOW = `changeSlide${$.backgroundArea.CONSTANTS.EVENT_NAMESPACE}`;

    function getAPI(globalName, url) {
        if (window[globalName]) {
            return $.when(window[globalName]);
        }

        return $.getScript(url).then(() => {
            const d = $.Deferred();

            const intvl = window.setInterval(() => {
                if (window[globalName]) {
                    d.resolve(window[globalName]);
                    window.clearInterval(intvl);
                }
            }, 50);

            return d;
        });
    }

    $.backgroundArea.videoPlayers.youtube.getYT = () => {
        return getAPI('YT', '//www.youtube.com/iframe_api');
    };

    function showBackground(newBackground) {
        $bgArea.backgroundArea('show', newBackground);
    }

    function makeBackgroundImageResponsive(newBackground) {
        const widthLandscape = jimdoData.responsiveBreakpointLandscape;
        const widthPortrait = jimdoData.responsiveBreakpointPortrait;
        const mediaQueryLandscape = window.matchMedia(`(max-width: ${widthLandscape}px)`);
        const mediaQueryPortrait = window.matchMedia(`(max-width: ${widthPortrait}px)`);
        const ratio = window.devicePixelRatio || 1;

        newBackground.images.forEach((image) => {
            if (mediaQueryPortrait.matches) {
                image.url = transformImageUrl.bySize(
                    image.url,
                    Math.floor(widthPortrait * ratio)
                );
            } else if (mediaQueryLandscape.matches) {
                image.url = transformImageUrl.bySize(
                    image.url,
                    Math.floor(widthLandscape * ratio)
                );
            }
        });

        showBackground(newBackground);

        mediaQueryLandscape.addListener((mediaQuery) => {
            newBackground.images.forEach((image) => {
                if (!mediaQuery.matches) {
                    image.url = transformImageUrl.original(image.url);
                }
            });

            showBackground(newBackground);
        });

        mediaQueryPortrait.addListener((mediaQuery) => {
            newBackground.images.forEach((image) => {
                if (!mediaQuery.matches) {
                    image.url = transformImageUrl.bySize(
                        image.url,
                        Math.floor(widthLandscape * ratio)
                    );
                }
            });

            showBackground(newBackground);
        });
    }

    function updateBackgroundArea(newBackground) {
        currentBackground = newBackground;

        if ($bgArea) {
            if (window.matchMedia && newBackground && newBackground.images && newBackground.images.length) {
                makeBackgroundImageResponsive(newBackground);
            } else {
                showBackground(newBackground);
            }
        }
    }


    function updateBgFullscreen(isFullscreen) {
        $(() => {
            if ($bgArea) {
                $bgArea.backgroundArea('destroy');
                $bgArea.off(CHANGE_SLIDE_SHOW, slideShowChangedCallbacks.fire);
            }

            const backgroundAreas = targetBackgroundAreas(document);
            let bgArea = null;

            if (typeof isFullscreen !== 'undefined' && isFullscreen !== null) {
                bgArea = backgroundAreas.filter(backgroundArea => {
                    return backgroundArea.type === (isFullscreen ? 'fullscreen' : 'stripe');
                })[0];
            }

            $bgArea = $(bgArea ? bgArea.node : backgroundAreas[0].node);

            $bgArea.on(CHANGE_SLIDE_SHOW, slideShowChangedCallbacks.fire);
            $bgArea.backgroundArea();

            updateBackgroundArea(currentBackground);
        });
    }

    function jdbgaPlaySlideshow() {
        if ($bgArea) {
            $bgArea.backgroundArea('slideshow.play');
        }
    }
    function jdbgaPauseSlideshow() {
        if ($bgArea) {
            $bgArea.backgroundArea('slideshow.pause');
        }
    }
    function jdbgaSetCurrentSlideshowImage(index) {
        if ($bgArea) {
            $bgArea.backgroundArea('slideshow.setCurrentImage', index);
        }
    }
    function jdbgaSetSlideshowFocalPoint(x, y, index) {
        if ($bgArea) {
            $bgArea.backgroundArea('slideshow.setFocalPoint', x, y, index);
        }
    }
    function jdbgaSetPictureFocalPoint(x, y) {
        if ($bgArea) {
            $bgArea.backgroundArea('picture.setFocalPoint', x, y);
        }
    }

    updateBgFullscreen(jimdoData.bgFullscreen);

    return {
        jdbgaPlaySlideshow,
        jdbgaPauseSlideshow,
        jdbgaSetCurrentSlideshowImage,
        jdbgaSetSlideshowFocalPoint,
        jdbgaSetPictureFocalPoint,
        updateBackgroundArea,
        updateBgFullscreen,
        onChangeSlide(callback) {
            slideShowChangedCallbacks.add(callback);

            return () => {
                slideShowChangedCallbacks.remove(callback);
            };
        },
    };
};



/** WEBPACK FOOTER **
 ** ./webserver/document_root/s/js/web/scripts/init.backgroundArea.esnext.js
 **/