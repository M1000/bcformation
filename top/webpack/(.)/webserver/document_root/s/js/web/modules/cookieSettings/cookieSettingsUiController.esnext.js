import $ from 'jquery';
import { CookieSettingsUiListeners } from './cookieSettingsUiListeners.esnext';

// eslint-disable-next-line import/prefer-default-export
export class CookieSettingsUiController {
    constructor({ state, renderer, cookieControl }) {
        this.state = state;
        this.renderer = renderer;
        this.cookieControl = cookieControl;
    }

    init() {
        this.loadCookies();
        this.setCookieSettingsVisibility();
        this.renderer.init({ listeners: new CookieSettingsUiListeners(this) });
        this.showCookieSettings();
    }

    setCookieSettingsVisibility() {
        this.state.setShowCookieSettings(
            !this.cookieControl.isCookieAllowed('cookielaw') &&
            !this.isPageWithoutCookieSettings()
        );
    }

    isPageWithoutCookieSettings() {
        const url = window.location.href;
        return this.state
            .getPagesWithoutCookieSettings()
            .some(pageWithoutCookieBanner => {
                return url.indexOf(pageWithoutCookieBanner, url.length - pageWithoutCookieBanner.length) !== -1;
            });
    }

    loadCookies() {
        const allowedCookies = this.state.getAllCookieKeys()
            .filter(cookieKey => this.cookieControl.isCookieAllowed(cookieKey));
        this.state.selectCookies(allowedCookies);
    }

    loadCookieSettingsHtml() {
        return new Promise((resolve) => {
            $.get(this.state.getCookieSettingsHtmlUrl()).done(resolve);
        });
    }

    showCookieSettings(cookieKey) {
        if (!this.state.showCookieSettings) {
            return;
        }

        if (!this.state.getCookieSettingsHtmlLoaded()) {
            this.loadCookieSettingsHtml().then((html) => {
                this.renderer.insertHtml(html);
                this.state.setCookieSettingsHtmlLoaded(true);
                this.updateView(cookieKey);
            });
        } else {
            this.updateView(cookieKey);
        }
    }

    updateView(cookieKey) {
        if (!this.state.showCookieSettings) {
            this.renderer.close();
            return;
        }

        this.renderer.render({
            view: this.state.view,
            categories: this.state.getCategoryStateMap(),
            cookies: this.state.getCookieStateMap(),
        });

        this.renderer.open();

        if (cookieKey !== undefined) {
            this.renderer.navigateToSpecificCookie(cookieKey);
        }
    }

    persistCookieSelection(cookies) {
        this.state.getAllCookieKeys().forEach(cookie => this.cookieControl.deny(cookie));
        cookies.forEach(cookie => this.cookieControl.allow(cookie));
    }

    onAcceptAll() {
        this.state.selectCookies(this.state.getAllCookieKeysWithoutNecessary());
        this.persistCookieSelection(this.state.selectedCookies);
        this.reloadPage();
    }

    onAcceptSelected() {
        this.persistCookieSelection(this.state.selectedCookies);
        this.reloadPage();
    }

    onViewCategory(categoryName) {
        this.state.setView(categoryName);
        this.updateView();
    }

    onViewAllCategories() {
        this.state.setView(this.state.defaultView);
        this.updateView();
    }

    onToggleCookie(cookieKey, cookieValue) {
        this.state.toggleCookie(cookieKey, cookieValue);
        this.updateView();
    }

    onToggleCategory(categoryName, categoryValue) {
        this.state.toggleCategory(categoryName, categoryValue);
        this.updateView();
    }

    reloadPage() {
        location.reload();
    }
}



/** WEBPACK FOOTER **
 ** ./webserver/document_root/s/js/web/modules/cookieSettings/cookieSettingsUiController.esnext.js
 **/