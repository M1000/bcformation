import { CookieControl } from './CookieControl.esnext';

// eslint-disable-next-line import/prefer-default-export
export class CookieControlOff extends CookieControl {

    deny() {
    }

    allow() {
    }

    isCookieAllowed() {
        return true;
    }

    isPOWrAllowed() {
        return true;
    }
}



/** WEBPACK FOOTER **
 ** ./webserver/document_root/s/js/web/modules/cookieControl/CookieControlOff.esnext.js
 **/