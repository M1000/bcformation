require('../scripts/registry.js');

(function ($, numeral) {
    "use strict";

    /**
     * @see CurrencyFormatterSpec.js
     */
    $.reg('CurrencyFormatter', function ($, currencyLocale, currencyFormat) {
        this.init = function() {
            var language = {
                delimiters: {
                    thousands: currencyFormat.symbols.GROUPING_SEPARATOR,
                    decimal: currencyFormat.symbols.DECIMAL_SEPARATOR
                },
                currency: {
                    symbol: currencyFormat.symbols.CURRENCY_SYMBOL
                }
            };

            numeral.language(currencyLocale, language);
            numeral.language(currencyLocale);
        };

        this.format = function (totalAmount) {
            if (currencyLocale === 'en_IN') {
                return this.formatIndianRupie(totalAmount);
            }

            // we have to replace the space with a non breaking space
            return numeral(totalAmount).format(currencyFormat.convertedPattern);
        };

        // https://stackoverflow.com/questions/16037165/displaying-a-number-in-indian-format-using-javascript
        this.formatIndianRupie = function (totalAmount) {
            var x = totalAmount.toFixed(2);
            var afterPoint = '';
            if (x.indexOf('.') > 0) {
                afterPoint = x.substring(x.indexOf('.'), x.length);
                if (afterPoint.length < 3) {
                    afterPoint = afterPoint + '0';
                }
            } else {
                afterPoint = '.00';
            }
            x = Math.floor(x);
            x = x.toString();
            var lastThree = x.substring(x.length - 3);
            var otherNumbers = x.substring(0, x.length - 3);
            if (otherNumbers !== '') {
                lastThree = ',' + lastThree;
            }

            return 'Rs\u00a0' + otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
        };
    });

})(require('jquery'), require('numeral'));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/shop/currencyFormatter.js
 ** module id = 264
 ** module chunks = 96 97 232
 **/