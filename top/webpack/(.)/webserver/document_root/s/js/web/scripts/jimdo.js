
// we can't use the function form of 'use strict' here as long as we have lots of globals being defined outside a closuring function
"use strict";

// Modal Window -- to become successor/core of JimdoSigninWindow
var ModalWindow = window.ModalWindow = {

    rmiClose: function(theWindow, returnObject)
    {
        var callback = ModalWindow.callback;
        delete ModalWindow.callback;

        ModalWindow.returnValue = {};
        for (var prop in returnObject) {
            ModalWindow.returnValue[prop] = returnObject[prop];
        }

        theWindow.close();
        callback();
        delete ModalWindow.returnValue;
    },
    check: function()
    {
        if (ModalWindow.popup) {

            var closed = false;
            try {
                closed = ModalWindow.popup.closed;
            } catch (e) { }

            if (closed) {
                delete ModalWindow.popup;
                if (ModalWindow.callback) {
                    ModalWindow.callback();
                    delete ModalWindow.callback;
                }
            } else {
                setTimeout(ModalWindow.check, 800);
            }
        }
    },
    hide: function()
    {
        if (null !== ModalWindow.popup) {
            ModalWindow.popup.close();
            ModalWindow.popup = null;
        }
    },
    show: function(url, options, callback) {

        if (ModalWindow.popup) {
           ModalWindow.popup.focus();
           return;
        }
        delete ModalWindow.returnValue;
        ModalWindow.callback = callback;

        var defaultOptions = {
            height: 80,
            width: 80,

            directories: 'no',
            location: 'yes',
            menu: 'no',
            resizable: 'yes',
            scroll: 'yes',
            status: 'yes',
            title: 'yes',
            tools: 'no'
        };
        for (var key in options) {
            defaultOptions[key] = options[key];
        }

        var hasEventListener = ('undefined' === typeof window.attachEvent);
        var hasModalDialog   = ('undefined' !== typeof window.showModalDialog);
        var optionString;

        if (hasEventListener && hasModalDialog) {
            optionString  = 'dialogHeight:'+defaultOptions.height+'px;';
            optionString += 'dialogWidth:'+defaultOptions.width+'px;';
            optionString += 'resizable:'+defaultOptions.resizable+';';
            optionString += 'scroll:'+defaultOptions.scroll+';';
            optionString += 'status:'+defaultOptions.status;
            // alert(optionString);

            window.showModalDialog(url, null, optionString);
            if (ModalWindow.callback) {
                ModalWindow.callback();
            }
        } else {
            optionString  = 'height='+defaultOptions.height+',';
            optionString += 'width='+defaultOptions.width+',';
            optionString += 'directories='+defaultOptions.directories+',';
            optionString += 'location='+defaultOptions.location+',';
            optionString += 'menubar='+defaultOptions.menu+',';
            optionString += 'resizable='+defaultOptions.resizable+',';
            optionString += 'scrollbars='+defaultOptions.scroll+',';
            optionString += 'status='+defaultOptions.status+',';
            optionString += 'titlebar='+defaultOptions.status+',';
            optionString += 'toolbar='+defaultOptions.tools;
            // alert(optionString);

            ModalWindow.popup = window.open(url, 'ModalWindow', optionString);

            if (window.addEventListener) {
                window.addEventListener('unload', ModalWindow.hide, true);
            } else if (window.attachEvent) {
                window.attachEvent('unload', ModalWindow.hide);
            }
            setTimeout(ModalWindow.check, 800);
        }
    }
};

(function($, window) {
    window.changeCaptcha = function(captchaId) {
        var time = new Date(),
            $image = $('#captchaImage' + captchaId);

        $image.prop('src', '/app/common/captcha/index/captchaId/' + captchaId + '/t/' + time.getTime());
    };
 })(require('jquery'), window);
// Captcha



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/scripts/jimdo.js
 ** module id = 625
 ** module chunks = 3 96 97 232
 **/