require('../scripts/registry.js');

(function($, jimdoData, window) {
    "use strict";

    $.reg('AddToCartEventHandler', function($, shoppingCart, addToCartOverlay, newTemplateCartUI, cartCalculator) {

        this.init = function () {
            var self = this;

            $(window).on('addProductToCart.shop', function (e, data) {
                self.addToCart(data.cartItem);

                if (window.CookieControl.isCookieAllowed("fb_analytics")
                    && jimdoData.isFacebookPixelIdEnabled
                    && jimdoData.isFacebookPixelIdEnabled === true
                    && typeof fbq !== 'undefined'
                ) {
                    fbq('track', 'AddToCart', { content_type: 'product', content_id: data.cartItem.productId});
                }
            });

            $(window).on('pageshow', function (event) {
                /**
                 * When navigating back (e.g. by swiping left on the edge of a page), Safari keeps the entire state of the page.
                 * This can lead to the add to cart overlay still being open (still indicating progress), when you navigate back from the checkout.
                 * Same is true for the cart as well. The cart might still show the spinner, when a user navigates back on a mobile Safari.
                 *
                 * Reset overlay and cart to desired state
                 *
                 * @see https://developer.mozilla.org/en-US/Firefox/Releases/1.5/Using_Firefox_1.5_caching#pageshow_event
                 */
                if (event.originalEvent.persisted) { // event.persisted is not available in jQuery event object
                    self.closeOverlay();
                    self.showShoppingCart(shoppingCart.getCartItems());
                }

            });
        };

        this.addToCart = function(cartItem) {
            var cartItems;

            shoppingCart.addProduct(cartItem.product, cartItem.amount);

            cartItems = shoppingCart.getCartItems();
            this.showOverlay(cartItem.product);
            this.showShoppingCart(cartItems);
        };

        this.showOverlay = function(product) {
            addToCartOverlay.success(product);
        };

        this.closeOverlay = function() {
            addToCartOverlay.close();
        };

        this.showShoppingCart = function(cartItems) {
            var count = cartCalculator.countProducts(cartItems),
                subtotal = cartCalculator.calculateSubtotal(cartItems);

            newTemplateCartUI.show(count, subtotal);
        };

    });

})(require('jquery'), require('jimdoData'), window);



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/shop/addToCartEventHandler.js
 ** module id = 684
 ** module chunks = 96 97 232
 **/