/**
 * as far as I understand this needs to be a function.
 * Check the example at https://github.com/kof/jquery-actionController/blob/7251a08a10c613086fcec77c6df3c4b4dc2e976a/demo/index.html#L91
 * @param listeners
 * @param listeners.onAcceptAll
 * @param listeners.onAcceptSelected
 * @param listeners.onViewCategory
 * @param listeners.onViewAllCategories
 * @param listeners.onToggleCookie
 * @param listeners.onToggleCategory
 * @constructor
 */

// eslint-disable-next-line import/prefer-default-export
export function CookieSettingsUiListeners(listeners) {
    // eslint-disable-next-line no-underscore-dangle
    this._acceptAllClick = () => {
        listeners.onAcceptAll();
    };

    // eslint-disable-next-line no-underscore-dangle
    this._acceptSelectedClick = () => {
        listeners.onAcceptSelected();
    };

    // eslint-disable-next-line no-underscore-dangle
    this._viewCategoryClick = (event, categoryName) => {
        listeners.onViewCategory(categoryName);
    };

    // eslint-disable-next-line no-underscore-dangle
    this._viewAllCategoriesClick = () => {
        listeners.onViewAllCategories();
    };

    // eslint-disable-next-line no-underscore-dangle
    this._toggleCookieClick = (event, cookieKey) => {
        listeners.onToggleCookie(cookieKey, event.currentTarget.dataset.value);
    };

    // eslint-disable-next-line no-underscore-dangle
    this._toggleCategoryClick = (event, categoryName) => {
        listeners.onToggleCategory(categoryName, event.currentTarget.dataset.value);
    };
}



/** WEBPACK FOOTER **
 ** ./webserver/document_root/s/js/web/modules/cookieSettings/cookieSettingsUiListeners.esnext.js
 **/