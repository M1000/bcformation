require('../scripts/bootstrap.js');

/**
 * Twitter module
 *
 * @param string  opts.username  Twitter Username
 * @param int     opts.count     Number of Tweets to show
 *
 * @param string  opts.translation.errormessage Translated Error Message if Twitter is not responding
 *
 * @author SK, reworked by Oleg
 *
 */

(function ($, jimdoData) {
    "use strict";

    $.modules.twitter = function (opts) {
        if (!opts.count || !opts.username) {
            return false;
        }

        function init(opts) {
            $.ajax({
                url: getUrl(),
                data: {
                    screen_name: opts.username,
                    count: opts.count
                },
                success: messagesCallback,
                error: function (xhr) {
                    if (!opts.withinCms) {
                        show('');
                        return;
                    }

                    var error = $.parseJSON(xhr.responseText).error,
                        msg = error.message;

                    if (error.type === 'not_connected') {
                        msg = '<a href="' + jimdoData.systemUrl + '?jumpto=siteadmin%2Fsettings&amp;slideto=social%2Fmessage%2Ftwitter">' + msg + '</a>';
                    }

                    show(msg);
                }
            });

             function getUrl() {
                 return jimdoData.isCMS ? jimdoData.systemUrl + 'app/module/twitter/readtimeline' : '/app/module/webtwitter/readtimeline';
             }

             /**
              * Callback for Loaded Tweets
              *
              * @param messages array
              */
             function messagesCallback(messages) {
                 if (!$.isArray(messages)) {
                     show(opts.translation.errorMessage);
                     return;
                 }
                 var statusHTML = '';
                 $.each(messages, function (i, tweet) {
                     var username = tweet.user.screen_name;
                     var status = tweet.text.replace(/((https?|s?ftp|ssh)\:\/\/[^"\s<>]*[^.,;'">:\s<>\)\]!])/g,function (url) {
                         return '<a href="' + url + '">' + url + '</a>';
                     }).replace(/\B@([_a-z0-9]+)/ig, function (reply) {
                                    return reply.charAt(0) + '<a href="https://twitter.com/' + reply.substring(1) + '" target="_blank">' + reply.substring(1) + '</a>';
                                });
                     var url = 'https://twitter.com/#!/' + username + '/status/' + tweet.id_str;
                     statusHTML += '<li class="j-twitter--list-item cc-m-twitter--list-item"><span class="icon-web-twitter-bird"></span><span class="j-twitter--message-status cc-m-twitter--message-status">' + status + '</span> <a style="font-size:85%" href="' + url + '" target="_blank">' + getRelativeTime(tweet.created_at) + '</a></li>';
                 });
                 show(statusHTML);
             }

             /**
              * Calculate Relative Time
              *
              * @param time_value string like Wed Mar 26 17:38:53 +0000 2008
              */
             function getRelativeTime(time_value) {
                 var values = time_value.split(" ");
                 time_value = values[1] + " " + values[2] + ", " + values[5] + " " + values[3];
                 var parsed_date = Date.parse(time_value);
                 var relative_to = (arguments.length > 1) ? arguments[1] : new Date();
                 var delta = parseInt((relative_to.getTime() - parsed_date) / 1000, 10);
                 delta = delta + (relative_to.getTimezoneOffset() * 60);

                 if (delta < 60) {
                     return opts.translation.lessThanAMinute;
                 } else if (delta < 120) {
                     return opts.translation.aboutAMinute;
                 } else if (delta < 3600) { // 60*60
                     return opts.translation.minutes.replace('{minuten}', parseInt(delta / 60, 10));
                 } else if (delta < 7200) { // 120*60
                     return opts.translation.aboutAnHour;
                 } else if (delta < 86400) { // 24*60*60
                     return opts.translation.hours.replace('{stunden}', parseInt(delta / 3600, 10));
                 } else if (delta < 172800) { // 48*60*60
                     return opts.translation.oneDay;
                 } else {
                     return opts.translation.days.replace('{tage}', parseInt(delta / 86400, 10));
                 }
             }

             function show(html) {
                 try {
                     $(opts.selector).children().html(html);
                 } catch(e) {
                     // for IE 9
                 }
                 var $module = $('#cc-m-' + opts.id);
                 $module.trigger('moduleresize', opts.id);
             }
        }

        var $module = $('#cc-m-twitter-' + opts.id);


        if (jimdoData.isCMS) {
            var $iframe = $('<iframe>', {
                src: '//platform.twitter.com/widgets/follow_button.html?screen_name=' + opts.username + '&show_count=' + opts.showCount,
                allowtransparency: true,
                frameborder: 0,
                scrolling: 'no'
            });

            $iframe.appendTo($module);
            init(opts);
        } else {
            var $twitterPlaceholder = $('.j-twitter-placeholder');

            $twitterPlaceholder.on('click', function() {
                $twitterPlaceholder.remove();

                var $module = $('#cc-m-twitter-' + opts.id);
                var $iframe = $('<iframe>', {
                        src: '//platform.twitter.com/widgets/follow_button.html?screen_name=' + opts.username + '&show_count=' + opts.showCount,
                        allowtransparency: true,
                        frameborder: 0,
                        scrolling: 'no'
                    }
                );
                $iframe.appendTo($module);
                $iframe.find('#follow-button').on('click', function() {
                    init(opts);
                });
            });
        }
    };

})(require('jquery'), require('jimdoData'));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/modules/twitter.js
 ** module id = 1238
 ** module chunks = 96 97 232
 **/