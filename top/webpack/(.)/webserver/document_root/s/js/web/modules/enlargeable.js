(function($, PhotoSwipe, PhotoSwipeUI_Default) {
    "use strict";

    var SELECTOR = '[rel*="lightbox"]';
    var DEFAULT_SLIDER_PAUSE = 2500;
    var allowedSharingTypes = {
        'pinterest': {
            label: 'Pin it',
            url: 'http://www.pinterest.com/pin/create/button/?url={{url}}&media={{image_url}}&description={{text}}'
        }
    };

    var EnlargeableGallery = function($module, options) {
        this.$module = $module;
        this.$items = $(SELECTOR, this.$module);
        this.$btn = {};

        this.options = options || {};
        this.gallery = null;

        if (!this.isHTMLStructureInDom()) {
            this.addHTMLStructureToDom();
        }

        $('.pswp__item').off('contextmenu').on('contextmenu', function(event) {
            event.preventDefault();
        });

        this.getImages();
        this.addHandler();
    };

    EnlargeableGallery.prototype.addHTMLStructureToDom = function() {
        $('body').append($([
            '<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true"> <div class="pswp__bg"></div>',
            '<div class="pswp__scroll-wrap"> <div class="pswp__container"><div class="pswp__item"></div>',
            '<div class="pswp__item"></div><div class="pswp__item"></div></div><div class="pswp__ui pswp__ui--hidden">',
            '<div class="pswp__top-bar"><div class="pswp__counter"></div>',
            '<button class="pswp__button pswp__button--close"></button>',
            '<button class="pswp__button pswp__button--share"></button>',
            '<button class="pswp__button pswp__button--fs"></button>',
            '<button class="pswp__button pswp__button--zoom"></button>',
            '<button class="pswp__button pswp__button--custom pswp__button--play hidden"></button>',
            '<button class="pswp__button pswp__button--custom pswp__button--pause hidden"></button>',
            '<div class="pswp__preloader"> <div class="pswp__preloader__icn"> <div class="pswp__preloader__cut">',
            '<div class="pswp__preloader__donut"></div></div></div></div></div>',
            '<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">',
            '<div class="pswp__share-tooltip"></div></div>',
            '<button class="pswp__button pswp__button--arrow--left"></button>',
            '<button class="pswp__button pswp__button--arrow--right"></button>',
            '<div class="pswp__caption"> <div class="pswp__caption__center"></div></div></div></div></div>'
        ].join('')));
    };

    EnlargeableGallery.prototype.isHTMLStructureInDom = function() {
        return $('.pswp').length === 1;
    };

    EnlargeableGallery.prototype.getGalleryID = function() {
        return this.$module.attr('id');
    };

    EnlargeableGallery.prototype.getImageSize = function($item) {
        var $image = $('img', $item);

        // It's a f*cking mess

        if ($image.data('origWidth')) {
            return {
                width: $image.data('origWidth'),
                height: $image.data('origHeight')
            };
        }

        if ($image.data('srcWidth')) {
            return {
                width: $image.data('srcWidth'),
                height: $image.data('srcHeight')
            };
        }

        if ($item.data('width')) {
            return {
                width: $item.data('width'),
                height: $item.data('height')
            };
        }

        if ($item.data('params')) {
            var tmp = $item.data('params').split(',');

            return {
                height: parseInt(tmp.pop(), 10),
                width: parseInt(tmp.pop(), 10)
            };
        }

        return {
            height: 500,
            width: 500
        };
    };

    EnlargeableGallery.prototype.getImages = function() {
        var images = [];

        this.$items.each(function(i, item) {
            var $item = $(item);
            var size = this.getImageSize($item);

            // Add index attribute to image
            $item.attr('data-index', i);

            images.push({
                src: $item.data('href'),
                title: $item.data('title') || $item.data('flickr'),
                w: size.width,
                h: size.height
            });
        }.bind(this));

        this.images = images;
    };

    EnlargeableGallery.prototype.getOptions = function() {
        var options = {
            galleryUID: this.getGalleryID(),
            getThumbBoundsFn: function(index) {
                var $image = $('img', this.$items.filter('[data-index="' + index + '"]'));
                var pageYScroll = window.pageYOffset || document.documentElement.scrollTop;
                var rect = $image[0].getBoundingClientRect();

                return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
            }.bind(this),
            history: false,
            shareButtons: [],
            fullscreenEl: self === top,
            shareEl: false
        };

        if (this.options.sharing && this.options.sharing.length > 0) {
            options.shareEl = true;

            for (var i = 0, m = this.options.sharing.length; i<m; i++) {
                var key = this.options.sharing[i];

                if (allowedSharingTypes[key]) {
                    options.shareButtons.push({
                        id: key,
                        label: allowedSharingTypes[key].label,
                        url: allowedSharingTypes[key].url
                    });
                }
            }
        }

        return options;
    };

    EnlargeableGallery.prototype.openGallery = function(index) {
        this.$btn = {
            play: this.$container.find('.pswp__button--play'),
            pause: this.$container.find('.pswp__button--pause')
        };

        var options = this.getOptions();
        options.index = parseInt(index, 10);

        this.gallery = new PhotoSwipe(this.$container[0], PhotoSwipeUI_Default, this.images, options);
        this.gallery.init();
        this.gallery.interval = null;


        this.gallery.listen('autoplay', function(start) {
            this.$btn.play.toggleClass('hidden', start);
            this.$btn.pause.toggleClass('hidden', !start);

            if (start) {
                this.gallery.interval = setInterval(this.gallery.next, this.options.sliderPause || DEFAULT_SLIDER_PAUSE);
            } else {
                clearInterval(this.gallery.interval);
            }
        }.bind(this));

        this.gallery.listen('close', function() {
            this.gallery.shout('autoplay', false);

            this.$btn.play.off('click touchstart');
            this.$btn.pause.off('click touchstart');
        }.bind(this));

        if (this.images.length > 1) {
            this.$btn.play.on('click touchstart', function () {
                this.gallery.shout('autoplay', true);
            }.bind(this));

            this.$btn.pause.on('click touchstart', function () {
                this.gallery.shout('autoplay', false);
            }.bind(this));

            this.$btn.play.removeClass('hidden');
        }
    };

    EnlargeableGallery.prototype.clickHandler = function(event) {
        event.stopPropagation();
        event.preventDefault();

        this.openGallery($(event.target).parent('a').data('index'));
    };

    EnlargeableGallery.prototype.addHandler = function() {
        this.$container = $('.pswp');

        if (!this.options.doNotBindClickHandler) {
            this.$module.off('click', SELECTOR);
            this.$module.on('click', SELECTOR, this.clickHandler.bind(this));
        }
    };

    module.exports = EnlargeableGallery;
})(
    require('jquery'),
    require('bower_components/photoswipe/dist/photoswipe.js'),
    require('bower_components/photoswipe/dist/photoswipe-ui-default.js')
);



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/modules/enlargeable.js
 ** module id = 469
 ** module chunks = 96 97 232
 **/