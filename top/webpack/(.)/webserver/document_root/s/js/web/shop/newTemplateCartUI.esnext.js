/*jshint esnext: true */
require('../scripts/registry.js');
require('libs/mustache/jquery.mustache.js');
require('plugins/actionController/jquery.actionController.js');
require('./currencyFormatter.js');

(function ($, jimdoData) {
    "use strict";


    /**
     * @see newTemplateCartUISpec.js
     */
    $.reg('NewTemplateCartUI', function ($, currencyFormatter, translations, checkoutButtonAction) {
        var self = this;

        var texts;

        /** @const */
        var CART_TEMPLATE = `<div class="j-cart-wrapper-link">
            <div class="j-cart {{containsProductsCssClass}}">
                <div class="j-cart-icon-wrapper" data-action="checkout">
                    <div class="j-cart-icon">
                        <span class="j-cart-loading-spinner icon-web-loading-spinner"></span>
                        <span class="j-cart-item-amount">
                            ({{totalItemCount}})
                        </span>
                    </div> <!-- j-cart-icon -->
                </div> <!-- j-cart-icon-wrapper -->
                <div class="j-cart--hover-popup">
                    <div class="j-cart--hover-popup--cart-sum">
                        <p>
                            {{subtotalText}}: {{formattedProductTotalAmount}}
                        </p>
                    </div><!-- j-cart__hover-popup__cart-sum -->
                    
                    {{#shopDeliveryInfoLink}}
                    <a href="{{shopDeliveryInfoLink.href}}" class="j-cart--hover-popup--shop-delivery-link">
                        {{{shopDeliveryInfoLink.label}}}
                    </a>
                    {{/shopDeliveryInfoLink}}
                    
                    <a href="javascript:;" class="j-cart--hover-popup--call-to-action" data-action="checkout">
                        <span class="j-cart--hover-popup--call-to-action-button cc-shop-addtocard">
                            {{checkoutButtonText}}
                        </span>
                    </a><!-- j-cart__hover-popup__call-to-action -->
                    
                    <div class="j-cart--hover-popup--empty-message">
                        <p>
                            {{emptyBasketText}}
                        </p>
                    </div><!-- j-cart__hover-popup__empty-message -->
                </div><!-- j-cart__hover-popup -->
            </div>
        </div>`;

        this.init = function () {
            texts = $.extend(
                {checkoutButtonText: jimdoData.shop.checkoutButtonText},
                translations
            );
        };

        this.render = function (count, subtotal) {
            var formattedSubtotal = currencyFormatter.format(subtotal);
            var shopDeliveryLinkNode = $('div#contentfooter a[data-meta-link="shop_delivery_info"]');

            let shopDeliveryInfoLink;
            if (shopDeliveryLinkNode.length > 0) {
                shopDeliveryInfoLink = {
                    href: shopDeliveryLinkNode.attr('href'),
                    label: shopDeliveryLinkNode.html(),
                };
            }

            var cartUIData = {
                containsProductsCssClass: ( count ? 'has-products' : 'is-empty'),
                totalItemCount: count,
                formattedProductTotalAmount: formattedSubtotal,
                shopDeliveryInfoLink,
            };

            return $.mustache(
                CART_TEMPLATE,
                $.extend({}, cartUIData, texts)
            );

        };

        this.show = function (count, subtotal) {
            var markup = self.render(count, subtotal);
            var $sideCartWrapper = $('#cc-sidecart-wrapper');

            $sideCartWrapper.html(markup);
            adjustScreenEstateForCart();

            $sideCartWrapper.actionController({
                _checkoutClick: function (event) {
                    uiAddProgressIndicator();
                    checkoutButtonAction.goToCheckout();
                    trackGoToCheckout($(event.target));
                }
            });

            function adjustScreenEstateForCart() {
                // only needed for mobile, as the cart is not fixed at the bottom in desktop mode
                // and therefore will not cover footer elements
                if (!jimdoData.mobile) {
                    return;
                }

                var $container = $('#cc-m-container');
                var containerMarginBottom = parseFloat($container.css('margin-bottom'));
                var $cart = $('.j-cart');
                var cartHeight = $cart.outerHeight();

                if ($cart.is(':visible') && containerMarginBottom < cartHeight) {
                    $container.css('margin-bottom', cartHeight);
                }
            }

            function uiAddProgressIndicator() {
                var $cartContainer = $('.j-cart');
                $cartContainer.addClass('is-loading');
            }

            /**
             * label 'link' tracks the click anywhere on cartUI: subtotal, cart icon, product count
             * label 'callToActionButton' tracks click on the 'checkout' button inside the above mentioned link
             * @param $evenTarget
             */
            function trackGoToCheckout($evenTarget) {
                /**
                 * @const
                 * @type {string}
                 */
                var callToActionButtonSelector = '.j-cart--hover-popup--call-to-action-button';
                var label = 'link';

                if ($evenTarget.is(callToActionButtonSelector)) {
                    label = 'callToActionButton';
                }
            }
        };

    });


})(require('jquery'), require('jimdoData'));



/** WEBPACK FOOTER **
 ** ./webserver/document_root/s/js/web/shop/newTemplateCartUI.esnext.js
 **/