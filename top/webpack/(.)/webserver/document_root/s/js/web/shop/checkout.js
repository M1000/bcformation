(function ($) {
    "use strict";

    $.reg('CheckoutController', function ($) {

        this.init = function() {
            var initialBillingCountry = $('#billing_country option:selected').val();
            setCountryStateState(initialBillingCountry, 'billing');
            stateMandatoryIfNeededByPayPal(initialBillingCountry, 'billing');

            var initialShippingCountry = $('#shipping_country option:selected').val();
            setCountryStateState(initialShippingCountry, 'shipping');
            stateMandatoryIfNeededByPayPal(initialShippingCountry, 'shipping');
        };

        // Check the first payment option (needs to be done via JS because of the way the view is built)
        var firstPaymentMethod = $(".cc-checkout-paymentoption").find(':radio:first').attr('checked', true);
        toggleSelectedPaymentMethod(firstPaymentMethod);

        this._changeMethodChange = function () {
            toggleSelectedPaymentMethod(this);
        };

        this._changeMollieMethodChange = function (e, method) {
            toggleSelectedPaymentMethod(this);
            var form = $(this).closest('form');
            form.find('[name=mollie_payment_method]').val(method);
        };

        this._countryChange = function () {
            var selectedCountry = $(this).find('option:selected').val();
            var prefix = this.name.substring(0, this.name.indexOf('_'));
            stateMandatoryIfNeededByPayPal(selectedCountry, prefix);
            setCountryStateState(selectedCountry, prefix);
        };

        function setCountryStateState(selectedCountry, prefix) {
            var $state = $('#'+prefix+'_state');
            var $USState = $('#'+prefix+'_us_state_field');
            if (selectedCountry === 'US') {
                $USState.show();
                $state.hide();
            } else {
                $USState.hide();
                $state.show();
            }
        }

        /**
         * This function marks the state as mandatory even if PayPal is not activated.
         * But because these countries need states anyway we accept that behaviour.
         */
        function stateMandatoryIfNeededByPayPal($country, prefix) {
            // Argentina, Brazil, Canada, China, Indonesia, India, Japan, Mexico, Thailand or USA
            var $billingStateLabel = $('#'+prefix+'_state label');
            if ($billingStateLabel.data('field-required-by-user-settings') === true) {
                return;
            }

            // switch state field to mandatory dynamically when chosen country requires a state in addresses
            var payPalCountryList = $billingStateLabel.data('field-required-for-these-countries');
            if (payPalCountryList) {
                $billingStateLabel
                    .removeClass('cc-checkout-adressform-mandatory')
                    .removeClass('j-checkout__addressform-mandatory').find('.j-m-required').remove();
                $billingStateLabel.find('.j-checkout__required').remove();
                if ($.inArray($country, payPalCountryList.split(',')) >= 0) {
                    $billingStateLabel
                        .addClass('cc-checkout-adressform-mandatory')
                        .addClass('j-checkout__addressform-mandatory')
                        .append('<em class="j-m-required j-checkout__required">*</em>');
                }
            }
        }

        this._toggleAddressFormChange = function () {
            var shippingPrefix = "#cc-checkout-shipping-address-form",
                headPrefix = "#cc-checkout-address-head-",
                action = this.checked ? 'show' : 'hide';

            // Display the shipping address form if the respective checkbox is checked (otherwise hide it)
            $(shippingPrefix)[action]();
            $(shippingPrefix + '-heading')[action]();

            // Display the appropriate header texts
            $(headPrefix + 'same')[this.checked ? 'hide' : 'show']();
            $(headPrefix + 'differs')[action]();
        };

        function toggleSelectedPaymentMethod (element) {
            // Paymill disables next button while loading external js
            // un-disable the next button, when another payment method is chosen
            // so buyers can continue with different method without having to wait
            $('.cc-checkout-btn').prop('disabled', false);

            $('.j-checkout__payment-method').removeClass('j-checkout__payment__selected');
            // selects the first payment method. Because of the way mollie is implemented, there are two containers with
            // .j-checkout__payment-method. One contains all mollie payment methods, the other wraps every single one.
            // So we use .first() to select the closest to the checked radio button.
            $(element).parents('.j-checkout__payment-method').first().addClass('j-checkout__payment__selected');
        }
    });

    $.modules.checkout = function (opts) {
        var controller = $.factory('CheckoutController', [opts]);

        // Init the action controller on the checkout area
        $('#cc-checkout-wrapper').actionController(controller, {events: 'change click focusout submit keypress'});

    };
})(require('jquery'));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/shop/checkout.js
 ** module id = 1532
 ** module chunks = 96 97
 **/