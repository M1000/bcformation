require('../../scripts/registry.js');
require('imagesloaded/jquery.imagesloaded.js');

(function ($, jimdoData, window) {
    "use strict";

    $.reg('ProductController', function ($, opts, $module, $superZoom, $confines, isMobile, activateSuperZoom, cart, addToCartOverlay) {
            var self = this;
            // Container for the product's price
            var $price = $('.cc-shop-price-value', $module),
                $oldPriceContainer = $('.cc-shop-product-price-old', $module),
                $basicPriceContainer = $('.cc-shop-product-basic-price-container', $module),
                $basicPrice = $('.cc-shop-product-basic-price', $module),
                $basicPriceUnit = $('.cc-shop-product-basic-price-unit', $module),
                $oldPriceValue = $('.cc-shop-old-price-value', $module),
                // The button that adds articles to the cart
                $addToCart = $('.cc-shop-addtocard', $module),
                // The product's main image (the one above the thumbnails)
                $mainImage = $('.cc-shop-product-main-image', $module),
                // Container for the availability and delivery time information
                $detailContainer = $('.j-product-details', $module),
                productParams = {
                    product_id: opts.productId,
                    hash: opts.hash,
                    variant_id: opts.defaultVariantId
                };

            // XXX soft migration added new .j-product-details class
            // but it takes some days for the change to take effect
            if (!$detailContainer.length) {
                $detailContainer = $module.find('.cc-shop-product-availability');
            }

            /** @const */
            var DISABLED_CLASS = 'cc-addtocard-disabled';

            this.validateShopReady = function(product) {
                if (jimdoData.shop.isReady !== false) {
                    return true;
                }

                addToCartOverlay.error(product, jimdoData.tr.shop.notReadyText);

                return false;
            };

            /**
             * @return string pool/inventoryLevel/stock of currently selected variant(or product if no variants)
             * from schema.org markup. May be "Infinity".
             */
            this.getPoolFromMarkup = function () {
                return $module.find('[itemprop=inventoryLevel] [itemprop=value]').attr('content');
            };

            this.validateAmountOnStock = function(product) {
                var amountInCart = cart.getAmountForProduct(product),
                    pool = this.getPoolFromMarkup(),
                    message;

                if (pool > amountInCart) {
                    return true;
                }
                if (pool === "1") {
                    message = jimdoData.tr.shop.oneLeftText;
                }
                else {
                    message = jimdoData.tr.shop.numLeftText.replace('{:num}', amountInCart);
                }
                addToCartOverlay.error(product, message);

                return false;
            };

            this._addToCartClick = function () {
                var $button = $(this),
                    product;

                // Don't do anything if the button is disabled
                if ($button.hasClass(DISABLED_CLASS)) {
                    return false;
                }

                product = self._parseProductFromMarkup($module);

                if (!self.validateShopReady(product)) {
                    return;
                }

                if (!self.validateAmountOnStock(product)) {
                    return;
                }

                $(window).trigger('addProductToCart.shop', {
                    cartItem: {
                        product: product,
                        amount: 1
                    }
                });
            };

            this._changeVariantChange = function () {
                var product = $.parseJSON($(':selected', this).attr('data-params')),
                    $deliveryContainer = $detailContainer.find('.j-product-delivery-time'),
                    $productPool = $detailContainer.find('.j-product-pool'),
                    $productWeight = $detailContainer.find('.j-product-weight'),
                    $poolScope = $('[itemprop=inventoryLevel] [itemprop=value]', $module),
                    $freeShippingCountriesContainer = $module.find('.j-product__info__free-shipping-countries'),
                    $freeShippingCountriesContainerPrefix = $module.find('.cc-product-free-shipping-countries-prefix'),
                    $freeShippingCountriesSuffix = $module.find('.cc-product-free-shipping-countries-suffix'),
                    $freeShippingCountriesSuffixContainer = $module.find('.cc-product-free-shipping-countries-suffix-wrapper'),
                    $taxAndFeeShippingInfo = $module.find('.cc-product-tax-and-shipping-info');


                // XXX soft migration added new .j-product-details class
                // but it takes some days for the change to take effect
                if (!$deliveryContainer.length) {
                    $deliveryContainer = $detailContainer.find('.cc-product-delivery-time-info');
                }

                if (!$productPool.length) {
                    $productPool = $detailContainer.find('.cc-shop-product-pool');
                }

                $price.attr('content', product.price) //update itemprop for client side cart
                    .html(product.priceFormatted);

                handleOldPrice(product);

                var hasBasicPriceAndUnit = !!product.basicPrice && !!product.basicPriceUnit;
                if (hasBasicPriceAndUnit) {
                    $basicPrice.html(product.basicPriceFormatted);
                    $basicPriceUnit.html(product.basicPriceUnit);
                }
                $basicPriceContainer.toggle(hasBasicPriceAndUnit);

                // disable the add-to-cart button if the selected variant is not in pool
                $addToCart.toggleClass(DISABLED_CLASS, product.availability === 3);

                if ($productPool.length) {
                    $productPool.text(product.availabilityText);
                    // if the product variants have different pool values, display the appropriate text/icon
                    $.each($productPool.attr('class').split(' '), function(index, item) {
                        if (startsWith(item, 'j-product-pool-status-') || startsWith(item, 'cc-shop-product-pool-variant-')) {
                            $productPool.removeClass(item);
                        }
                    });
                    $productPool.addClass('j-product-pool-status-' + product.availability);
                    $productPool.addClass('cc-shop-product-pool-variant-' + product.availability);
                }

                $productWeight.remove();

                if (product.weight) {
                    $detailContainer.prepend("<li class='j-product-weight'>" + product.weightFormatted + "</li>");
                }

                if (product.freeShippingCountriesPrefix === '') {
                    $freeShippingCountriesContainer.addClass('hide');
                } else {
                    $freeShippingCountriesContainer.removeClass('hide');
                    $freeShippingCountriesContainerPrefix.html(product.freeShippingCountriesPrefix);

                    if (product.freeShippingCountriesSuffix === '') {
                        $freeShippingCountriesSuffixContainer.addClass('hide');
                    } else {
                        $freeShippingCountriesSuffixContainer.removeClass('hide');
                        $freeShippingCountriesSuffix.html(product.freeShippingCountriesSuffix);
                    }
                }

                if (product.taxAndShippingCostInformationText && product.taxAndShippingCostInformationText.length > 0) {
                    $taxAndFeeShippingInfo.html(product.taxAndShippingCostInformationText);
                }

                $deliveryContainer.hide();

                if (product.availability !== 3) {
                    // display the appropriate delivery time text and don't forget the footnote
                    var $footnoteMarker = $deliveryContainer.find("sup");
                    $deliveryContainer
                        .text(jimdoData.shop.deliveryTimeTexts[product.delivery])
                        .append($footnoteMarker)
                        .show();

                    var classes = $deliveryContainer.attr('class');
                    if (classes) {
                        $.each(classes.split(' '), function(index, item) {
                            if (startsWith(item, 'j-product-delivery-time-status-') || startsWith(item, 'cc-delivery-time-variant-')) {
                                $deliveryContainer.removeClass(item);
                            }
                        });
                    }
                    $deliveryContainer.addClass('j-product-delivery-time-status-' + product.delivery);
                    $deliveryContainer.addClass('cc-delivery-time-variant-' + product.delivery);
                }

                if ($poolScope.length) { // XXX SOFTMIG make unconditional when markup is ready (after some PR is deployed and a week passed)
                    $poolScope.attr('content', product.pool);
                }

                // add variant value to opts
                productParams.variant_id = parseInt($(this).val(), 10);
            };

            this._changeMainImageClick = function (e, stdUrl, zoomUrl, origWidth, origHeight) {
                if (isMobile) {
                    return false;
                }

                // Mark the selected thumbnail with a white arrow
                var $thumb = $(this).parent(),
                    activeClass = 'cc-shop-product-alternatives-active',
                    margin;

                if ($thumb.hasClass(activeClass)) {
                    return false;
                }

                $thumb.addClass(activeClass).siblings().removeClass(activeClass);

                if ($.browser.msie) {
                    // IE has a caching bug: If you change the image src to a file that is in the browser cache, the load event will
                    // not be triggered. Don't do this for all browsers, though, to avoid circumventing CDN caching.
                    stdUrl += '?' + new Date().getTime();
                }

                // Load the new main image and update its size values
                $mainImage
                    .imagesLoaded(function () {
                        margin = parseInt(opts.mainImageHeight / 2, 10) - parseInt(this.height / 2, 10);

                        // Update some zoom element attributes and unbind all its events to reset the zoom features
                        $superZoom
                            .css({
                                marginTop: margin + 'px',
                                width: this.width + 'px'
                            })
                            .attr({
                                href: zoomUrl,
                                "data-width": origWidth,
                                "data-height": origHeight
                            })
                            .unbind();

                        // Update the height of the image confines
                        $confines.height(opts.mainImageHeight - margin);

                        $mainImage.show();

                        // Re-initialize the zoom with the new image
                        activateSuperZoom(opts, $superZoom, $confines);
                    })
                    .attr({srcset: ''})
                    .attr({src: ''}) // Remove src first so $.imagesLoaded definitely notices a change
                    .attr({src: stdUrl})
                    .removeAttr('width height')
                    .hide();

                return false;
            };

            this._toggleFreeShippingCountriesClick = function () {
                var $moreLink = $(this);

                $moreLink.parent().find('.cc-product-free-shipping-countries-suffix').toggleClass('isVisible');
                $moreLink.toggleClass('showLess');
                $moreLink.blur();
            };

            this._parseProductFromMarkup = function ($scope) {
                var variantId,
                    variantName,
                    variantParams,
                    basicPrice,
                    basicPriceFormatted,
                    basicPriceUnit,
                    $itemprop = $scope.find('[itemprop]:not([itemprop=description] *)'),
                    $price = $itemprop.filter('[itemprop=price]'),
                    $productInfoLink = $scope.find('.cc-product-infolink > a'),
                    $shortDescription = $scope.find('.cc-shop-product-short-desc');

                if (opts.hasVariants) {
                    var $selectedVariantOption = $scope.find('.cc-product-variant-selectbox option:selected');

                    variantId = $selectedVariantOption.val();
                    variantName = $selectedVariantOption.attr('content');
                    variantParams = $selectedVariantOption.data('params');
                    basicPrice = variantParams.basicPrice;
                    basicPriceFormatted = variantParams.basicPriceFormatted;
                    basicPriceUnit = variantParams.basicPriceUnit;
                }

                return {
                    productId: opts.productId,
                    variantId: variantId || null,

                    name: $itemprop.filter('[itemprop=name]').text(),

                    price: parseFloat($price.attr('content') || 0),

                    productUrl: $itemprop.filter('[itemprop=url]').attr('content'),
                    imageUrl: $itemprop.filter('[itemprop=image]').prop('src') || null,

                    variantName: variantName || null,

                    productInfoLink: {
                        label: $.trim($productInfoLink.html()),
                        href: $productInfoLink.attr('href'),
                    },

                    basicPrice: basicPrice,
                    basicPriceFormatted: basicPriceFormatted,
                    basicPriceUnit: basicPriceUnit,

                    shortDescription: $.trim($shortDescription.text())
                };
            };

            function handleOldPrice(p) {
                var displayOldPrice = (typeof p.oldPrice !== "undefined" && parseFloat(p.oldPrice, 10) > 0);
                $oldPriceContainer.toggle(displayOldPrice);

                if (displayOldPrice) {
                    $oldPriceValue.html(p.oldPriceFormatted);
                }
            }

            function startsWith(haystack, needle) {
                if (!String.prototype.startsWith) {
                    return haystack.indexOf(needle, 0) === 0;
                }
                return haystack.startsWith(needle);
            }
        }
    );
})(require('jquery'), require('jimdoData'), window);



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/modules/product/productController.js
 ** module id = 683
 ** module chunks = 96 97 232
 **/