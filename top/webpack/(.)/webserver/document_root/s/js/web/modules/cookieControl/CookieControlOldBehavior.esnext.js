import Cookies from 'js-cookie';
import { CookieControl } from './CookieControl.esnext';

const cookiePrefix = 'ckies_';

const categoryMapping = {
    fr: 'marketing',
    ga: 'performance',
    powr: 'functional',
    jimdo_analytics: 'performance',
    fb_analytics: 'marketing',
    google: 'functional',
};

// eslint-disable-next-line import/prefer-default-export
export class CookieControlOldBehavior extends CookieControl {

    deny() {
    }

    allow() {
    }

    isCookieAllowed(key) {
        let cookieKey;
        if (categoryMapping[key] !== undefined) {
            cookieKey = cookiePrefix + categoryMapping[key];
        } else {
            cookieKey = cookiePrefix + key;
        }
        const cookieValue = Cookies.get(cookieKey);

        if (window.CKIES_OPTIN) {
            return cookieValue === 'allow';
            // eslint-disable-next-line no-else-return
        } else {
            return cookieValue !== 'deny';
        }
    }

    isPOWrAllowed() {
        return this.isCookieAllowed('powr');
    }
}



/** WEBPACK FOOTER **
 ** ./webserver/document_root/s/js/web/modules/cookieControl/CookieControlOldBehavior.esnext.js
 **/