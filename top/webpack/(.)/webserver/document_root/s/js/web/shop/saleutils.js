require('../scripts/registry.js');
/**
 * Stuff needed for checkout
 */

(function($) {
    "use strict";

    /** @const */
    var PATH_TO_SHOPPINGCART = '/app/shop/shoppingcart/';

    $.reg('SaleUtils', function($, options) {

        var self = this,
            defaults = {
                loadingClass: 'cc-cart-loading',
                onSuccess: $.noop,
                onError: $.noop,
                getContainer: $.noop,
                type: null
            },
            s;

        this.init = function() {
            s = $.extend({}, defaults, options);
        };

        this.setOnSuccess = function(onSuccess) {
            s.onSuccess = onSuccess;
        };

        this.setOnError = function(onError) {
            s.onError = onError;
        };

        this.loading = function( show ) {
            var $container = s.getContainer();
            $container[show ? 'addClass' : 'removeClass'](s.loadingClass);
        };

        this.spinnerchange = function ( value ) {
            var $input = $(this);

            var data = {};
            data.newcount = value;
            data.count = $input.data('product-count');
            data.item_id = $input.data('product-item-id');
            data.hash = $input.data('product-hash');

            self.loading(true);

            $.sync({
                url: PATH_TO_SHOPPINGCART + 'updatecount',
                type: 'post',
                data: data,
                success: function(r) {
                    var $container = s.getContainer();

                    self.loading(false);
                    if ( r.status === 'success' ) {
                        // update the checkout with the new prices

                        $container.html(r.payload.checkout);
                        s.onSuccess.call(self);
                    } else {
                        $input.val(r.payload.count).message(r);

                        s.onError.call(self);
                    }
                }
            });
        };
    });

})(require('jquery'));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/shop/saleutils.js
 ** module id = 803
 ** module chunks = 96 97 232
 **/