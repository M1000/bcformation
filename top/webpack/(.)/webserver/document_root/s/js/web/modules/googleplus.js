/* global gapi */
(function($, window) {
    "use strict";

window._onLoadGooglePlus = function() {
    require('jquery')(window).trigger('googlePlusLoad');
};

$.modules.googleplus = function(opts) {
    var $content = $('#cc-m-' + opts.id),
    $container = $content.find('.googleplus-container'),
    type;

    switch (opts.widgetType) {
        case '1':
        type = 'plusone';
        break;
        case '2':
        case '3':
        type = 'plus';
        break;
    }

    $(window).on('googlePlusLoad', function() {
        gapi[type].go($container[0]);
    });

    if (window.jimdoData.isCMS) {
        $container.before('\
            <script src="https://apis.google.com/js/plusone.js?onload=_onLoadGooglePlus" async="true" defer="true">\
            {parsetags: "explicit"}\
            </script>\
        ');
    } else {
        var $button = $('.googleplus-placeholder');
        $button.on('click', function() {
            $container.before('\
                <script src="https://apis.google.com/js/plusone.js?onload=_onLoadGooglePlus" async="true" defer="true">\
                {parsetags: "explicit"}\
                </script>\
            ');
            $button.remove();
        });
    }
};

})(require('jquery'), window);



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/modules/googleplus.js
 ** module id = 1233
 ** module chunks = 96 97 232
 **/