
// eslint-disable-next-line import/prefer-default-export
export class UrlHelper {

    static getParams(url) {
        const params = {};
        const parser = document.createElement('a');
        parser.href = url;
        const query = parser.search.substring(1);
        const vars = query.split('&');
        for (let i = 0; i < vars.length; i++) {
            const pair = vars[i].split('=');
            params[pair[0]] = decodeURIComponent(pair[1]);
        }
        return params;
    }
}



/** WEBPACK FOOTER **
 ** ./webserver/document_root/s/js/web/modules/helpers/UrlHelper.esnext.js
 **/