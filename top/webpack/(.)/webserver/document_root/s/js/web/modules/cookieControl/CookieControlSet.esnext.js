
import { CookieControl } from './CookieControl.esnext';
import { CookieControlOff } from './CookieControlOff.esnext';
import { CookieControlOldBehavior } from './CookieControlOldBehavior.esnext';
import { CookieControlPreview } from './CookieControlPreview.esnext';

// eslint-disable-next-line import/prefer-default-export
export class CookieControlSet {

    setToOff() {
        window.CookieControl = new CookieControlOff();
    }

    setToOldBehavior() {
        window.CookieControl = new CookieControlOldBehavior();
    }

    setToNormal() {
        if (this.isInIFrame()) {
            this.setToPreview();
            return;
        }
        window.CookieControl = new CookieControl();
    }

    setToPreview() {
        window.CookieControl = new CookieControlPreview();
    }

    isInIFrame() {
        try {
            return window.self !== window.top;
        } catch (e) {
            return true;
        }
    }
}

window.CookieControlSet = new CookieControlSet();



/** WEBPACK FOOTER **
 ** ./webserver/document_root/s/js/web/modules/cookieControl/CookieControlSet.esnext.js
 **/