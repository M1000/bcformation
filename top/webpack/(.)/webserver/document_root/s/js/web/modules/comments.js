require('../scripts/bootstrap.js');
require('../scripts/jimdo.js');

/* global changeCaptcha */
(function($, window, jimdoData) {
    "use strict";

    window.jsonCallback = {};
    $.modules.comment = function(opts) {
        var commentsManager = {
            urls: {
                getForm: '/app/module/comment/getform/?t=' + (new Date()).getTime(),
                addComment: '/app/module/comment/add',
                validateUrl: '/app/auth/discover/index/url/'
            },
            mods: {},
            init: function(opts) {
                this.modData = opts.data;
                var id = this.id = opts.data.id;
                this.initEvents(id);
                this.getForm(id);
            },

            initEvents: function(id) {
                $('#commentsModule' + id + ' [data-action]').live('click', function(e){
                    e.preventDefault();
                    // cant use e.target.dataset because of poor ie support
//                      this[e.target.dataset.action](id, e);
                    this[e.target.getAttribute('data-action')](id, e);
                }.bind(this));

                $('#commentform' + id).on('submit', function(e){
                    e.preventDefault();
                    this.addComment(id);
                }.bind(this));

                $('#commentsModule' + id + ' [data-blur-action]').live('blur', function(e){
                    this[e.target.getAttribute('data-blur-action')](id, e);
                }.bind(this));
            },

            getForm: function (id){
                var data = {
                    moduleId: id,
                    closed: this.modData.closed
                };
                this.sendAjax(this.urls.getForm, data, 'getFormSuccess');
            },

            goToForm: function(id){
                var commentForm = $('#commentform'+ id)[0];
                if(!commentForm){
                    this.resetForm(id);
                    var content = $('#commentOutput'+ id).html();
                    content += '<li id="commentFormContainer'+id+'" class="commentstd clearover"></li>';
                    $('#commentOutput'+id).html(content);
                    this.getForm(id);
                }

                $('html, body').animate({
                    scrollTop: $('#commentFormContainer'+ id).offset().top
                }, 500);
                location.hash = 'commentForm'+id;
            },

            addComment: function(id){
                this.resetForm(id);
                $('#submit'+id).attr("disabled", "disabled");
                var commentFormContainer = $('#commentFormContainer'+ id);
                commentFormContainer.addClass('sending');
                var dataToPost = {};
                dataToPost.comment =  $('#comment'+id).val();
                dataToPost.name =  $('#name'+id).val();
                dataToPost.moduleId = id;
                dataToPost.url =  $('#url'+id).val();
                dataToPost.privacyPolicy = !!$('#privacyPolicy'+id).attr('checked') ? '1' : '0';

                if (this.modData.captcha){
                    dataToPost.captcha = $('#captcha'+this.modData.captcha).val();
                    dataToPost['g-recaptcha-response'] = commentFormContainer.find('#g-recaptcha-response').val();
                }

                this.sendAjax(this.urls.addComment, dataToPost, 'addCommentSuccess');
            },

            showError: function(response, id){
                $('#commentError'+id).html(response.html);
                $('#commentError'+id).css('display', 'block');
            },

            resetForm: function(id){
                $('#commentError'+id+', #commentSucces'+id).hide();
                $('#commentFormContainer'+ id).removeClass('sending').show();
            },

            sendAjax: function(url, data, callback){
                var that = this;
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: data,
                    dataType: 'json',
                    success: function(data){
                        that[callback](data);
                    }
                });
            },

            addCommentSuccess: function(json){
                var response = json.data ? json.data : json;
                var id =  this.id;
                if(this.modData.captcha) {
                    changeCaptcha(this.modData.captcha);
                }

                $('#commentFormContainer'+ id).removeClass('sending');
                $('#submit'+id).removeAttr("disabled");

                if(response.error){
                    this.showError(response, id);
                    return;
                }

                if(response.delay){
                    $('#commentSuccess'+id).css('display', 'block').html(response.delay);
                    $('#commentFormContainer'+id).css('display', 'none');
                    return;
                }

                var numComments = $('#numComments'+id).find('.cc-comments-count');
                if(numComments[0]){
                    numComments.text(response.count);
                }

                $('#commentOutput' + id).html(response.html);

            },

            getFormSuccess: function (data){
                var id = this.id;
                $('#commentFormContainer'+id).html(data.data).removeClass('cc-m-comment-loading');
                // re-init form events
                this.initEvents(id);
                if(this.modData.captcha) {
                    changeCaptcha(this.modData.captcha);
                }
            },

            showAll: function(id, e) {
                $('#commentOutput' + id + ' li.commentstd').show();
                $(e.target).hide();
            }
        };
         if(jimdoData && !jimdoData.isCMS){
             commentsManager.init(opts);
         }

    };

})(require('jquery'), window, require('jimdoData'));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/modules/comments.js
 ** module id = 1229
 ** module chunks = 96 97 232
 **/