var klass = require('../../libs/Class/Class.js');

(function($) {
    "use strict";

    /** @const */
    var SINGLETON_REGISTRY_PREFIX = 'singleton|';

    var classes = {},
        instances = {};

    /**
     * Add a class definition
     * @param name {String}
     * @param _class {Function}
     * @return {Function}
     */
    $.reg = function(name, _class) {
        if (typeof name === 'string' || $.isFunction(_class)) {
            name = name.match(/^\s*([\w\.-]+)(\s+extends\s+([\w\.-]+))?\s*$/);
            var superClassName = name[3];
            if (superClassName && typeof classes[superClassName] === 'undefined') {
                throw new Error("Could not find class named '" + superClassName + "'");
            }
            var className = name[1];
            classes[className] = superClassName ? classes[superClassName].extend(_class) : klass(_class);
        } else {
            $.error("Couldn't create class. \n " + JSON.stringify(arguments));
        }
        return _class;
    };

    /**
     * Get instance, that was already created
     * @param {String} instanceName
     * @param {Function} [creationCallback] If no instance can be found for the given key, a new one will be created using the return value of this callback function
     * @return {Object}
     */
    $.reg.get = function(instanceName, creationCallback) {
        var instance = instances[instanceName];
        if (!instance && typeof creationCallback === 'function') {
            instance = $.reg.set(instanceName, creationCallback());
        }
        return instance;
    };

    /**
     * Set instance
     * @param {String} instanceName
     * @param {Object} instance
     * @return {Object} instance
     */
    $.reg.set = function(instanceName, instance) {
        instances[instanceName] = instance;
        return instance;
    };

    /**
     * Remove instance from instances pool
     * @param {String} instanceName
     * @return {Boolean}
     */
    $.reg.del = function(instanceName) {
        if (typeof instances[instanceName] !== 'undefined') {
            instances[instanceName] = null;
            delete instances[instanceName];
            return true;
        }
        return false;
    };


    function createFactory(intendedJQuery, registry) {
        return     function(className, params) {
            if ($.isArray(params)) {
                params.unshift(intendedJQuery);
            } else {
                params = [intendedJQuery];
            }
            var clazz = registry.getClass(className);
            return clazz ? clazz.apply({}, params) : $.error("Class " + className + " doesn't exist.");
        };
    }


    /**
     *
     * @param {String} className
     * @param {Array} [params]
     * @return {Object}
     */
    $.factory = createFactory($, $.reg);

    /**
     * Generates a singleton instance for a given class name
     *
     * @param className The classname of the class to create a singleton for
     * @param [params]    params to initialize the class with
     * @returns {*}
     */
    $.reg.singleton = function(className, params) {
        return $.reg.get(SINGLETON_REGISTRY_PREFIX + className, function() {
            return $.factory(className, params);
        });
    };

    /**
     * Get class
     * @param {String} className
     * @return {Function}
     */
    $.reg.getClass = function(className) {
        return classes[className];
    };

    /**
     * Remove class from classes pool
     * @param {String} className
     * @return {Boolean}
     */
    $.reg.delClass = function(className) {
        return delete classes[className];
    };

})(require('jquery'));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/scripts/registry.js
 ** module id = 47
 ** module chunks = 96 97 229 232 234 235 241
 **/