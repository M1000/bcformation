require('../cart.js');

(function($, jimdoData, window) {
    "use strict";

    $.modules.checkoutcomplete = function(params) {
        if (!params.status) {
            return;
        }

        if (window.CookieControl.isCookieAllowed("fb_analytics") && jimdoData.isFacebookPixelIdEnabled && jimdoData.isFacebookPixelIdEnabled === true) {
            $(function() {
                var successInfo = document.querySelector('#cc-checkout-successinfo');

                if (!successInfo || typeof fbq === 'undefined') {
                    return;
                }

                var currency = successInfo.getAttribute('data-grand-total-currency');
                var amount = parseFloat(successInfo.getAttribute('data-grand-total-amount'));

                fbq('track', 'Purchase', {
                    currency: currency,
                    value: amount
                });
            });
        }

        // clear localStorage based shopping cart
        var cart = $.factory('ShoppingCartFactory', []).create();
        cart.clear();
    };

})(require('jquery'), require('jimdoData'), window);



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/shop/checkout/complete.js
 ** module id = 1534
 ** module chunks = 96 97
 **/