import mustache from '../../libs/mustache/mustache.js';
import $ from 'jquery';
import jimdoData from 'jimdoData';

import '../../../sass/web/help-page-table-of-contents.sass';

function renderToc(headerStructure) {
    const TOC_TEMPLATE = `
            <div class="cc-toc__container">
                {{#headers}}
                    <p class="cc-toc__headline--level-{{level}} cc-toc__headline">
                        <a class="js-scroll-to-hash" href="#{{id}}">{{text}}</a>
                    </p>
                {{/headers}}
            </div>
        `;
    return mustache.render(TOC_TEMPLATE, { headers: headerStructure });
}

function createTocStructureFromPageHeadlines() {
    // retrieve all Hn elements, except for h1 (H1 should not be in toc)
    const $headersOnPage = $('#content_area .j-header > h2,h3,h4,h5,h6');
    const tocHeaderData = [];
    $headersOnPage.each((index, element) => {
        const $header = $(element);
        const level = $header[0].tagName.slice(-1);
        const id = $header[0].id;
        const text = $header.text();
        tocHeaderData.push({
            id,
            level,
            text,
        });
    });
    return tocHeaderData;
}

$(() => {
    if (!jimdoData.tocGeneration) {
        return;
    }
    const $tocReplaceElements = $('.j-toc-replace');
    if (!$tocReplaceElements.length) {
        return;
    }
    const tocMarkup = renderToc(createTocStructureFromPageHeadlines());
    $tocReplaceElements.each((index, element) => {
        $(element).html(tocMarkup);
    });
});



/** WEBPACK FOOTER **
 ** ./webserver/document_root/s/js/web/scripts/helpPageTableOfContentsGenerator.esnext.js
 **/