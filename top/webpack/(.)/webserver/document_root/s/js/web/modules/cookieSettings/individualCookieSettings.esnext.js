import $ from 'jquery';
import { CookieSettingsUiController } from './cookieSettingsUiController.esnext';
import { CookieSettingsUiState } from './cookieSettingsUiState.esnext';
import { CookieSettingsUiRenderer } from './cookieSettingsUiRenderer.esnext';

require('jquery-ui/ui/dialog');
require('jquery-ui/themes/base/jquery-ui.css');
require('../../../../sass/web/module/cookieSettings/individualCookieSettings.sass');
require('../../../plugins/actionController/jquery.actionController');

/**
 *
 * @param data
 * @param {object[]} data.categories a list of category objects with name, description,
 * and an array of cookies, that also have name and description
 * @param {string[]} data.pagesWithoutCookieSettings a list of urls in which the cookie settings should
 * not be shown
 * @param {string} data.cookieSettingsHtmlUrl the URL for retrieving the HTML of the Cookie Settings
 */
const CookieSettings = ({ categories, pagesWithoutCookieSettings, cookieSettingsHtmlUrl }) => {
    const DISPLAY_COOKIE_SETTINGS_COOKIE_NAME = 'cookielaw';
    const COOKIE_SETTINGS_DIV_ID = 'cc-individual-cookie-settings';
    const state = new CookieSettingsUiState({
        categories,
        defaultView: 'all-categories-view',
        selectedCookies: [DISPLAY_COOKIE_SETTINGS_COOKIE_NAME],
        pagesWithoutCookieSettings,
        cookieSettingsHtmlUrl,
    });
    const renderer = new CookieSettingsUiRenderer({
        cookieSettingsDivId: COOKIE_SETTINGS_DIV_ID,
        mainView: state.defaultView,
    });
    const controller = new CookieSettingsUiController({
        state,
        renderer,
        cookieControl: window.CookieControl,
    });
    if (typeof window.CookieControl.onShowCookieSettings === 'function') {
        window.CookieControl.onShowCookieSettings((cookieKey) => {
            state.setShowCookieSettings(true);
            controller.showCookieSettings(cookieKey);
        });
    }
    controller.init();
};

$.modules.individualCookieSettings = CookieSettings;



/** WEBPACK FOOTER **
 ** ./webserver/document_root/s/js/web/modules/cookieSettings/individualCookieSettings.esnext.js
 **/