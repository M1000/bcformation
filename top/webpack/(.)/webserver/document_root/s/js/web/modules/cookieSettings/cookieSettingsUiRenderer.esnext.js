import $ from 'jquery';

// eslint-disable-next-line import/prefer-default-export
export class CookieSettingsUiRenderer {
    constructor({ cookieSettingsDivId, mainView }) {
        this.cookieSettingsDivId = cookieSettingsDivId;
        this.mainView = mainView;
        this.cookieSettingsDiv = $(`#${cookieSettingsDivId}`);
    }

    findCategory(categoryName) {
        return this.cookieSettingsDiv.find(`[data-category-name="${categoryName}"]`);
    }

    findCategories() {
        return this.cookieSettingsDiv.find('[data-type=toggle][data-action=toggleCategory]');
    }

    findCookies() {
        return this.cookieSettingsDiv.find('[data-type=toggle][data-action=toggleCookie]');
    }

    renderAllCategories() {
        this.cookieSettingsDiv.find('[data-render-group=category-view]').hide();
        this.cookieSettingsDiv.find('[data-render-group=all-categories-view]').show();
    }

    renderSelectedCategory(category) {
        this.cookieSettingsDiv.find('[data-render-group=all-categories-view]').hide();
        this.cookieSettingsDiv.find('[data-render-group=category-view]').hide();
        this.cookieSettingsDiv.find('button[data-render-group=category-view]').show();
        this.findCategory(category).show();
    }

    renderCategoryToggles(categories) {
        this.findCategories().each((i, toggle) => {
            $(toggle).attr('data-value', categories[$(toggle).attr('data-params')]);
        });
    }

    renderCookieToggles(cookies) {
        this.findCookies(this.cookieSettingsDiv).each((i, toggle) => {
            $(toggle).attr('data-value', cookies[$(toggle).attr('data-params')]);
        });
    }

    render({ view, categories, cookies }) {
        if (view === this.mainView) {
            this.renderAllCategories();
        } else {
            this.renderSelectedCategory(view);
        }
        this.renderCategoryToggles(categories);
        this.renderCookieToggles(cookies);
    }

    findCategoryNameForCookie(cookieKey) {
        return this.cookieSettingsDiv.find(`li[data-cookiekey="${cookieKey}"]`)
            .closest('div[data-render-group="category-view"]')
            .data('category-name');
    }

    navigateToSpecificCookie(cookieKey) {
        const categoryName = this.findCategoryNameForCookie(cookieKey);
        if (categoryName) {
            this.renderSelectedCategory(categoryName);
            const isIE = window.document.documentMode;
            if (!isIE) {
                const cookieDiv = this.cookieSettingsDiv.find(`li[data-cookiekey="${cookieKey}"]`);
                cookieDiv[0].scrollIntoView();
            }
        }
    }

    init({ listeners }) {
        const renderer = this;
        renderer.cookieSettingsDiv.dialog({
            dialogClass: renderer.cookieSettingsDivId,
            resizable: false,
            draggable: true,
            modal: true,
            closeOnEscape: false,
            autoOpen: false,
            width: 360,
            open() {
                $('.ui-widget-overlay').addClass(`${renderer.cookieSettingsDivId}-overlay`);
                renderer.cookieSettingsDiv.actionController(listeners, { events: 'click' });
            },
        });
    }

    insertHtml(data) {
        this.cookieSettingsDiv.html(data);
        this.moveCookieSettingsHtmlToEndToAvoidOverlapping();
    }

    moveCookieSettingsHtmlToEndToAvoidOverlapping() {
        this.cookieSettingsDiv.parent().appendTo($("body"));
    }

    open() {
        this.cookieSettingsDiv.dialog('open');
    }

    close() {
        this.cookieSettingsDiv.dialog('close');
    }

}



/** WEBPACK FOOTER **
 ** ./webserver/document_root/s/js/web/modules/cookieSettings/cookieSettingsUiRenderer.esnext.js
 **/