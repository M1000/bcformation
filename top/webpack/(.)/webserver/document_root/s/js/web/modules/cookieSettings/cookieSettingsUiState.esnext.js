// eslint-disable-next-line import/prefer-default-export
export class CookieSettingsUiState {
    // eslint-disable-next-line max-len
    constructor({ categories, defaultView, view, showCookieSettings, selectedCookies, pagesWithoutCookieSettings, cookieSettingsHtmlUrl }) {
        this.defaultView = defaultView;
        this.view = view || defaultView;
        this.categories = categories.slice();
        this.selectedCookies = selectedCookies || [];
        this.pagesWithoutCookieSettings = pagesWithoutCookieSettings || [];
        this.showCookieSettings = showCookieSettings;
        this.cookieSettingsHtmlUrl = cookieSettingsHtmlUrl;
        this.cookieSettingsHtmlLoaded = false;
    }

    setCookieSettingsHtmlLoaded(value) {
        this.cookieSettingsHtmlLoaded = value;
    }

    getCookieSettingsHtmlUrl() {
        return this.cookieSettingsHtmlUrl;
    }

    getCookieSettingsHtmlLoaded() {
        return this.cookieSettingsHtmlLoaded;
    }

    setView(view) {
        this.view = view;
    }

    setShowCookieSettings(value) {
        this.showCookieSettings = value;
    }

    getAllCategoryNames() {
        return this.categories.map(({ name }) => name);
    }

    getAllCookieKeys() {
        return this.categories
            .reduce((prev, { cookies }) => prev.concat(cookies), [])
            .reduce((prev, { key }) => prev.concat(key), []);
    }

    getAllCookieKeysWithoutNecessary() {
        const categoriesWithoutNecessary = this.categories.filter(category => category.type !== 'NECESSARY');
        return categoriesWithoutNecessary
            .reduce((prev, { cookies }) => prev.concat(cookies), [])
            .reduce((prev, { key }) => prev.concat(key), []);
    }

    getPagesWithoutCookieSettings() {
        return this.pagesWithoutCookieSettings.slice();
    }

    getCategory(categoryName) {
        return this.categories.filter(({ name }) => name === categoryName)[0];
    }

    selectCookies(cookies) {
        this.selectedCookies = this.unique([...this.selectedCookies, ...cookies]);
    }

    unselectCookies(cookies) {
        this.selectedCookies = this.selectedCookies.filter(cookie => cookies.indexOf(cookie) === -1);
    }

    getCategoryCookieKeys(categoryName) {
        return this.getCategory(categoryName).cookies.map(({ key }) => key);
    }

    toggleCookies(cookies, value) {
        if (this.nextValue(value) === 'all') {
            this.selectCookies(cookies);
        } else {
            this.unselectCookies(cookies);
        }
    }

    toggleCookie(key, value) {
        this.toggleCookies([key], value);
    }

    toggleCategory(name, value) {
        this.toggleCookies(this.getCategoryCookieKeys(name), value);
    }

    isCookieAllowed(key) {
        return this.selectedCookies.indexOf(key) >= 0;
    }

    cookieState(cookieKey) {
        return this.isCookieAllowed(cookieKey) ? 'all' : 'none';
    }

    categoryState(categoryName) {
        const categoryCookies = this.getCategory(categoryName).cookies;
        if (!categoryCookies.length) {
            return 'none';
        }
        if (categoryCookies.every(({ key }) => this.isCookieAllowed(key))) {
            return 'all';
        }
        if (categoryCookies.some(({ key }) => this.isCookieAllowed(key))) {
            return 'some';
        }
        return 'none';
    }

    getCategoryStateMap() {
        return this.toKeyValueMap(this.getAllCategoryNames(), this.categoryState.bind(this));
    }

    getCookieStateMap() {
        return this.toKeyValueMap(this.getAllCookieKeys(), this.cookieState.bind(this));
    }

    toKeyValueMap(keys, getValue) {
        const merge = (map, key) => ({
            ...map,
            [key]: getValue(key),
        });
        return keys.reduce(merge, {});
    }

    nextValue(value) {
        return {
            all: 'none',
            some: 'all',
            none: 'all',
        }[value];
    }

    unique(array) {
        return array.reduce((previous, current) => {
            if (previous.indexOf(current) === -1) {
                previous.push(current);
            }
            return previous;
        }, []);
    }
}



/** WEBPACK FOOTER **
 ** ./webserver/document_root/s/js/web/modules/cookieSettings/cookieSettingsUiState.esnext.js
 **/