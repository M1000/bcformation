(function($){
    "use strict";

$.modules.downloadDocument = function(opts) {
    var $preview,
        height;

    if (opts.showpreview) {
        $preview = $('#cc-m-download-preview-' + opts.id);
        height = $preview.width() / 1.33;
        $preview.html('<iframe src="http://docs.google.com/viewer?url=<?php echo urlencode($this->downloadUrl) ?>&embedded=true" width="100%" height="' + height + '" style="border: none;"></iframe>');
    }
};

})(require('jquery'));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/modules/downloadDocument.js
 ** module id = 1230
 ** module chunks = 96 97 232
 **/