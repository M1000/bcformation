// COMMON
require('../../scripts/registry.js');
require('../../../plugins/numericInput/jquery.numericInput.js');
require('../saleutils.js');
// NEW TEMPLATES
require('../cart.js');
require('numeral');
require('../checkoutButtonAction.js');
require('../currencyFormatter.js');
require('../addToCartOverlay.js');


(function ($) {
    "use strict";

    /** @const */
    var PATH_TO_SHOPPINGCART = '/app/shop/shoppingcart/';

    $.reg('CheckoutBasketController', function ($, opts, $co, utils, cart, addToCartOverlay) {

        var jimdoData = require('jimdoData');
        var self = this;

        this.init = function() {
            clearCartOnError(opts);
            $(document).ready(function () {
                ensureCheckoutIsStarted();
                bindSubmitClick();
            });

            utils.setOnSuccess(function() {
                initNumericInput($co);
            });
            initNumericInput($co);
            disableCheckoutIfCartIsEmpty();
        };

        this._removeArticleClick = function (e, itemId) {
            var $link = $(this),
                product = createProductFromElement($link.closest('[data-product-id]'));

            utils.loading(true);

            cart.removeProduct(product);

            $.sync({
                url: PATH_TO_SHOPPINGCART + 'remove',
                type: 'post',
                data: {
                    product_id: itemId,
                    mode: 'all'
                },
                success: function (r) {
                    // Update the checkout cart with the new prices
                    utils.loading(false);
                    $co.html(r.payload.checkout);
                    initNumericInput($co);
                    disableCheckoutIfCartIsEmpty();
                }
            });

            return false;
        };

        this._discountCodeButtonClick = function () {
            var discountCodeName = $('.cc-discount-code-input').val();
            applyDiscountCode(discountCodeName);
        };

        this._discountCodeInputKeypress = function (e) {
            //enter was pressed, submit the discount form
            if (e.which === 13) {
                var discountCodeName = $('.cc-discount-code-input').val();
                applyDiscountCode(discountCodeName);
                e.preventDefault();
            }
        };

        this._removeDiscountCodeClick = function () {
            removeDiscountCode();
        };

        // Called from numeric input plugin
        this._onNumericInputChange = function(value) {
            var $input = $(this),
                $productElement = $input.closest('[data-product-id]'),
                product = createProductFromElement($productElement);

            self._setAmountForProduct(product, value);
            utils.spinnerchange.apply(this, arguments);
        };

        this._deleteItemClick = function() {
            var $thisDiv = $(this);
            var $thisContainer = $thisDiv.parent();

            var itemId = $thisContainer.data('product-item-id');
            var hash = $thisContainer.data('product-hash');

            changeProductItemQuantity(0, itemId, hash, $thisDiv);
        };

        this._increaseItemClick = function() {
            var $thisDiv = $(this);
            var $thisContainer = $thisDiv.closest('[data-product-count]');

            var currentValue = $thisContainer.data('product-count');

            var itemId = $thisContainer.data('product-item-id');
            var hash = $thisContainer.data('product-hash');

            changeProductItemQuantity(currentValue + 1, itemId, hash, $thisDiv);
        };


        this._decreaseItemClick = function() {
            var $thisDiv = $(this);
            var $thisContainer = $thisDiv.closest('[data-product-count]');

            var currentValue = $thisContainer.data('product-count');

            var itemId = $thisContainer.data('product-item-id');
            var hash = $thisContainer.data('product-hash');

            changeProductItemQuantity(currentValue - 1, itemId, hash, $thisDiv);
        };

        this._onNumericInputMaxValueExceeded = function(pool) {
            var $productElement = $(this).closest('[data-product-id]');

            var productIdTuple = createProductFromElement($productElement);
            var product = cart.getProduct(productIdTuple);

            // we show the error using addToCartOverlay, so no overlay (old templates or mobile) no cry
            if (!addToCartOverlay) {
                return;
            }

            var message;

            if (pool === "1") {
                message = jimdoData.tr.shop.oneLeftText;
            }
            else {
                message = jimdoData.tr.shop.numLeftText.replace('{:num}', pool);
            }

            addToCartOverlay.error(product, message);
        };

        this._setAmountForProduct = function(product, value) {
            cart.setAmountForProduct(product, value);
        };

        this._addDiscountCodeResponsiveClick = function(event) {
            var discountCodeName = $('.j-checkout__discount-code-cash .j-checkout__input-field').val();
            applyDiscountCode(discountCodeName);
            event.preventDefault();
        };

        this._removeDiscountCodeResponsiveClick = function(event) {
            event.preventDefault();
            removeDiscountCode();
        };

        function clearCartOnError(opts) {
            if (opts.status === 'error') {
                cart.clear();
            }
        }

        function ensureCheckoutIsStarted() {
            var clientSideCartItemCount = cart.getCartItemCount();
            if (opts.status !== 'error' && !opts.hasProductEntries && clientSideCartItemCount > 0) {
                var alreadyBeenThere = window.location.search.match(/startCheckout/);

                if (!alreadyBeenThere) {

                    $.sync({
                        url: '/j/start-checkout',
                        type: 'post',
                        contentType: 'application/json',
                        data: JSON.stringify(cart.getCart()),
                        complete: function (jqXHR) {
                            var redirectUrl = jqXHR.getResponseHeader('Location');

                            if (redirectUrl) {
                                window.location.href = redirectUrl + '?startCheckout';
                            }
                        },
                        dataType: 'json'
                    });
                }
            }
        }

        function removeDiscountCode() {
            $.sync({
                url: '/app/shop/shoppingcart/deletediscountcodes',
                type: 'post',
                success: function (r) {
                    var $container = $('#cc-checkout');
                    $container.html(r.payload.checkout);
                    initNumericInput($container);
                }
            });
        }

        function applyDiscountCode(discountCodeName) {
            $.sync({
                url: '/app/shop/shoppingcart/applydiscountCode',
                type: 'post',
                data: {
                    discount_code_name: discountCodeName
                },
                success: function (r) {
                    if (r.status === 'success') {
                        // update basket view
                        var $container = $('#cc-checkout');
                        $container.html(r.payload.checkout);
                        initNumericInput($container);
                    } else {
                        $.each(r.errors, function (name, msg) {
                            $(document).find('[name="' + name + '"]').eq(0).message(msg, {status: 'error'});
                            $('.j-checkout__discount-code-cash .j-checkout__input-field').message(msg, {status: 'error'});
                        });
                    }
                }
            });
        }

        // XXX #79107 this shouldn't exist
        // there should probably be a common container with a shopping cart/first checkout view without products,
        // so it can be styled nicely. But we don't have time to change markup because of goal #1/publish.
        function disableCheckoutIfCartIsEmpty() {
            var $nextStep = $('.cc-checkout-btn[type=submit]'),
                $userNote = $('.cc-checkout-user-note-form');
            if ($('.cc-empty-cart').length) {
                $nextStep.hide();
                $userNote.hide();
            } else {
                $nextStep.show();
                $userNote.show();
            }
        }

        function createProductFromElement($product) {
            return {
                productId: $product.data('product-id'),
                variantId: '' + $product.data('variant-id') || null
            };
        }

        function initNumericInput($scope) {
            $scope.find('.cc-m-input-numeric').numericInput({
                change: self._onNumericInputChange,
                maxValueExceeded: self._onNumericInputMaxValueExceeded
            });
        }

        function changeProductItemQuantity(newAmount, itemId, hash, $thisDiv) {
            var data = {};
            data.newcount = newAmount;
            data.item_id = itemId;
            data.hash = hash;

            var $productElement = $thisDiv.closest('.j-checkout__product');
            var product = createProductFromElement($productElement);

            $.sync({
                url: PATH_TO_SHOPPINGCART + 'updatecount',
                type: 'post',
                data: data,
                success: function(r) {
                    if ( r.status === 'success' ) {
                        // update the side cart or checkout with the new prices

                        var $container = $('#cc-checkout');
                        $container.html(r.payload.checkout);
                        self._setAmountForProduct(product, newAmount);
                        initNumericInput($container);

                    } else {
                        $thisDiv.message(r);
                    }
                }
            });
        }

        function bindSubmitClick() {
            $('#cc-checkout-submit-0').click(function (e) {
                e.preventDefault();
                var $form = $('#cc-checkout-paymentmethod-form');
                var $cartItemInput = $('<input type="hidden" name="cart_items">');
                $form.append($cartItemInput);
                $cartItemInput.val(JSON.stringify(cart.getCart()));
                $form.submit();
            });
        }
    });

    $.modules.checkoutbasket = function (opts) {
        var prefix = 'cc-checkout';
        // The shopping cart in the checkout
        var $co = $('#' + prefix);

        var utils = $.factory('SaleUtils', [{
            getContainer: function() {
                return $co;
            }
        }]);

        var shoppingCart = $.factory('ShoppingCartFactory').create();
        var addToCartOverlay = $.factory('AddToCartOverlayFactory').create();
        var controller = $.factory('CheckoutBasketController', [opts, $co, utils, shoppingCart, addToCartOverlay]);

        // Init the action controller on the checkout area
        $('#' + prefix + '-wrapper').actionController(controller, {events: 'change click focusout submit keypress'});
    };

    })(require('jquery'));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/shop/checkout/basket.js
 ** module id = 1533
 ** module chunks = 96 97
 **/