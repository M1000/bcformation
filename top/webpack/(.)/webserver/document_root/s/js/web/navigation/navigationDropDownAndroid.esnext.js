import $ from 'jquery';

const NAVIGATION_ITEM_WITH_CHILDREN = '[data-dropdown="true"] .j-nav-has-children';

function fixDropDownNavigationForAndroid() {
    const navigationItemsWithChildren = $(NAVIGATION_ITEM_WITH_CHILDREN);

    if (!('ontouchstart' in window)) {
        return;
    }

    if (document.body.clientWidth < 768) { // Do not intercept on phones
        return;
    }

    navigationItemsWithChildren.each(function () { // eslint-disable-line func-names
        let expandedNavigation = null;

        $(this).on('click', function (event) { // eslint-disable-line func-names
            const item = $(this);

            if (!expandedNavigation || item[0] !== expandedNavigation[0]) {
                event.preventDefault();
                expandedNavigation = item;
            }
        });

        $(document).on('click touchstart MSPointerDown', (e) => {
            if (!expandedNavigation) {
                return;
            }

            const parents = $(e.target).parents('.j-nav-has-children').toArray();
            const clickedOnExpandedNavigation = parents
                .reduce((sameOrParent, parent) => sameOrParent || parent === expandedNavigation[0], false);

            if (!clickedOnExpandedNavigation) {
                expandedNavigation = null;
            }
        });
    });
}

// Only activate if requested by the template
if ($(NAVIGATION_ITEM_WITH_CHILDREN).length >= 1) {
    fixDropDownNavigationForAndroid();
}



/** WEBPACK FOOTER **
 ** ./webserver/document_root/s/js/web/navigation/navigationDropDownAndroid.esnext.js
 **/