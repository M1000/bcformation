require('../scripts/bootstrap.js');
require('./catalog/catalogoptions.js');

(function($, jimdoData){
    "use strict";

$.modules.catalog = function (opts) {
    var $container = $('.cc-catalog-container', opts.selector),
        $products = $('.cc-webview-product', $container);
    if(jimdoData.isMobile()){
        $products.css({
            display: 'block',
            margin: '0 auto'
        });
        return;
    }
    var catalogOptions = $.factory('CatalogOptions');

    $(function() {
        $container
            .show()
            .parent()
            .trigger('moduleresize');

        catalogOptions
            .fixMargin($container, $products.css('margin-right'))
            .initSlider($container);

        $(window).on('resize', function() {
            catalogOptions
                .fixMargin($container, $products.css('margin-right'))
                .initSlider($container);
        });
    });
};

})(require('jquery'), require('jimdoData'));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/modules/catalog.js
 ** module id = 1228
 ** module chunks = 96 97 232
 **/