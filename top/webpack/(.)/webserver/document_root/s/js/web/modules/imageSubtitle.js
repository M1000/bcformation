require('../scripts/bootstrap.js');
require('../scripts/setup.js');
require('../../plugins/pinterest/jquery.pinterest.js');

(function($, EnlargeableGallery){
    "use strict";

    $.modules.imageSubtitle = function( opts ) {
        var pinterestOption = opts.data && opts.data.pinterest && opts.data.pinterest == '1';

        if (opts.selector) {
            new EnlargeableGallery($(opts.selector), {
                sharing: pinterestOption ? ['pinterest'] : null
            });

        }
        if (pinterestOption && !('ontouchstart' in document.documentElement)) {
            $('#cc-m-imagesubtitle-image-' + opts.data.id).pinterest();
        }
    };

})(require('jquery'), require('./enlargeable.js'));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/modules/imageSubtitle.js
 ** module id = 1234
 ** module chunks = 96 97 232
 **/