require('../../plugins/pinterest/jquery.pinterest.js');
require('../scripts/bootstrap.js');

(function($, EnlargeableGallery){
    "use strict";

    $.modules.textWithImage = function( opts ) {
        var pinterestOption = opts.data && opts.data.pinterest && opts.data.pinterest == '1';

        if (opts.selector) {
            new EnlargeableGallery($(opts.selector), {
                sharing: pinterestOption ? ['pinterest'] : null
            });

        }
        if (pinterestOption && !('ontouchstart' in document.documentElement)) {
            $('#cc-m-textwithimage-image-' + opts.data.id).pinterest();
        }
    };

})(require('jquery'), require('./enlargeable.js'));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/modules/textWithImage.js
 ** module id = 1237
 ** module chunks = 96 97 232
 **/