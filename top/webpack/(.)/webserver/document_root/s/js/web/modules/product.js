require('../scripts/bootstrap.js');

require('../shop/cart.js');
require('./product/productController.js');
require('../../plugins/superzoom/jquery.superzoom.js');
require('../shop/checkoutButtonAction');
require('numeral');
require('../shop/currencyFormatter.js');
require('../shop/addToCartOverlay.js');
var Cookies = require('js-cookie');

(function($, jimdoData, EnlargeableGallery){
    "use strict";

    function isMobile() {
        try {
            return Cookies.get('mobile') === '1' || "ontouchstart" in document.documentElement;
        } catch(e) {
            return false;
        }
    }

$.modules.product = function (opts) {

    // Don´t initialize product module within CMS
    if(jimdoData && jimdoData.isCMS){
        return;
    }

    var $module = $(opts.selector),
        // The zoom element
        $superZoom = $('#cc-product-superzoom-' + opts.moduleId),
        // The div around the main image
        $confines = $('.cc-shop-product-img-confines', $module),
        cart = $.factory('ShoppingCartFactory').create(),
        addToCartOverlay = $.factory('AddToCartOverlayFactory').create();

    var productController = $.factory('ProductController', [opts, $module, $superZoom, $confines, isMobile(), activateSuperZoom, cart, addToCartOverlay]);
    $module.actionController(productController, {events: 'change click'});

    if (isMobile()) {
        var $thumbnailContainer = $('.cc-shop-product-alternatives', $module);

        if ($('img', $thumbnailContainer).length === 0) {
            new EnlargeableGallery($module);
        } else {
            var gallery = new EnlargeableGallery($thumbnailContainer);

            $superZoom.on('click', function(event) {
                event.stopPropagation();
                event.preventDefault();

                gallery.openGallery(0);
            });
        }
    } else {
        activateSuperZoom(opts, $superZoom);
    }
};

function activateSuperZoom(opts, $sz) {
    // The mobile version has no zoom
    if (isMobile()) {
        return false;
    }

    // If no product image has been uploaded, there is no zoom
    if (!$sz.length) {
        return false;
    }

    if (!opts.superzoom) {
        // If the user didn't enable zoom, make sure the zoom element doesn't link anywhere
        $sz.click(function(){return false;});
        return false;
    }

    // Activate the zoom
    $sz.superzoom($('#product-desc-' + opts.moduleId));
}
})(require('jquery'), require('jimdoData'), require('./enlargeable.js'));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/modules/product.js
 ** module id = 1235
 ** module chunks = 96 97 232
 **/