require('../../plugins/flash/jquery.flash.js');
require('../../../sass/web/module/functional/oldmoduleinterface/video.sass');

(function($, jimdoData){
    "use strict";

$.modules.video = function (opts) {
    var $iframe = $('#cc-m-video-container-' + opts.id),
        $module = $('#cc-m-' + opts.id);

    if (opts.variant === 'default') {
        if (jimdoData.isMobile()) {
            var multiplier = opts.useContentSize === 1 ? 3/4 : 9/16,
                width = $iframe.parent().width(),
                height = parseInt(width * multiplier, 10);

            $iframe.attr({width: width, height: height});
            $module.trigger('moduleresize', opts.id);
        }
    } else {
        if ( !opts.videos ) {
            appendVideo($(opts.selector), opts.type, opts.options);
        } else {
            $.each(opts.videos, function(i,opts){
                appendVideo($(opts.selector), opts.type, opts.options);
            });
        }

        $module.trigger('moduleresize', opts.id);
    }

    function appendVideo($element, type, options) {
        if(!$element.length) {
            throw new Error('Missing target element');
        }

        if (type && type === 'iframe') {
            var  $iframe = $('<iframe />')
                .attr(options.attr)
                .attr('src', options.src);

            $iframe.appendTo($element)
                .wrap('<div />')
                .parent('div')
                    .css({
                        display: 'block',
                        overflow: 'hidden',
                        height: options.attr.height +'px',
                        width: options.attr.width+'px'
                    });

            // XXX: Remove this ugly div hack until a fix for the youtube player scrolling bug is available
            // @todo: Looks like we can test and fix it by June 23rd
            // @see: http://groups.google.com/group/youtube-api-gdata/browse_thread/thread/7490cb2d0aa69120
            window.setTimeout(function(){
                var currentHeight = parseInt($iframe.height(), 10),
                    height = options.attr.height;

                if (currentHeight && currentHeight > height) {
                    $iframe.css({
                        position: 'relative',
                        top: Math.floor((height - currentHeight)/2)
                    });
                }
            }, 500);
        }
        else {
            $element.flash(options);
        }
    }
};

})(require('jquery'), require('jimdoData'));




/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/modules/video.js
 ** module id = 1239
 ** module chunks = 96 97 232
 **/