require('../../plugins/contentBox/jquery.contentBox.js');

(function($){
    "use strict";

$.modules.promotion = function ( params ) {
    if ( /packages|features/.test(params.variant) ) {
        $(params.containerSelector).bind('click', function(e){
            if ( $(e.target).attr('data-action') == 'jumpto' ) {
                location.href = $(e.target).attr('data-params');
            }
            return false;
        })
        .add($(params.selector)).contentBox({width:740, height:500});
    }
    else if ( params.variant == 'shopoverview' ) {
        $(params.containerSelector).bind('click', function(e){
            if ( $(e.target).attr('data-action') == 'jumpto' ) {
                location.href = $(e.target).attr('data-params');
            }
            return false;
        });
    }
    else if ( params.variant == 'sidebarteaser') {
        $(params.selector).children()
        .hover(function(){
            $(this).find('h4:first, div.cc-sidebarteaser-btn').addClass('cc-state-hover');
        }, function(){
            $(this).find('h4:first, div.cc-sidebarteaser-btn').removeClass('cc-state-hover');
        })
        .bind('click', function(){
            location.href = $(this).attr('data-params');
            return false;
        });

    }

};

})(require('jquery'));




/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/modules/promotion.js
 ** module id = 1236
 ** module chunks = 96 97 232
 **/