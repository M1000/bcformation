(function ($) {
    "use strict";
    var jimDoge = (window._jimDoge = window._jimDoge || []);

    $(function () {
        var jimdoData = require('jimdoData'),
            $body = $('body');

        if (!jimdoData || !jimdoData.copyableHeadlineLinks) {
            return;
        }

        $body
            .addClass('j-copyable-headlines')
            .on('keydown', function (event) {
                if (event.altKey) {
                    $body.addClass('j-alt-pressed');
                }
            })
            .on('keyup', function () {
                $body.removeClass('j-alt-pressed');
            })
            .on('click', 'h1, h2, h3, h4, h5, h6', function (event) {
                var $headline = $(this),
                    HIDE_SUCCESS_MESSAGE_DELAY = 3000;

                if (!this.id || !$body.hasClass('j-alt-pressed')) {
                    return;
                }

                event.preventDefault();

                copyText(retrieveCurrentUrlWithoutHash() + '#' + this.id);

                $headline.addClass('j-link-copy-success');

                window.setTimeout(function () {
                    $headline.removeClass('j-link-copy-success');
                }, HIDE_SUCCESS_MESSAGE_DELAY);

                jimDoge.push(['event', 'Internal Feature', 'Copy headline link', 'copy']);
            });

        function copyText(text) {
            var $textInput = $('<input/>')
                .val(text)
                .appendTo($body)
                .select();

            try {
                document.execCommand('copy');
            } catch (e) {
            }

            $textInput.remove();
        }

        function retrieveCurrentUrlWithoutHash() {
            return window.location.href.replace(window.location.hash, '');
        }
    });
})(require('jquery'));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/scripts/copyableHeadlineLinks.js
 ** module id = 3713
 ** module chunks = 96
 **/