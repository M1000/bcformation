/*jshint esnext: true */

"use strict";

import $ from 'jquery';
import {authUrl, websiteId} from 'jimdoData';

const LOGINSTATE_ENDPOINT = 'app/web/loginstate';

$(function(){
    $.ajax({
        url: authUrl + LOGINSTATE_ENDPOINT,
        dataType: 'jsonp',
        jsonp: 'callback',
        contentType: "application/json",
        data: {
            owi: websiteId
        }
    }).always(function(data){
        var {loginstate} = data;

        $('.loggedin').toggle(!!loginstate);
        $('.loggedout').toggle(!loginstate);
    });
});



/** WEBPACK FOOTER **
 ** ./webserver/document_root/s/js/web/login/loginstate.esnext.js
 **/