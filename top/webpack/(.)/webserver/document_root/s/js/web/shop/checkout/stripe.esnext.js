require('./paymentform.js');
require('plugins/xLazyLoader/jquery.xLazyLoader.js');
import rolloutIsActive from 'common/rolloutIsActive.esnext.js';

(function ($) {
    var messages,
        $form,
        stripe,
        card;

    function stripeTokenHandler(token) {
        $form.append($('<input type="hidden" name="stripe_token" />').val(token));
        $.modules.checkoutpaymentform.submit();
    }

    /* global Stripe */
    function retrieveToken() {
        if(!rolloutIsActive){
            retrieveTokenPreStripeMigration();
        } else {
            const owner = {
                name: $form.find('.payment-stripe-card-customer-name').val(),
                address: {
                    line1: $form.find('.payment-stripe-card-customer-line1').val(),
                    city: $form.find('.payment-stripe-card-customer-city').val(),
                    country: $form.find('.payment-stripe-card-customer-country').val(),
                    postal_code: $form.find('.payment-stripe-card-customer-zip').val()
                }
            };

            stripe.createSource(card, { owner: owner }).then(function (result) {
                if (result.error) {
                    // Inform the user if there was an error
                    var $errorElement = $('#payment-stripe-card__errors');
                    $errorElement.text(result.error.message);
                    $errorElement.addClass('message-warning');
                } else {
                    // Send the token to our server
                    stripeTokenHandler(result.source.id);
                }
            });
        }
    }

    function retrieveTokenPreStripeMigration() {
        var cardData = {
            name: $form.find('.payment-stripe-card-customer-name').val(),
            address_line1: $form.find('.payment-stripe-card-customer-line1').val(),
            address_city: $form.find('.payment-stripe-card-customer-city').val(),
            address_zip: $form.find('.payment-stripe-card-customer-zip').val(),
            address_country: $form.find('.payment-stripe-card-customer-country').val(),
        };
        stripe.createToken(card, cardData).then(function(result) {
            if (result.error) {
                // Inform the user if there was an error
                var $errorElement = $('#payment-stripe-card__errors');
                $errorElement.text(result.error.message);
                $errorElement.addClass('message-warning');
            } else {
                // Send the token to your server
                stripeTokenHandler(result.token.id);
            }
        });
    }

    $.modules.checkoutstripe = function (params) {
        $form = $.modules.checkoutpaymentform.initForm();

        $.xLazyLoader({
            js: ["https://cdn.polyfill.io/v2/polyfill.min.js?features=Promise", "https://js.stripe.com/v3/"],
            jsKey: null,
            success: function () {
                stripe = Stripe(params.publicKey);
                messages = params.messages;
                $.modules.checkoutpaymentform.bindPaymentMethod(4096, retrieveToken);
                card = stripe.elements().create('card', {hidePostalCode: true});
                card.mount('#payment-stripe-card');

                card.addEventListener('change', function (event) {
                    var $errorElement = $('#payment-stripe-card__errors');
                    if (event.error) {
                        $errorElement.text(event.error.message);
                        $errorElement.addClass('message-warning');
                    } else {
                        $errorElement.text('');
                        $errorElement.removeClass('message-warning');
                    }
                });
            }
        });
    };

})(require('jquery'), window);



/** WEBPACK FOOTER **
 ** ./webserver/document_root/s/js/web/shop/checkout/stripe.esnext.js
 **/