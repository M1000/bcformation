import { CookieControl } from './CookieControl.esnext';
import { UrlHelper } from '../helpers/UrlHelper.esnext';

const cookiePrefix = 'ckies_';
const previewStorageKey = 'preview_sid';

// eslint-disable-next-line import/prefer-default-export
export class CookieControlPreview extends CookieControl {

    deny(key) {
        localStorage.removeItem(cookiePrefix + key);
    }

    allow(key) {
        localStorage.setItem(cookiePrefix + key, 'allow');
    }

    isNewSid(previewSid) {
        return localStorage.getItem(previewStorageKey) !== previewSid;
    }

    setNewSid(previewSid) {
        localStorage.setItem(previewStorageKey, previewSid);
    }

    isCookieAllowed(key) {
        this.handlePreview();
        return localStorage.getItem(cookiePrefix + key) === 'allow';
    }

    handlePreview() {
        const previewSid = this.getSidParam();
        if (previewSid !== null && this.isNewSid(previewSid)) {
            this.clearCookiePrefixStorageItems();
            this.setNewSid(previewSid);
        }
    }

    getSidParam() {
        const params = UrlHelper.getParams(window.location.href);
        return params.preview_sid !== undefined ? params.preview_sid : null;
    }

    clearCookiePrefixStorageItems() {
        Object.keys(localStorage).forEach(key => {
            if (key.indexOf(cookiePrefix) === 0) {
                localStorage.removeItem(key);
            }
        });
    }
}



/** WEBPACK FOOTER **
 ** ./webserver/document_root/s/js/web/modules/cookieControl/CookieControlPreview.esnext.js
 **/