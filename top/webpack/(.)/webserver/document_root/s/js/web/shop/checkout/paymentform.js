(function($) {
    "use strict";

    var boundMethods = {},
        $form,
        done = false,
        initialized = false;

    $.modules.checkoutpaymentform = {
        initForm: function() {
            if (initialized) {
                return $form;
            }
            initialized = true;
            $form = $('#cc-checkout-paymentmethod-form');

            $form.on('submit', function(e) {
                if (done) {
                    return true;
                }
                var paymentMethod = $form.find("input:radio[name=payment_method]:checked").val(),
                    boundMethod = boundMethods[paymentMethod];

                if($.isFunction(boundMethod)) {
                    boundMethod(e);
                    return false;
                }

                //no method with special js was chosen just submit the form
                return true;
            });

            return $form;
        },
        bindPaymentMethod: function(id, callback) {
            boundMethods[id] = callback;
        },
        submit: function() {
            done = true;
            $form.submit();
        }
    };

    /* global module */
    module.exports = $.modules.checkoutpaymentform;

})(require('jquery'));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/web/shop/checkout/paymentform.js
 ** module id = 612
 ** module chunks = 96 97
 **/