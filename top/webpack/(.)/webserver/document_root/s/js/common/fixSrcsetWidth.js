// this file can be removed as soon as the FF bug is fixed:
// https://bugzilla.mozilla.org/show_bug.cgi?id=1149357

(function($, window) {
    "use strict";

    window.setSrcSetImgWidth = function(imgToResizeId) {
        var imgToResize = document.querySelectorAll('.cc-imagewrapper img')[imgToResizeId];
        imgToResize.style.width = imgToResize.offsetWidth + 'px';
    };

    function updateSrcSetImages() {
        if (window.devicePixelRatio && window.devicePixelRatio > 1) {
            var i = 0,
                imagesWithSrcSets = document.querySelectorAll('.cc-imagewrapper img'),
                listLength = imagesWithSrcSets.length;

            for (; i < listLength; ++i) {
                if (imagesWithSrcSets[i]) {
                    var imgToResize = imagesWithSrcSets[i];
                    imgToResize.style.width = '';

                    setTimeout(
                        "window.setSrcSetImgWidth(" + i + ")",
                        100
                    );
                }
            }
        }
    }

    if (navigator.userAgent.indexOf('Firefox') !== -1) {
        $(function() {
            updateSrcSetImages();

            $(window).on('resize', updateSrcSetImages);
        });
    }
}(require('jquery'), window));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/common/fixSrcsetWidth.js
 ** module id = 1526
 ** module chunks = 96 232
 **/