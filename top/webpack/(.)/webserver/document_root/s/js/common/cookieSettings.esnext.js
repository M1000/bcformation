import { ckies } from '@ckies/library';

import * as CookieLaw from 'web/footer/cookielaw';

const handleEvent = (event) => {
    if (event.target) {
        const type = event.target.getAttribute('data-cookie-type');

        if (!!event.target.checked) {
            ckies.allow(type);
            CookieLaw.hideCookieLaw();
        } else {
            ckies.deny(type);
        }
    }
};

const initCheckbox = (item) => {
    const type = item.getAttribute('data-cookie-type');

    if (ckies.use(type)) {
        item.setAttribute('checked', 'checked');
    } else {
        item.removeAttribute('checked');
    }

    item.addEventListener('change', handleEvent);
};

(function($, window) {
    "use strict";

    $(function() {
        const inputs = document.querySelectorAll(`input[data-cookie-type]`)
        if (inputs.length > 0) {
            inputs.forEach(initCheckbox);
        }
    });
}(require('jquery'), window));



/** WEBPACK FOOTER **
 ** ./webserver/document_root/s/js/common/cookieSettings.esnext.js
 **/