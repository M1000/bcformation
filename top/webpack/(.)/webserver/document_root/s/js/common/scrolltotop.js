require('jquery-ui/ui/effect.js');

(function($, _) {
    "use strict";

    /**
     * @const
     * @type {string}
     */
    var CLASS_BUTTON = 'cc-FloatingButtonBarContainer-button-scroll';

    /**
     * @const
     * @type {string}
     */
    var CLASS_CONTAINER = 'cc-FloatingButtonBarContainer';

    /**
     * @const
     * @type {string}
     */
    var CLASS_BUTTON_SHOW = CLASS_BUTTON + '-show';

    /**
     *
     * @enum {number}
     * @const
     * @type {{interval: number, scrollTime: number}}
     */
    var DEFAULTS = {
        interval: 250,
        scrollTime: 500,
        yPos: 400
    };

    $.modules.scrolltotop = function(opts) {
        var $window = $(window),
            $scrollToTopContainer = $('.' + CLASS_CONTAINER),
            $scrollToTopButton = $('.' + CLASS_BUTTON),
            scrolling = false;

        var options = $.extend({}, DEFAULTS, opts);

        function toggle(has) {
            $scrollToTopButton.toggleClass(CLASS_BUTTON_SHOW, has);
        }

        $window.on('scroll', _.throttle(function() {
            if (scrolling) {
                return;
            }
            toggle($window.scrollTop() > options.yPos);
        }, options.interval));

        // We initially add the .hidden class to the container, so the button does not appear unstyled
        // before the async web.css is loaded. It's safe to do it here, since we know web.css will be ready at this point
        $scrollToTopContainer.removeClass('hidden');

        $scrollToTopButton.on('click', function(e) {
            if (e) {
                e.preventDefault();
            }

            toggle(false);
            scrolling = true;
            $('html,body')
                .stop()
                .animate(
                {'scrollTop': 0 },
                options.scrollTime,
                'easeOutCirc',
                function() {
                    scrolling = false;
                });
        });
    };

    module.exports = $.modules.scrolltotop;
})(require('jquery'), require('lodash'));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/common/scrolltotop.js
 ** module id = 1222
 ** module chunks = 96 97 232
 **/