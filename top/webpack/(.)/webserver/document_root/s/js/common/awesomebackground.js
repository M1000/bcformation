require('../plugins/cycle/jquery.cycle.js');
require('imagesloaded/jquery.imagesloaded.js');
require('../plugins/fullscreensize/jquery.fullscreensize.js');
require('../external/scripts/froogaloop.js');

var Cookies = require('js-cookie');

(function($, window) {
    'use strict';

    $.modules.awesomebackground = function(data) {

        // TODO: pass these in over constants map
        var BG_TYPE_SLIDER = 2,
            BG_TYPE_VIDEO = 3,
            BG_SIZE_COVER = 4,
            BG_SLIDER_EFFECT_NONE = 0,
            BG_SLIDER_EFFECT_FADE = 1,
            BG_SLIDER_EFFECT_SCROLLLEFT = 2,
            BG_SLIDER_EFFECT_SCROLLRIGHT = 3,
            BG_SLIDER_EFFECT_SCROLLUP = 4,
            BG_SLIDER_EFFECT_SCROLLDOWN = 5,
            BG_SLIDER_SPEED_SLOW = 1,
            BG_SLIDER_SPEED_FAST = 2,
            FACTOR = 1.20,
            VIDEOSERVICE_VIMEO = 'vimeo',
            VIDEOSERVICE_YOUTUBE = 'youtu',
            VIDEO_COOKIE_PATH = '/',
            $bgVideoContainer = null;

        function onToggleContents(isContentVisible) {
            // this gets injected by the video implementations
        }

        /**
         * This works similar to dojo.connect
         *
         * @param fn the method to hook into the calling chain
         * @private
         */
        function _connectOnToggleContents(fn) {
            var old = onToggleContents;
            onToggleContents = function(isContentVisible) {
                // call the old method
                old(isContentVisible);

                // call the new method
                fn(isContentVisible);
            };
        }

        function init() {
            var type = data.bgtype,
                service,
                serviceInit = null,
                css;

            if (type === BG_TYPE_VIDEO && data.video) {
                // Background is video

                if(data.overlayStyle !== data.constants.bgConfig.BG_OVERLAY_STYLE_NONE && $.browser.webkit) {
                    // we have an overlay defined (e.g. some pattern that lies over the video)
                    // AND we are in WebKit

                    // disable scrolling of the background overlay
                    $('#' + data.constants.page.CC_INNER_ID).css('background-attachment','scroll');
                }

                // video background
                service = data.video.service;

                // vimeo has issues with iphone & ipads, please see #54810
                if (service === VIDEOSERVICE_VIMEO && (isIpad() || isIphone())) {
                    return;
                }

                if (service === VIDEOSERVICE_YOUTUBE) {
                    // The user selected a YouTube video
                    serviceInit = setupYoutube;
                } else if (service === VIDEOSERVICE_VIMEO) {
                    // The user selected a vimeo video
                    serviceInit = setupVimeo;
                }

                if (serviceInit) {
                    var currentId = service + '_' + data.video.movieId;
                    var VIDEO_COOKIE_KEY = data.constants.bgVideo.BG_VIDEO_COOKIE_KEY;


                    if(!isIE()) {
                        // usually we just hide our video, so it does not flicker
                        // we can't use display: none because otherwise the video does not play
                        css = 'visibility: hidden;';
                    } else {
                        // IE does not even like visibility, so we just move the video out of the viewport
                        css = 'left: -3000px;';
                    }

                    // we add this on the fly and hide it, so there is no flickering involved.
                    // Note, that display: none does not work due to the YouTube player constraints
                    $bgVideoContainer = $(
                        '<div id="' + data.constants.bgVideo.BG_VIDEO_CONTAINER_ID + '" style="' + css + '">' +
                        '</div>');

                    $bgVideoContainer.prependTo($('body'));

                    $(window).on('beforeunload',function() {
                        /**
                         * @seekPos    The seek position in seconds
                         */
                        getSeekPosition(function(seekPos) {
                            var current = {};

                            // we are going to save the video position when unloading the page (e.g. when using the navigation)
                            // so we can restart it at the position we were at
                            var vidCookie = Cookies.get(VIDEO_COOKIE_KEY);
                            var vidCookieObj = vidCookie && JSON.parse(vidCookie);
                            current[currentId] = seekPos;

                            var newCookieValue = $.extend({}, vidCookieObj, current);
                            var cookieOptions = {path: VIDEO_COOKIE_PATH};

                            Cookies.set(VIDEO_COOKIE_KEY, JSON.stringify(newCookieValue), cookieOptions);

                        });
                    });

                    // check if we know this video already and if it has been played - if not, seekPos is set to 0
                    var currentCookie = Cookies.get(VIDEO_COOKIE_KEY);
                    var currentVal = currentCookie && JSON.parse(Cookies.get(VIDEO_COOKIE_KEY));
                    var seekPos = (currentVal || {})[currentId] || 0;

                    // The visitor of the page muted background videos
                    var cookieMute = !!Cookies.get(data.constants.bgVideo.BG_VIDEO_MUTE_COOKIE_KEY);

                    // initialise our service - whichever this might be
                    serviceInit(!!data.videoMute, parseFloat(seekPos), cookieMute);

                    // set up the content hiding button
                    setupContentHideButton(!!data.videoHideContent);

                    // initialise the mute button state
                    initMuteButtonState(cookieMute);
                } else {
                    // If this comes up, an unknown video service was used - throw an error here?
                    /*jshint noempty:false */
                }
            } else if (type === BG_TYPE_SLIDER) {
                // slideshow background
                setupSlider();
            } else if (data.repeat === BG_SIZE_COVER) {
                // single (random) image
                if(!hasBackgroundSizeFeature()) {
                    handleBackgroundSizeWorkaroundSingleImage();
                }
            }

        }

        /**
         * This gets called once the background video is ready - it changes the visibility of the background video container
         * We need this because otherwise the video flickers (e.g. small video frame shows in black, then gets resized on fullscreen).
         * Call this once the initialisation has been done and there is something to show
         */
        function onBgVideoReady() {
            if(!isIE()) {
                // Makes the container (which is invisible by default)
                $bgVideoContainer.css('visibility','visible');
            } else {
                // IE does not like visibility, display: none or opacity: 0 (IE8 can't handle opacity)
                $bgVideoContainer.css('left','0px');
            }
        }

        /**
         *
         * @param cb The callback function to pass the seek position (in seconds) as the first parameter
         */
        var getSeekPosition = function(cb) {
            // this gets injected by the video implementations
        };

        /**
         * Sets up a YouTube video
         *
         * @param isMuted Whether the video is muted by default (true) or not (false)
         * @param seekPos The seeking position in seconds from the start of the video
         * @param cookieMute The visitor muted the video (true) or not (false)
         */
        function setupYoutube(isMuted, seekPos, cookieMute) {
            var videoId = data.video.movieId,
                tag,
                firstScriptTag,
                player,
                $ytPlayer;

            // Load the IFrame Player API code asynchronously.
            tag = document.createElement('script');
            tag.src = 'https://www.youtube.com/player_api';
            firstScriptTag = document.getElementsByTagName('script')[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

            $ytPlayer = $('<div id="' + data.constants.bgVideo.BG_VIDEO_PLAYER_ID + '"></div>');
            $bgVideoContainer.append($ytPlayer);

            // Replace the 'ytplayer' element with an <iframe> and
            // YouTube player after the API code downloads.
            window.onYouTubePlayerAPIReady = function() {

                var playerVars = {
                    wmode: 'transparent',
                    playlist: videoId, // this is needed for proper looping, because loop always references a playlist
                    autoplay: 0,
                    controls: 0,
                    modestbranding: 1,
                    showinfo: 0,
                    enablejsapi: 1 // Enable the JavaScript API (needed to mute/unmute the video)
                };

                if(isLinux() && isMozilla()) {
                    // We are on FF Linux...meaning we are fucked...
                    // By default the site gets unusable due to the background video - so let's switch to
                    // HTML5 - means we have no sound on most systems, though, oh well, Linux users...
                    playerVars.html5 = 1;
                }

                /* global YT: {} */
                player = new YT.Player(data.constants.bgVideo.BG_VIDEO_PLAYER_ID, {
                    videoId: videoId,
                    playerVars: playerVars,
                    events: {
                        onReady: function() {
                            // we need to get a reference to the YouTube player again here,
                            // because our old markup is replaced by whatever the Player API does with it (currently replace it with an iframe)
                            $ytPlayer = $('#' + data.constants.bgVideo.BG_VIDEO_PLAYER_ID);
                            var resizeFn = function() {
                                $ytPlayer.fullscreensize('resize');
                            };
                            // we need this because otherwise the scroll bar shows a free space which does not get filled up,
                            // because hiding the content does not resize the player
                            _connectOnToggleContents(resizeFn);

                            // Inject the seek position getter for YouTube
                            // We need this for page switches
                            getSeekPosition = function(cb) {
                                cb(player.getCurrentTime());
                            };

                            $(window).on('resize', resizeFn);

                            $ytPlayer.fullscreensize({factor: FACTOR});

                            player.seekTo(seekPos || 0, true);
                            player.playVideo();

                            if (isMuted || cookieMute) {
                                // video is muted by default
                                // OR the visitor muted it at some point in time
                                player.mute();
                            }

                            if (!isMuted) {
                                // video is not muted by default, so give the visitor the opportunity to mute any horrible harlem shake clips
                                setupVideoMuteButton(function(fn) { fn(player.isMuted()); }, function() { player.mute(); }, function() { player.unMute(); });
                            }

                            // init is done and we are ready to show some stuff
                            onBgVideoReady();
                        }
                    }

                });

                player.addEventListener('onStateChange', function(value) {
                    var data = value.data;

                    if (data == YT.PlayerState.ENDED) {
                        player.seekTo(0, true);
                        player.playVideo();
                    }
                });
            };
        }

        /**
         * Method to initialise the content hiding button
         *
         * @param isContentHideEnabled This is true if the user enabled the content hiding
         */
        function setupContentHideButton(isContentHideEnabled) {
            if (!isContentHideEnabled) {
                // Content hiding is not enabled, so this is basically a no-op
                return;
            }
            var $button = $('.' + data.constants.floatingButtonBar.SHOW_FULL_VIDEO_CLASS),
                Controller = function () {
                    this._enableClick = function () {
                        var $content = $('#' + data.constants.page.CC_INNER_ID),
                            $other = $(
                                [
                                    '#' + data.constants.cms.LOGINBOX_ID
                                ].join(',')),
                            isContentVisible,
                            text;

                        // toggle the visibility of the content
                        $content.toggle();

                        // get the current visibility
                        isContentVisible = $content.is(':visible');

                        // hide other elements
                        $other.toggle(isContentVisible);

                        // change the icon
                        $button.toggleClass(data.constants.floatingButtonBar.ENABLED_BUTTON_CLASS, !isContentVisible);

                        // and change the text on the title and the content of span
                        text = isContentVisible ? data.i18n.hideContent : data.i18n.showContent;
                        $button.find('a').attr('title', text).find('span').html(text);

                        // make sure the video gets resized,
                        // otherwise the scroll bar might be hidden and leave a blank space on the right
                        onToggleContents(isContentVisible);
                    };
                };
            $button.actionController(new Controller(), {events: 'click'});
        }

        /**
         * THis initialises the state of the mute button
         *
         * @param isMute Whether the user muted background videos (true) or not (false)
         */
        function initMuteButtonState(isMute) {
            var $button = $('.' + data.constants.floatingButtonBar.MUTE_BUTTON_CLASS),
                text;

            // change the icon
            $button.toggleClass(data.constants.floatingButtonBar.ENABLED_BUTTON_CLASS, isMute);

            // and change the text on the title and the content of span
            text = isMute ? data.i18n.unmute : data.i18n.mute;
            $button.find('a').attr('title', text).find('span').html(text);

            // We hide it by default to avoid flickering whilst we change the state
            // based on whether the visitor muted the video or not
            $button.css('visibility','visible');
        }

        /**
         * Method to initialise the video player mute button
         *
         * @param isMuteFn This method returns true if the player is muted currently, false if not
         * @param muteFn   This method can be called to mute the video player
         * @param unmuteFn This methid unmutes the video player
         */
        function setupVideoMuteButton(isMuteFn, muteFn, unmuteFn) {
            var $button = $('.' + data.constants.floatingButtonBar.MUTE_BUTTON_CLASS),
                Controller;

            Controller = function() {
                this._enableClick = function() {

                    // check if it is muted
                    isMuteFn(function(isMute) {
                        // toggle
                        (isMute ? unmuteFn : muteFn)();

                        // we switched
                        isMute = !isMute;

                        // keep whether the
                        Cookies.set(data.constants.bgVideo.BG_VIDEO_MUTE_COOKIE_KEY, isMute, {
                            path: VIDEO_COOKIE_PATH
                        });

                        // change the state
                        initMuteButtonState(isMute);
                    });
                };
            };
            $button.actionController(new Controller(), {events: 'click'});
        }

        /**
         * @see parameters of the setupYoutube method
         */
        /* global $f */
        function setupVimeo(isMuted, seekPos, cookieMute) {
            var $iframe = $('<iframe id="' + data.constants.bgVideo.BG_VIDEO_PLAYER_ID + '" src="https://player.vimeo.com/video/' + data.video.movieId + '?api=1&loop=1&player_id=' + data.constants.bgVideo.BG_VIDEO_PLAYER_ID + '" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>'),
                player = $f($iframe[0]);

            $bgVideoContainer.append($iframe);

            player.addEvent('ready', function() {
                var muteFn,
                    isMuteFn,
                    unmuteFn,
                    resizeFn;

                muteFn = function() {
                    player.api('setVolume', 0);
                };

                if (isMuted || cookieMute) {
                    // video is muted by default
                    // OR the visitor muted it at some point
                    muteFn();
                }

                if (!isMuted) {
                    // video is not muted by default, so give the visitor the opportunity
                    // to mute the endless Gangnam Style
                    isMuteFn = function(fn) {
                        player.api('getVolume', function (value, player_id) {
                            if (player_id === data.constants.bgVideo.BG_VIDEO_PLAYER_ID) {
                                // yep, this is our player
                                // and the value is 0, e.g. muted
                                // Attention, vimeo returns a string "0.000"
                                fn(parseInt(value, 10) === 0);
                            }
                        });
                    };

                    unmuteFn = function() {
                        player.api('setVolume', 1);
                    };
                    setupVideoMuteButton(isMuteFn, muteFn, unmuteFn);
                }

                // start the video
                player.api('play');

                // Inject the seek position getter for Vimeo
                // We need this for page switches
                getSeekPosition = function(cb) {
                    player.api('getCurrentTime', cb);
                };

                // check if we have a seek position
                if(seekPos) {
                    // this only works in the HTML player if the load progress is before
                    // the position we want to seek to - in flash, we completely stall, see below...
                    player.api('seekTo', seekPos);
                }

                resizeFn = function() {
                    $iframe.fullscreensize('resize');
                };

                // we need this because otherwise the scroll bar shows a free space which does not get filled up,
                // because hiding the content does not resize the player
                _connectOnToggleContents(resizeFn);

                $(window).on('resize', resizeFn);

                //This is part of the handling for the vimeo flash player
                //if(!seekPos) {
                // init is done and we are ready to show some stuff
                onBgVideoReady();
                //}
            });

            $iframe.fullscreensize({factor: FACTOR});
        }

        function setupSlider() {
            var ipad = isIpad() ? 'background-attachment:scroll;' : '',
                $slider = $('<div id="cc-jimdo-bgr-slider"></div>').prependTo($('body')),
                backgroundImageSizeCover = hasBackgroundSizeFeature(),
                URL_PLACEHOLDER = '{url}',
                li = backgroundImageSizeCover ? '<li style="' + ipad + 'background-image: url('+URL_PLACEHOLDER+');" /></li>' : '<li><img src="'+URL_PLACEHOLDER+'" /></li>',
                html = '',
                length = data.images.length,
                i;

            for (i = 0; i < 2 && i < length; i++) {
                html += li.replace(URL_PLACEHOLDER, data.images[i]);
            }

            $('<ul>' + html + '</ul>')
                .appendTo($slider)
                .cycle({
                    fit: 0,
                    slideResize: 0,
                    speed: getSpeed(),
                    timeout: getTimeout(),
                    fx: getFx(),
                    before: function (curr, next, options) {
                        if (!backgroundImageSizeCover) {
                            handleBackgroundSizeWorkaroundSlideshow($(next));
                        }

                        if (!options.addSlide || i === length) {
                            return;
                        }

                        options.addSlide(li.replace(URL_PLACEHOLDER, data.images[i]));

                        i++;
                    }
                });
        }

        function getSpeed() {
            if (data.effect === BG_SLIDER_EFFECT_NONE) {
                return getTimeout();
            }

            return 2000;
        }

        function getFx() {
            switch (data.effect) {
                case BG_SLIDER_EFFECT_FADE:
                    return 'fade';
                case BG_SLIDER_EFFECT_SCROLLLEFT:
                    return 'scrollLeft';
                case BG_SLIDER_EFFECT_SCROLLRIGHT:
                    return 'scrollRight';
                case BG_SLIDER_EFFECT_SCROLLUP:
                    return 'scrollUp';
                case BG_SLIDER_EFFECT_SCROLLDOWN:
                    return 'scrollDown';
            }

            return 'none';
        }

        function getTimeout() {
            switch (data.speed) {
                case BG_SLIDER_SPEED_SLOW:
                    return 12000;
                case BG_SLIDER_SPEED_FAST:
                    return 2000;
            }

            return 8000;
        }

        function isLinux() {
            return (/Linux/i).test(navigator.platform);
        }

        function isMozilla() {
            return !!$.browser.mozilla;
        }

        function isIE() {
            return !!$.browser.msie;
        }

        function hasBackgroundSizeFeature() {
            return !isIE();
        }

        function isIpad() {
            return navigator && navigator.userAgent && /iPad/i.test(navigator.userAgent);
        }

        function isIphone() {
            return navigator && navigator.userAgent && /iPhone/i.test(navigator.userAgent);
        }

        function handleBackgroundSizeWorkaroundSlideshow($li) {
            var $img = $li.find('img'),
                oldDisplayValue,
                oldVisibilityValue;

            oldDisplayValue = $img.css('display');
            oldVisibilityValue = $img.css('visibility');

            $img.add($li).css({
                display: 'block',
                visibility: 'visible'
            });

            $img.fullscreensize('resize');

            $img.add($li).css({
                display: oldDisplayValue,
                visibility: oldVisibilityValue
            });
        }

        function handleBackgroundSizeWorkaroundSingleImage() {
            var $body = $('body'),
                url = $body.css('background-image'),
                image = url.replace(/^url\(["']?/, '').replace(/["']?\)$/, ''),
                $img;

            $img = $('<div id="cc-awesome-bg-single-image"><img src="' + image + '" ></div>')
                .prependTo($body)
                .find('img');

            $img.imagesLoaded(function() {
                $img.fullscreensize();
            });

            $(window).on('resize', function() {
                $img.fullscreensize('resize');
            });
        }
        init();
    };


})(require('jquery'), window);



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/common/awesomebackground.js
 ** module id = 1221
 ** module chunks = 96 97 232
 **/