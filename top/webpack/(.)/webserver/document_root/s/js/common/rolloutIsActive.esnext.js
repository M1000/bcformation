import { rollouts } from 'jimdoData';

export default key => rollouts && rollouts.indexOf(key) > -1;



/** WEBPACK FOOTER **
 ** ./webserver/document_root/s/js/common/rolloutIsActive.esnext.js
 **/