//apprently srcset is not yet supported by edge and will cause a rendering bug

(function($, window) {
    "use strict";

    function removeSrcSets(images) {
        for (var i = 0; i < images.length; i++) {
            images[i].removeAttribute('srcset');
        }
    }

    $(document).ready(function() {
        if (/Edge\/\d+/i.test(navigator.userAgent)) {
            var images = document.querySelectorAll('.cc-imagewrapper img');
            removeSrcSets(images);
        }
    });
}(require('jquery'), window));



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/common/fixSrcSetForEdge.js
 ** module id = 1525
 ** module chunks = 96 232
 **/