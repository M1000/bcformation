function transformImageUrl(url, transformation) {
    return url.replace(
        /\/transf\/(none|dimension=(\d*|orig)x(\d*|orig))(\/|:)/,
        '/transf/' + transformation + '$4'
    );
}

function isOriginal(url) {
    return url.match(/\/transf\/(none|dimension=origxorig)/);
}

module.exports = {
    bySize: function(url, width, height) {
        height = height || '';

        return transformImageUrl(url, 'dimension=' + width + 'x' + height);
    },

    original: function(url) {
        if (isOriginal(url)) {
            return url;
        }

        return transformImageUrl(url, 'dimension=origxorig');
    }
};



/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/js/modules/transformImageUrl.js
 ** module id = 1224
 ** module chunks = 96 232 234
 **/