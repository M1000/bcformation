var cbs = [], 
	data;
module.exports = function(cb) {
	if(cbs) cbs.push(cb);
	else cb(data);
}
require.ensure([], function(require) {
	data = require("!!./datepicker-it.js");
	var callbacks = cbs;
	cbs = null;
	for(var i = 0, l = callbacks.length; i < l; i++) {
		callbacks[i](data);
	}
}, "jqueryui/locale/datepicker-it");


/*****************
 ** WEBPACK FOOTER
 ** ./webserver/document_root/s/bower_components/jquery-ui/ui/i18n/datepicker-it.js
 ** module id = 850
 ** module chunks = 2 28 96 97 232
 **/