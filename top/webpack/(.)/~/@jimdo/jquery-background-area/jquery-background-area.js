(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("jquery"), require("bxslider"), require("@jimdo/jquery-fullscreensize"));
	else if(typeof define === 'function' && define.amd)
		define(["jquery", "bxslider", "@jimdo/jquery-fullscreensize"], factory);
	else {
		var a = typeof exports === 'object' ? factory(require("jquery"), require("bxslider"), require("@jimdo/jquery-fullscreensize")) : factory(root["jquery"], root["bxslider"], root["@jimdo/jquery-fullscreensize"]);
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(this, function(__WEBPACK_EXTERNAL_MODULE_5__, __WEBPACK_EXTERNAL_MODULE_9__, __WEBPACK_EXTERNAL_MODULE_12__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	module.exports = __webpack_require__(1);

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	__webpack_require__(2);
	
	__webpack_require__(4);
	
	__webpack_require__(6);
	
	__webpack_require__(7);
	
	__webpack_require__(8);
	
	__webpack_require__(11);
	
	__webpack_require__(21);

/***/ },
/* 2 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 3 */,
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _jquery = __webpack_require__(5);
	
	var _jquery2 = _interopRequireDefault(_jquery);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	_jquery2.default.backgroundArea = {};
	_jquery2.default.backgroundArea.types = {};
	_jquery2.default.backgroundArea.videoPlayers = {};
	_jquery2.default.backgroundArea.CONSTANTS = {
	  EVENT_NAMESPACE: '.backgroundArea'
	};

/***/ },
/* 5 */
/***/ function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_5__;

/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _jquery = __webpack_require__(5);
	
	var _jquery2 = _interopRequireDefault(_jquery);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var CLASS_WEB_COLOR = 'jdbga-web--color';
	
	(function () {
	  'use strict';
	
	  var module = {};
	
	  module.init = function ($container, background) {
	    $container.css({
	      backgroundColor: background.color
	    }).addClass(CLASS_WEB_COLOR);
	  };
	
	  module.destroy = function ($container) {
	    $container.removeClass(CLASS_WEB_COLOR).css({
	      backgroundColor: ''
	    });
	  };
	
	  _jquery2.default.backgroundArea.types.color = module;
	})();

/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.CLASS_WEB_IMAGE = undefined;
	
	var _jquery = __webpack_require__(5);
	
	var _jquery2 = _interopRequireDefault(_jquery);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var CLASS_WEB_IMAGE = exports.CLASS_WEB_IMAGE = 'jqbga-web--image';
	
	(function () {
	  'use strict';
	
	  var module = {};
	
	  var $container = null;
	
	  module.init = function ($someContainer, background) {
	    var image = background.images[0];
	    $container = $someContainer;
	    $container.css({
	      backgroundImage: 'url(' + (image.previewUrl || image.url) + ')'
	    }).addClass(CLASS_WEB_IMAGE);
	
	    if (image.focalPointX || image.focalPointY) {
	      module.setFocalPoint(image.focalPointX, image.focalPointY);
	    }
	  };
	
	  module.setFocalPoint = function (x, y) {
	    $container.css({
	      backgroundPosition: x + '% ' + y + '%'
	    });
	  };
	
	  module.destroy = function ($container) {
	    $container.removeClass(CLASS_WEB_IMAGE).css({
	      backgroundImage: '',
	      backgroundPosition: ''
	    });
	  };
	
	  _jquery2.default.backgroundArea.types.picture = module;
	})();

/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(jQuery) {'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.SLIDER_DATA_ATTR = exports.MAX_SLIDESHOW_PAUSE = exports.MIN_SLIDESHOW_PAUSE = exports.CLASS_SLIDER_IMAGE_WRAP = exports.CLASS_SLIDER_IMAGE = exports.CLASS_SLIDER_WRAP = undefined;
	
	var _jquery = __webpack_require__(5);
	
	var _jquery2 = _interopRequireDefault(_jquery);
	
	__webpack_require__(9);
	
	var _constants = __webpack_require__(10);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var CLASS_SLIDER_WRAP = exports.CLASS_SLIDER_WRAP = 'jqbga-slider';
	var CLASS_SLIDER_IMAGE = exports.CLASS_SLIDER_IMAGE = 'jqbga-slider--image';
	var CLASS_SLIDER_IMAGE_WRAP = exports.CLASS_SLIDER_IMAGE_WRAP = 'jqbga-slider--image--wrap';
	var MIN_SLIDESHOW_PAUSE = exports.MIN_SLIDESHOW_PAUSE = 5000; // must be a bit greater than the slideshows speed
	var MAX_SLIDESHOW_PAUSE = exports.MAX_SLIDESHOW_PAUSE = 30000;
	var SLIDER_DATA_ATTR = exports.SLIDER_DATA_ATTR = _constants.PLUGIN_NAME + 'slider';
	
	(function () {
	  'use strict';
	
	  var SLIDESHOW_SPEED = 2000;
	
	  var module = {};
	  var slider;
	
	  function triggerCurrentSlide($container, activeIndex) {
	    $container.trigger(jQuery.Event('changeSlide' + _jquery2.default.backgroundArea.CONSTANTS.EVENT_NAMESPACE, { index: activeIndex }));
	  }
	
	  function calculateSliderPause(options) {
	    // if the speed is numeric
	    // ensure its range is [0..100]
	    // and use it to adjust the pause between MIN_SLIDESHOW_PAUSE and MAX_SLIDESHOW_PAUSE
	    if (options && _jquery2.default.isNumeric(options.speed)) {
	      var speed = parseFloat(options.speed);
	      speed = Math.min(100, Math.max(0, speed));
	      return Math.floor(MIN_SLIDESHOW_PAUSE + (MAX_SLIDESHOW_PAUSE - MIN_SLIDESHOW_PAUSE) / 100 * speed);
	    }
	
	    return MAX_SLIDESHOW_PAUSE;
	  }
	
	  function fixBxSliderContainerHeight(slider) {
	    slider.closest('.bx-viewport').css('height', '100%');
	  }
	
	  var $wrap = null;
	
	  module.init = function ($container, background) {
	    module.goToSlide = null;
	    $wrap = (0, _jquery2.default)('<div class="' + CLASS_SLIDER_IMAGE_WRAP + '">');
	    var imgTemplate = '<div class="' + CLASS_SLIDER_IMAGE + '">';
	
	    background.images.forEach(function (image, index) {
	      var imgElem = (0, _jquery2.default)(imgTemplate);
	
	      imgElem.css('background-image', 'url(' + (image.previewUrl || image.url) + ')');
	
	      $wrap.append(imgElem);
	
	      if (image.focalPointX && image.focalPointY) {
	        module.setFocalPoint(image.focalPointX, image.focalPointY, index);
	      }
	    });
	
	    $container.append($wrap);
	
	    slider = $wrap.bxSlider({
	      keyboardEnabled: false,
	      touchEnabled: false,
	      mode: 'fade',
	      pager: false,
	      controls: false,
	      speed: SLIDESHOW_SPEED,
	      startSlide: background.$currentPreviewImage || 0,
	      pause: calculateSliderPause(background.options),
	      wrapperClass: CLASS_SLIDER_WRAP,
	      auto: false,
	      onSlideNext: function onSlideNext(slide, oldIndex, activeIndex) {
	        triggerCurrentSlide($container, activeIndex);
	        return true;
	      },
	      onSliderResize: function onSliderResize() {
	        fixBxSliderContainerHeight(slider);
	      },
	      onSlideAfter: function onSlideAfter() {
	        if (module.goToSlide !== null) {
	          slider.goToSlide(module.goToSlide);
	          module.goToSlide = null;
	        }
	      }
	    });
	
	    fixBxSliderContainerHeight(slider);
	
	    if (background.$playing !== false) {
	      module.play();
	    } else {
	      module.pause();
	    }
	
	    (0, _jquery2.default)(window).on('orientationchange', function () {
	      slider.redrawSlider();
	      fixBxSliderContainerHeight(slider);
	    });
	
	    $container.data(SLIDER_DATA_ATTR, slider);
	    triggerCurrentSlide($container, slider.getCurrentSlide());
	  };
	
	  module.play = function () {
	    slider.startAuto();
	  };
	
	  module.pause = function () {
	    slider.stopAuto();
	  };
	
	  module.setCurrentImage = function (index) {
	    if (slider.isWorking()) {
	      module.goToSlide = index;
	    } else {
	      slider.goToSlide(index);
	    }
	  };
	
	  module.setFocalPoint = function (x, y, index) {
	    (0, _jquery2.default)($wrap.children()[index]).css('background-position', x + '% ' + y + '%');
	  };
	
	  module.destroy = function ($container) {
	    $wrap = null;
	    var slider = $container.data(SLIDER_DATA_ATTR);
	
	    if (slider) {
	      slider.destroySlider();
	    }
	
	    $container.find('.' + CLASS_SLIDER_IMAGE_WRAP).remove();
	  };
	
	  module._calculateSliderPause = calculateSliderPause;
	
	  _jquery2.default.backgroundArea.types.slideshow = module;
	})();
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(5)))

/***/ },
/* 9 */
/***/ function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_9__;

/***/ },
/* 10 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	var PLUGIN_NAME = exports.PLUGIN_NAME = 'backgroundArea';
	var PLUGIN_DATA_ATTR = exports.PLUGIN_DATA_ATTR = 'plugin_' + PLUGIN_NAME;

/***/ },
/* 11 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.RESIZE_FACTOR = exports.EVENT_NAMESPACE = exports.STORAGE_KEY = exports.CLASS_WEB_VIDEO = undefined;
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };
	
	var _jquery = __webpack_require__(5);
	
	var _jquery2 = _interopRequireDefault(_jquery);
	
	__webpack_require__(12);
	
	var _canAutoplay = __webpack_require__(13);
	
	var _canAutoplay2 = _interopRequireDefault(_canAutoplay);
	
	__webpack_require__(15);
	
	__webpack_require__(20);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var CLASS_WEB_VIDEO = exports.CLASS_WEB_VIDEO = 'jqbga-web--video';
	var STORAGE_KEY = exports.STORAGE_KEY = 'jdbga-video';
	var EVENT_NAMESPACE = exports.EVENT_NAMESPACE = '.jdbga-video';
	var RESIZE_FACTOR = exports.RESIZE_FACTOR = 1.20;
	
	(function () {
	  //jshint ignore: line
	  'use strict';
	
	  var module = {};
	  var $window = (0, _jquery2.default)(window);
	
	  module._storeSeekPosition = function (player, movieId) {
	    player.getSeekPosition(function (seekPosition) {
	      var data = {};
	      data[movieId] = {
	        seekPosition: seekPosition
	      };
	      module._storeValue(player.type, data);
	    });
	  };
	
	  module._getPlayerType = function (url) {
	    var playerType = null;
	    _jquery2.default.each(_jquery2.default.backgroundArea.videoPlayers, function (_playerType, player) {
	      if (!playerType && player.movieIdRegex.test(url)) {
	        playerType = _playerType;
	      }
	    });
	
	    return playerType;
	  };
	
	  module._resize = function ($playerContainer, $backgroundContainer) {
	    if ($backgroundContainer.is('body')) {
	      $playerContainer.fullscreensize('resize');
	    } else {
	      module._resizeInContainer($playerContainer, $backgroundContainer);
	    }
	  };
	
	  module._computeRatio = function (a, b) {
	    return a / b;
	  };
	
	  module._calculateSizeToFitContainer = function ($backgroundContainer, $playerContainer) {
	    var computedRatio = this._computeRatio($playerContainer.height(), $playerContainer.width());
	    var computedHeight = $backgroundContainer.innerWidth() * computedRatio;
	
	    var newHeight, newWidth;
	
	    if (computedHeight >= $backgroundContainer.innerHeight()) {
	      newHeight = computedHeight;
	      newWidth = $backgroundContainer.innerWidth();
	    } else {
	      computedRatio = this._computeRatio($playerContainer.width(), $playerContainer.height());
	      newHeight = $backgroundContainer.innerHeight();
	      newWidth = newHeight * computedRatio;
	    }
	
	    return { height: newHeight, width: newWidth };
	  };
	
	  module._updatePlayerSize = function ($playerContainer, newSize) {
	    $playerContainer.height(newSize.height);
	    $playerContainer.width(newSize.width);
	  };
	
	  module._updatePlayerPosition = function ($playerContainer, marginType, marginValue) {
	    $playerContainer.css('margin', '0');
	    $playerContainer.css(marginType, marginValue * -1);
	  };
	
	  module._resizeInContainer = function ($playerContainer, $backgroundContainer) {
	    var computedMargin, marginType;
	
	    var newSize = this._calculateSizeToFitContainer($backgroundContainer, $playerContainer);
	
	    if (newSize.height >= $backgroundContainer.innerHeight()) {
	      computedMargin = (newSize.height - $backgroundContainer.innerHeight()) / 2;
	      marginType = 'margin-top';
	    } else {
	      computedMargin = (newSize.width - $backgroundContainer.innerWidth()) / 2;
	      marginType = 'margin-left';
	    }
	
	    this._updatePlayerSize($playerContainer, newSize);
	    this._updatePlayerPosition($playerContainer, marginType, computedMargin);
	  };
	
	  module._initResize = function ($playerContainer, $backgroundContainer) {
	    if ($backgroundContainer.is('body')) {
	      $playerContainer.fullscreensize({ factor: RESIZE_FACTOR });
	    } else {
	      module._resizeInContainer($playerContainer, $backgroundContainer);
	    }
	  };
	
	  module._sessionStorageExists = function () {
	    return _typeof(window.sessionStorage) === 'object';
	  };
	
	  module._getSessionStorage = function () {
	    return window.sessionStorage;
	  };
	
	  module._getStorage = function () {
	    if (module._sessionStorageExists()) {
	      return module._getSessionStorage();
	    }
	
	    return null;
	  };
	
	  module._getStoredValue = function (key) {
	    if (module._getStorage()) {
	      var item = module._getStorage().getItem(STORAGE_KEY);
	      if (item) {
	        var data = JSON.parse(item);
	        if (key in data) {
	          return data[key];
	        }
	      }
	    }
	
	    return undefined;
	  };
	
	  module._storeValue = function (key, value) {
	    if (module._getStorage()) {
	      var item = module._getStorage().getItem(STORAGE_KEY);
	      var data;
	      if (item) {
	        data = JSON.parse(item);
	      }
	
	      if (!data) {
	        data = {};
	      }
	
	      data[key] = value;
	
	      module._getStorage().setItem(STORAGE_KEY, JSON.stringify(data));
	    }
	  };
	
	  module._isValidUrl = function (url) {
	    return !!url && !!module._getPlayerType(url);
	  };
	
	  module._getMovieId = function (url) {
	    var playerType = module._getPlayerType(url);
	
	    if (playerType) {
	      return module._getPlayer(playerType).movieIdRegex.exec(url)[1];
	    }
	
	    return null;
	  };
	
	  module._getPlayer = function (playerType) {
	    return _jquery2.default.backgroundArea.videoPlayers[playerType];
	  };
	
	  module._setPreview = function ($container) {
	    return function (previewUrl) {
	      _jquery2.default.backgroundArea.types.picture.init($container, {
	        type: 'picture',
	        images: [{ url: previewUrl }]
	      });
	    };
	  };
	
	  module._init = function ($container, background, playerType, player) {
	    var $innerContainer = (0, _jquery2.default)('<div class="jqbga-container--inner" />');
	    var $playerWrapper = (0, _jquery2.default)('<div class="' + CLASS_WEB_VIDEO + '" />');
	    $container.prepend($innerContainer.append($playerWrapper));
	
	    /*
	     * initially hiding the player reduces flickering
	     */
	    $playerWrapper.css('opacity', 0);
	
	    var movieId = module._getMovieId(background.url);
	    var storedPlayerData = module._getStoredValue(player.type);
	    var storedMovieData = storedPlayerData ? storedPlayerData[movieId] : null;
	    var $playerContainer;
	
	    function storeSeek() {
	      module._storeSeekPosition(player, movieId);
	    }
	
	    function resize() {
	      module._resize($playerContainer, $container);
	    }
	
	    $window.on('beforeunload' + EVENT_NAMESPACE, storeSeek);
	
	    $container.on('destroy' + EVENT_NAMESPACE, function () {
	      $window.off('beforeunload' + EVENT_NAMESPACE, storeSeek);
	      $window.off('resize' + EVENT_NAMESPACE, resize);
	    });
	
	    player.init({
	      $playerWrapper: $playerWrapper,
	      background: background,
	      movieId: movieId,
	      onReady: function onPlayerReady(_$playerWrapper, _$playerContainer) {
	        // jshint ignore: line
	        $playerContainer = _$playerContainer;
	        _$playerWrapper.css('opacity', 1);
	        module._initResize($playerContainer, $container);
	
	        player.mute(function () {
	          player.seekTo(storedMovieData ? storedMovieData.seekPosition : null, function () {
	            player.play();
	          });
	        });
	
	        $window.off('resize' + EVENT_NAMESPACE, resize).on('resize' + EVENT_NAMESPACE, resize);
	      }
	    });
	  };
	
	  module.init = function ($container, background) {
	    //jshint ignore: line
	    if (!module._isValidUrl(background.url)) {
	      return;
	    }
	
	    var playerType = module._getPlayerType(background.url);
	    var player = module._getPlayer(playerType);
	
	    var movieId = module._getMovieId(background.url);
	
	    this.canAutoplay().done(function () {
	      this._init($container, background, playerType, player);
	    }.bind(this)).fail(function () {
	      player.getPreviewImageUrl(movieId).then(this._setPreview($container));
	    }.bind(this));
	  };
	
	  module.canAutoplay = function () {
	    var d = _jquery2.default.Deferred();
	    (0, _canAutoplay2.default)(function (autoplay) {
	      if (autoplay) {
	        return d.resolve();
	      }
	      return d.reject();
	    });
	    return d.promise();
	  };
	
	  module.destroy = function ($container) {
	    var $wrap = $container.find('.' + CLASS_WEB_VIDEO);
	
	    if ($wrap.length) {
	      $wrap.remove();
	    }
	
	    _jquery2.default.backgroundArea.types.picture.destroy($container);
	  };
	
	  _jquery2.default.backgroundArea.types.video = module;
	})();

/***/ },
/* 12 */
/***/ function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_12__;

/***/ },
/* 13 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _jquery = __webpack_require__(5);
	
	var _jquery2 = _interopRequireDefault(_jquery);
	
	__webpack_require__(14);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var getVideo = function getVideo() {
	  var video = document.createElement("video");
	  video.muted = true;
	
	  Object.defineProperty(video, "paused", {
	    value: true
	  });
	
	  return video;
	};
	
	var commonTest = function commonTest() {
	  var defer = _jquery2.default.Deferred();
	  ModernizrVideo.on('videoautoplay', defer.resolve);
	  return defer.promise();
	};
	
	var playReturnsPromise = function playReturnsPromise() {
	  var defer = _jquery2.default.Deferred();
	  var promise = defer.promise();
	
	  try {
	    var vid = getVideo();
	    var play = 'play' in vid && vid.play();
	
	    defer.resolve(!vid.paused || 'Promise' in window && play instanceof Promise);
	  } catch (e) {
	    defer.resolve(false);
	  }
	
	  return promise;
	};
	
	var playDoesNotReject = function playDoesNotReject() {
	  var defer = _jquery2.default.Deferred();
	  var promise = defer.promise();
	
	  try {
	    var vid = getVideo();
	    var play = vid.play().catch(function () {
	      return defer.resolve(false);
	    });
	
	    setTimeout(function () {
	      return defer.resolve(play.status !== 'rejected');
	    }, 100);
	  } catch (e) {
	    defer.resolve(false);
	  }
	
	  return promise;
	};
	
	var modernTest = function modernTest() {
	  var defer = _jquery2.default.Deferred();
	  var promise = defer.promise();
	
	  _jquery2.default.when(playReturnsPromise(), playDoesNotReject()).then(function (a, b) {
	    return defer.resolve(a && b);
	  });
	
	  return promise;
	};
	
	exports.default = function (callback) {
	  return _jquery2.default.when(commonTest(), modernTest()).done(function (commonTestResult, modernTestResult) {
	    return callback(commonTestResult || modernTestResult);
	  });
	};

/***/ },
/* 14 */
/***/ function(module, exports) {

	"use strict";
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };
	
	/* jshint ignore:start */
	/*! modernizr 3.3.1 (Custom Build) | MIT *
	 * https://modernizr.com/download/?-videoautoplay-setclasses !*/
	!function (A, e, o) {
	  function t(A, e) {
	    return (typeof A === "undefined" ? "undefined" : _typeof(A)) === e;
	  }function n() {
	    var A, e, o, n, a, l, i;for (var r in s) {
	      if (s.hasOwnProperty(r)) {
	        if (A = [], e = s[r], e.name && (A.push(e.name.toLowerCase()), e.options && e.options.aliases && e.options.aliases.length)) for (o = 0; o < e.options.aliases.length; o++) {
	          A.push(e.options.aliases[o].toLowerCase());
	        }for (n = t(e.fn, "function") ? e.fn() : e.fn, a = 0; a < A.length; a++) {
	          l = A[a], i = l.split("."), 1 === i.length ? Modernizr[i[0]] = n : (!Modernizr[i[0]] || Modernizr[i[0]] instanceof Boolean || (Modernizr[i[0]] = new Boolean(Modernizr[i[0]])), Modernizr[i[0]][i[1]] = n), c.push((n ? "" : "no-") + i.join("-"));
	        }
	      }
	    }
	  }function a(A) {
	    var e = h.className,
	        o = Modernizr._config.classPrefix || "";if (R && (e = e.baseVal), Modernizr._config.enableJSClass) {
	      var t = new RegExp("(^|\\s)" + o + "no-js(\\s|$)");e = e.replace(t, "$1" + o + "js$2");
	    }Modernizr._config.enableClasses && (e += " " + o + A.join(" " + o), R ? h.className.baseVal = e : h.className = e);
	  }function l() {
	    return "function" != typeof e.createElement ? e.createElement(arguments[0]) : R ? e.createElementNS.call(e, "http://www.w3.org/2000/svg", arguments[0]) : e.createElement.apply(e, arguments);
	  }function i(A, e) {
	    if ("object" == (typeof A === "undefined" ? "undefined" : _typeof(A))) for (var o in A) {
	      d(A, o) && i(o, A[o]);
	    } else {
	      A = A.toLowerCase();var t = A.split("."),
	          n = Modernizr[t[0]];if (2 == t.length && (n = n[t[1]]), "undefined" != typeof n) return Modernizr;e = "function" == typeof e ? e() : e, 1 == t.length ? Modernizr[t[0]] = e : (!Modernizr[t[0]] || Modernizr[t[0]] instanceof Boolean || (Modernizr[t[0]] = new Boolean(Modernizr[t[0]])), Modernizr[t[0]][t[1]] = e), a([(e && 0 != e ? "" : "no-") + t.join("-")]), Modernizr._trigger(A, e);
	    }return Modernizr;
	  }var c = [],
	      s = [],
	      r = { _version: "3.3.1", _config: { classPrefix: "", enableClasses: !0, enableJSClass: !0, usePrefixes: !0 }, _q: [], on: function on(A, e) {
	      var o = this;setTimeout(function () {
	        e(o[A]);
	      }, 0);
	    }, addTest: function addTest(A, e, o) {
	      s.push({ name: A, fn: e, options: o });
	    }, addAsyncTest: function addAsyncTest(A) {
	      s.push({ name: null, fn: A });
	    } },
	      Modernizr = function Modernizr() {};Modernizr.prototype = r, Modernizr = new Modernizr();var h = e.documentElement,
	      R = "svg" === h.nodeName.toLowerCase();Modernizr.addTest("video", function () {
	    var A = l("video"),
	        e = !1;try {
	      (e = !!A.canPlayType) && (e = new Boolean(e), e.ogg = A.canPlayType('video/ogg; codecs="theora"').replace(/^no$/, ""), e.h264 = A.canPlayType('video/mp4; codecs="avc1.42E01E"').replace(/^no$/, ""), e.webm = A.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/, ""), e.vp9 = A.canPlayType('video/webm; codecs="vp9"').replace(/^no$/, ""), e.hls = A.canPlayType('application/x-mpegURL; codecs="avc1.42E01E"').replace(/^no$/, ""));
	    } catch (o) {}return e;
	  });var d;!function () {
	    var A = {}.hasOwnProperty;d = t(A, "undefined") || t(A.call, "undefined") ? function (A, e) {
	      return e in A && t(A.constructor.prototype[e], "undefined");
	    } : function (e, o) {
	      return A.call(e, o);
	    };
	  }(), r._l = {}, r.on = function (A, e) {
	    this._l[A] || (this._l[A] = []), this._l[A].push(e), Modernizr.hasOwnProperty(A) && setTimeout(function () {
	      Modernizr._trigger(A, Modernizr[A]);
	    }, 0);
	  }, r._trigger = function (A, e) {
	    if (this._l[A]) {
	      var o = this._l[A];setTimeout(function () {
	        var A, t;for (A = 0; A < o.length; A++) {
	          (t = o[A])(e);
	        }
	      }, 0), delete this._l[A];
	    }
	  }, Modernizr._q.push(function () {
	    r.addTest = i;
	  }), Modernizr.addAsyncTest(function () {
	    function A(l) {
	      n++, clearTimeout(e);var c = l && "playing" === l.type || 0 !== a.currentTime;return !c && t > n ? void (e = setTimeout(A, o)) : (a.removeEventListener("playing", A, !1), i("videoautoplay", c), void a.parentNode.removeChild(a));
	    }var e,
	        o = 200,
	        t = 5,
	        n = 0,
	        a = l("video"),
	        c = a.style;if (!(Modernizr.video && "autoplay" in a)) return void i("videoautoplay", !1);c.position = "absolute", c.height = 0, c.width = 0;try {
	      if (Modernizr.video.ogg) a.src = "data:video/ogg;base64,T2dnUwACAAAAAAAAAABmnCATAAAAAHDEixYBKoB0aGVvcmEDAgEAAQABAAAQAAAQAAAAAAAFAAAAAQAAAAAAAAAAAGIAYE9nZ1MAAAAAAAAAAAAAZpwgEwEAAAACrA7TDlj///////////////+QgXRoZW9yYSsAAABYaXBoLk9yZyBsaWJ0aGVvcmEgMS4xIDIwMDkwODIyIChUaHVzbmVsZGEpAQAAABoAAABFTkNPREVSPWZmbXBlZzJ0aGVvcmEtMC4yOYJ0aGVvcmG+zSj3uc1rGLWpSUoQc5zmMYxSlKQhCDGMYhCEIQhAAAAAAAAAAAAAEW2uU2eSyPxWEvx4OVts5ir1aKtUKBMpJFoQ/nk5m41mUwl4slUpk4kkghkIfDwdjgajQYC8VioUCQRiIQh8PBwMhgLBQIg4FRba5TZ5LI/FYS/Hg5W2zmKvVoq1QoEykkWhD+eTmbjWZTCXiyVSmTiSSCGQh8PB2OBqNBgLxWKhQJBGIhCHw8HAyGAsFAiDgUCw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDAwPEhQUFQ0NDhESFRUUDg4PEhQVFRUOEBETFBUVFRARFBUVFRUVEhMUFRUVFRUUFRUVFRUVFRUVFRUVFRUVEAwLEBQZGxwNDQ4SFRwcGw4NEBQZHBwcDhATFhsdHRwRExkcHB4eHRQYGxwdHh4dGxwdHR4eHh4dHR0dHh4eHRALChAYKDM9DAwOExo6PDcODRAYKDlFOA4RFh0zV1A+EhYlOkRtZ00YIzdAUWhxXDFATldneXhlSFxfYnBkZ2MTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTEhIVGRoaGhoSFBYaGhoaGhUWGRoaGhoaGRoaGhoaGhoaGhoaGhoaGhoaGhoaGhoaGhoaGhoaGhoaGhoaGhoaGhESFh8kJCQkEhQYIiQkJCQWGCEkJCQkJB8iJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQREhgvY2NjYxIVGkJjY2NjGBo4Y2NjY2MvQmNjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRISEhUXGBkbEhIVFxgZGxwSFRcYGRscHRUXGBkbHB0dFxgZGxwdHR0YGRscHR0dHhkbHB0dHR4eGxwdHR0eHh4REREUFxocIBERFBcaHCAiERQXGhwgIiUUFxocICIlJRcaHCAiJSUlGhwgIiUlJSkcICIlJSUpKiAiJSUlKSoqEBAQFBgcICgQEBQYHCAoMBAUGBwgKDBAFBgcICgwQEAYHCAoMEBAQBwgKDBAQEBgICgwQEBAYIAoMEBAQGCAgAfF5cdH1e3Ow/L66wGmYnfIUbwdUTe3LMRbqON8B+5RJEvcGxkvrVUjTMrsXYhAnIwe0dTJfOYbWrDYyqUrz7dw/JO4hpmV2LsQQvkUeGq1BsZLx+cu5iV0e0eScJ91VIQYrmqfdVSK7GgjOU0oPaPOu5IcDK1mNvnD+K8LwS87f8Jx2mHtHnUkTGAurWZlNQa74ZLSFH9oF6FPGxzLsjQO5Qe0edcpttd7BXBSqMCL4k/4tFrHIPuEQ7m1/uIWkbDMWVoDdOSuRQ9286kvVUlQjzOE6VrNguN4oRXYGkgcnih7t13/9kxvLYKQezwLTrO44sVmMPgMqORo1E0sm1/9SludkcWHwfJwTSybR4LeAz6ugWVgRaY8mV/9SluQmtHrzsBtRF/wPY+X0JuYTs+ltgrXAmlk10xQHmTu9VSIAk1+vcvU4ml2oNzrNhEtQ3CysNP8UeR35wqpKUBdGdZMSjX4WVi8nJpdpHnbhzEIdx7mwf6W1FKAiucMXrWUWVjyRf23chNtR9mIzDoT/6ZLYailAjhFlZuvPtSeZ+2oREubDoWmT3TguY+JHPdRVSLKxfKH3vgNqJ/9emeEYikGXDFNzaLjvTeGAL61mogOoeG3y6oU4rW55ydoj0lUTSR/mmRhPmF86uwIfzp3FtiufQCmppaHDlGE0r2iTzXIw3zBq5hvaTldjG4CPb9wdxAme0SyedVKczJ9AtYbgPOzYKJvZZImsN7ecrxWZg5dR6ZLj/j4qpWsIA+vYwE+Tca9ounMIsrXMB4Stiib2SPQtZv+FVIpfEbzv8ncZoLBXc3YBqTG1HsskTTotZOYTG+oVUjLk6zhP8bg4RhMUNtfZdO7FdpBuXzhJ5Fh8IKlJG7wtD9ik8rWOJxy6iQ3NwzBpQ219mlyv+FLicYs2iJGSE0u2txzed++D61ZWCiHD/cZdQVCqkO2gJpdpNaObhnDfAPrT89RxdWFZ5hO3MseBSIlANppdZNIV/Rwe5eLTDvkfWKzFnH+QJ7m9QWV1KdwnuIwTNtZdJMoXBf74OhRnh2t+OTGL+AVUnIkyYY+QG7g9itHXyF3OIygG2s2kud679ZWKqSFa9n3IHD6MeLv1lZ0XyduRhiDRtrNnKoyiFVLcBm0ba5Yy3fQkDh4XsFE34isVpOzpa9nR8iCpS4HoxG2rJpnRhf3YboVa1PcRouh5LIJv/uQcPNd095ickTaiGBnWLKVWRc0OnYTSyex/n2FofEPnDG8y3PztHrzOLK1xo6RAml2k9owKajOC0Wr4D5x+3nA0UEhK2m198wuBHF3zlWWVKWLN1CHzLClUfuoYBcx4b1llpeBKmbayaR58njtE9onD66lUcsg0Spm2snsb+8HaJRn4dYcLbCuBuYwziB8/5U1C1DOOz2gZjSZtrLJk6vrLF3hwY4Io9xuT/ruUFRSBkNtUzTOWhjh26irLEPx4jPZL3Fo3QrReoGTTM21xYTT9oFdhTUIvjqTkfkvt0bzgVUjq/hOYY8j60IaO/0AzRBtqkTS6R5ellZd5uKdzzhb8BFlDdAcrwkE0rbXTOPB+7Y0FlZO96qFL4Ykg21StJs8qIW7h16H5hGiv8V2Cflau7QVDepTAHa6Lgt6feiEvJDM21StJsmOH/hynURrKxvUpQ8BH0JF7BiyG2qZpnL/7AOU66gt+reLEXY8pVOCQvSsBtqZTNM8bk9ohRcwD18o/WVkbvrceVKRb9I59IEKysjBeTMmmbA21xu/6iHadLRxuIzkLpi8wZYmmbbWi32RVAUjruxWlJ//iFxE38FI9hNKOoCdhwf5fDe4xZ81lgREhK2m1j78vW1CqkuMu/AjBNK210kzRUX/B+69cMMUG5bYrIeZxVSEZISmkzbXOi9yxwIfPgdsov7R71xuJ7rFcACjG/9PzApqFq7wEgzNJm2suWESPuwrQvejj7cbnQxMkxpm21lUYJL0fKmogPPqywn7e3FvB/FCNxPJ85iVUkCE9/tLKx31G4CgNtWTTPFhMvlu8G4/TrgaZttTChljfNJGgOT2X6EqpETy2tYd9cCBI4lIXJ1/3uVUllZEJz4baqGF64yxaZ+zPLYwde8Uqn1oKANtUrSaTOPHkhvuQP3bBlEJ/LFe4pqQOHUI8T8q7AXx3fLVBgSCVpMba55YxN3rv8U1Dv51bAPSOLlZWebkL8vSMGI21lJmmeVxPRwFlZF1CpqCN8uLwymaZyjbXHCRytogPN3o/n74CNykfT+qqRv5AQlHcRxYrC5KvGmbbUwmZY/29BvF6C1/93x4WVglXDLFpmbapmF89HKTogRwqqSlGbu+oiAkcWFbklC6Zhf+NtTLFpn8oWz+HsNRVSgIxZWON+yVyJlE5tq/+GWLTMutYX9ekTySEQPLVNQQ3OfycwJBM0zNtZcse7CvcKI0V/zh16Dr9OSA21MpmmcrHC+6pTAPHPwoit3LHHqs7jhFNRD6W8+EBGoSEoaZttTCZljfduH/fFisn+dRBGAZYtMzbVMwvul/T/crK1NQh8gN0SRRa9cOux6clC0/mDLFpmbarmF8/e6CopeOLCNW6S/IUUg3jJIYiAcDoMcGeRbOvuTPjXR/tyo79LK3kqqkbxkkMRAOB0GODPItnX3Jnxro/25Ud+llbyVVSN4ySGIgHA6DHBnkWzr7kz410f7cqO/Syt5KqpFVJwn6gBEvBM0zNtZcpGOEPiysW8vvRd2R0f7gtjhqUvXL+gWVwHm4XJDBiMpmmZtrLfPwd/IugP5+fKVSysH1EXreFAcEhelGmbbUmZY4Xdo1vQWVnK19P4RuEnbf0gQnR+lDCZlivNM22t1ESmopPIgfT0duOfQrsjgG4tPxli0zJmF5trdL1JDUIUT1ZXSqQDeR4B8mX3TrRro/2McGeUvLtwo6jIEKMkCUXWsLyZROd9P/rFYNtXPBli0z398iVUlVKAjFlY437JXImUTm2r/4ZYtMy61hf16RPJIU9nZ1MABAwAAAAAAAAAZpwgEwIAAABhp658BScAAAAAAADnUFBQXIDGXLhwtttNHDhw5OcpQRMETBEwRPduylKVB0HRdF0A";else {
	        if (!Modernizr.video.h264) return void i("videoautoplay", !1);a.src = "data:video/mp4;base64,AAAAIGZ0eXBpc29tAAACAGlzb21pc28yYXZjMW1wNDEAAAAIZnJlZQAAAs1tZGF0AAACrgYF//+q3EXpvebZSLeWLNgg2SPu73gyNjQgLSBjb3JlIDE0OCByMjYwMSBhMGNkN2QzIC0gSC4yNjQvTVBFRy00IEFWQyBjb2RlYyAtIENvcHlsZWZ0IDIwMDMtMjAxNSAtIGh0dHA6Ly93d3cudmlkZW9sYW4ub3JnL3gyNjQuaHRtbCAtIG9wdGlvbnM6IGNhYmFjPTEgcmVmPTMgZGVibG9jaz0xOjA6MCBhbmFseXNlPTB4MzoweDExMyBtZT1oZXggc3VibWU9NyBwc3k9MSBwc3lfcmQ9MS4wMDowLjAwIG1peGVkX3JlZj0xIG1lX3JhbmdlPTE2IGNocm9tYV9tZT0xIHRyZWxsaXM9MSA4eDhkY3Q9MSBjcW09MCBkZWFkem9uZT0yMSwxMSBmYXN0X3Bza2lwPTEgY2hyb21hX3FwX29mZnNldD0tMiB0aHJlYWRzPTEgbG9va2FoZWFkX3RocmVhZHM9MSBzbGljZWRfdGhyZWFkcz0wIG5yPTAgZGVjaW1hdGU9MSBpbnRlcmxhY2VkPTAgYmx1cmF5X2NvbXBhdD0wIGNvbnN0cmFpbmVkX2ludHJhPTAgYmZyYW1lcz0zIGJfcHlyYW1pZD0yIGJfYWRhcHQ9MSBiX2JpYXM9MCBkaXJlY3Q9MSB3ZWlnaHRiPTEgb3Blbl9nb3A9MCB3ZWlnaHRwPTIga2V5aW50PTI1MCBrZXlpbnRfbWluPTEwIHNjZW5lY3V0PTQwIGludHJhX3JlZnJlc2g9MCByY19sb29rYWhlYWQ9NDAgcmM9Y3JmIG1idHJlZT0xIGNyZj0yMy4wIHFjb21wPTAuNjAgcXBtaW49MCBxcG1heD02OSBxcHN0ZXA9NCBpcF9yYXRpbz0xLjQwIGFxPTE6MS4wMACAAAAAD2WIhAA3//728P4FNjuZQQAAAu5tb292AAAAbG12aGQAAAAAAAAAAAAAAAAAAAPoAAAAZAABAAABAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAACGHRyYWsAAABcdGtoZAAAAAMAAAAAAAAAAAAAAAEAAAAAAAAAZAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAEAAAAAAAgAAAAIAAAAAACRlZHRzAAAAHGVsc3QAAAAAAAAAAQAAAGQAAAAAAAEAAAAAAZBtZGlhAAAAIG1kaGQAAAAAAAAAAAAAAAAAACgAAAAEAFXEAAAAAAAtaGRscgAAAAAAAAAAdmlkZQAAAAAAAAAAAAAAAFZpZGVvSGFuZGxlcgAAAAE7bWluZgAAABR2bWhkAAAAAQAAAAAAAAAAAAAAJGRpbmYAAAAcZHJlZgAAAAAAAAABAAAADHVybCAAAAABAAAA+3N0YmwAAACXc3RzZAAAAAAAAAABAAAAh2F2YzEAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAgACAEgAAABIAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAY//8AAAAxYXZjQwFkAAr/4QAYZ2QACqzZX4iIhAAAAwAEAAADAFA8SJZYAQAGaOvjyyLAAAAAGHN0dHMAAAAAAAAAAQAAAAEAAAQAAAAAHHN0c2MAAAAAAAAAAQAAAAEAAAABAAAAAQAAABRzdHN6AAAAAAAAAsUAAAABAAAAFHN0Y28AAAAAAAAAAQAAADAAAABidWR0YQAAAFptZXRhAAAAAAAAACFoZGxyAAAAAAAAAABtZGlyYXBwbAAAAAAAAAAAAAAAAC1pbHN0AAAAJal0b28AAAAdZGF0YQAAAAEAAAAATGF2ZjU2LjQwLjEwMQ==";
	      }
	    } catch (s) {
	      return void i("videoautoplay", !1);
	    }a.setAttribute("autoplay", ""), a.style.cssText = "display:none", h.appendChild(a), setTimeout(function () {
	      a.addEventListener("playing", A, !1), e = setTimeout(A, o);
	    }, 0);
	  }), n(), a(c), delete r.addTest, delete r.addAsyncTest;for (var E = 0; E < Modernizr._q.length; E++) {
	    Modernizr._q[E]();
	  }A.ModernizrVideo = Modernizr;
	}(window, document);

/***/ },
/* 15 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _jquery = __webpack_require__(5);
	
	var _jquery2 = _interopRequireDefault(_jquery);
	
	var _playerMin = __webpack_require__(16);
	
	var _playerMin2 = _interopRequireDefault(_playerMin);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	(function () {
	  // jshint ignore: line
	  'use strict';
	
	  var vimeoPlayer = null;
	
	  var player = {
	    type: 'vimeo',
	    movieIdRegex: /vimeo\.com\/([\w-]+)/i
	  };
	
	  player._getVimeoPlayer = function ($iframe) {
	    return new _playerMin2.default($iframe[0]);
	  };
	
	  player._getIframe = function (movieId) {
	    return (0, _jquery2.default)('<iframe webkitAllowFullScreen mozallowfullscreen allowFullScreen />').attr({
	      frameborder: 0,
	      src: 'https://player.vimeo.com/video/' + movieId + '?' + ['loop=1', 'badge=0', 'byline=0', 'portrait=0', 'title=0'].join('&')
	    });
	  };
	
	  player._vimeoPlayerId = 0;
	
	  player.mute = function (cb) {
	    vimeoPlayer.setVolume(0).then(cb);
	  };
	
	  player.play = function () {
	    vimeoPlayer.play();
	  };
	
	  player.seekTo = function (seekPosition, cb) {
	    vimeoPlayer.getCurrentTime().then(function (currentTime) {
	      var seekTime = seekPosition || 0;
	      // Apparently Vimeo player doesn't like to be told to go to
	      // a position where it is already.
	      if (currentTime === seekTime) {
	        cb();
	        return;
	      }
	      vimeoPlayer.setCurrentTime(seekTime).then(cb);
	    });
	  };
	
	  player.init = function (config) {
	    var $iframe = player._getIframe(config.movieId);
	    player._vimeoPlayerId = player._vimeoPlayerId + 1;
	    $iframe.data('vimeoPlayerId', player._vimeoPlayerId);
	    $iframe.appendTo(config.$playerWrapper);
	    vimeoPlayer = player._getVimeoPlayer($iframe);
	
	    vimeoPlayer.ready().then(function () {
	      if (typeof config.onReady === 'function' && $iframe.data('vimeoPlayerId') === player._vimeoPlayerId) {
	        config.onReady(config.$playerWrapper, $iframe);
	      }
	    });
	  };
	
	  player.getSeekPosition = function (callback) {
	    vimeoPlayer.getCurrentTime().then(callback);
	  };
	
	  // vimeo uses underscore for their naming convention which jshint does not like
	  player.getPreviewImageUrl = function (movieId) {
	    var d = _jquery2.default.Deferred();
	
	    var vimeoApiUrl = 'https://vimeo.com/api/oembed.json?url=http%3A//vimeo.com/' + movieId;
	    _jquery2.default.get(vimeoApiUrl, function (data) {
	      /* jshint camelcase: false */
	      if (data && data.thumbnail_url) {
	        d.resolve(data.thumbnail_url);
	      }
	    });
	
	    return d.promise();
	  };
	
	  _jquery2.default.backgroundArea.videoPlayers[player.type] = player;
	})();

/***/ },
/* 16 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;/* WEBPACK VAR INJECTION */(function(global, setImmediate, __webpack_provided_window_dot_jQuery, jQuery) {/*! @vimeo/player v1.0.6 | (c) 2016 Vimeo | MIT License | https://github.com/vimeo/player.js */
	!function(e,t){ true?module.exports=t():"function"==typeof define&&define.amd?define(t):(e.Vimeo=e.Vimeo||{},e.Vimeo.Player=t())}(this,function(){"use strict";function e(e,t){return t={exports:{}},e(t,t.exports),t.exports}function t(e,t,n){var r=T.get(e.element)||{};t in r||(r[t]=[]),r[t].push(n),T.set(e.element,r)}function n(e,t){var n=T.get(e.element)||{};return n[t]||[]}function r(e,t,n){var r=T.get(e.element)||{};if(!r[t])return!0;if(!n)return r[t]=[],T.set(e.element,r),!0;var o=r[t].indexOf(n);return o!==-1&&r[t].splice(o,1),T.set(e.element,r),r[t]&&0===r[t].length}function o(e,t){var n=T.get(e);T.set(t,n),T.delete(e)}function i(e,t){return 0===e.indexOf(t.toLowerCase())?e:""+t.toLowerCase()+e.substr(0,1).toUpperCase()+e.substr(1)}function a(e){return e instanceof window.HTMLElement}function u(e){return!isNaN(parseFloat(e))&&isFinite(e)&&Math.floor(e)==e}function s(e){return/^(https?:)?\/\/(player.)?vimeo.com(?=$|\/)/.test(e)}function c(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.id,n=e.url,r=t||n;if(!r)throw new Error("An id or url must be passed, either in an options object or as a data-vimeo-id or data-vimeo-url attribute.");if(u(r))return"https://vimeo.com/"+r;if(s(r))return r.replace("http:","https:");if(t)throw new TypeError("“"+t+"” is not a valid video id.");throw new TypeError("“"+r+"” is not a vimeo.com url.")}function f(e){for(var t=arguments.length<=1||void 0===arguments[1]?{}:arguments[1],n=_,r=Array.isArray(n),o=0,n=r?n:n[Symbol.iterator]();;){var i;if(r){if(o>=n.length)break;i=n[o++]}else{if(o=n.next(),o.done)break;i=o.value}var a=i,u=e.getAttribute("data-vimeo-"+a);(u||""===u)&&(t[a]=""===u?1:u)}return t}function l(e){var t=arguments.length<=1||void 0===arguments[1]?{}:arguments[1];return new Promise(function(n,r){if(!s(e))throw new TypeError("“"+e+"” is not a vimeo.com url.");var o="https://vimeo.com/api/oembed.json?url="+encodeURIComponent(e);for(var i in t)t.hasOwnProperty(i)&&(o+="&"+i+"="+encodeURIComponent(t[i]));var a="XDomainRequest"in window?new XDomainRequest:new XMLHttpRequest;a.open("GET",o,!0),a.onload=function(){if(404===a.status)return void r(new Error("“"+e+"” was not found."));if(403===a.status)return void r(new Error("“"+e+"” is not embeddable."));try{var t=JSON.parse(a.responseText);n(t)}catch(e){r(e)}},a.onerror=function(){var e=a.status?" ("+a.status+")":"";r(new Error("There was an error fetching the embed code from Vimeo"+e+"."))},a.send()})}function h(e,t){var n=e.html;if(!t)throw new TypeError("An element must be provided");if(null!==t.getAttribute("data-vimeo-initialized"))return t.querySelector("iframe");var r=document.createElement("div");return r.innerHTML=n,t.appendChild(r.firstChild),t.setAttribute("data-vimeo-initialized","true"),t.querySelector("iframe")}function d(){var e=arguments.length<=0||void 0===arguments[0]?document:arguments[0],t=[].slice.call(e.querySelectorAll("[data-vimeo-id], [data-vimeo-url]")),n=function(e){"console"in window&&console.error&&console.error("There was an error creating an embed: "+e)},r=function(){if(i){if(a>=o.length)return"break";u=o[a++]}else{if(a=o.next(),a.done)return"break";u=a.value}var e=u;try{if(null!==e.getAttribute("data-vimeo-defer"))return"continue";var t=f(e),r=c(t);l(r,t).then(function(t){return h(t,e)}).catch(n)}catch(e){n(e)}};e:for(var o=t,i=Array.isArray(o),a=0,o=i?o:o[Symbol.iterator]();;){var u,s=r();switch(s){case"break":break e;case"continue":continue}}}function p(e){return"string"==typeof e&&(e=JSON.parse(e)),e}function v(e,t,n){if(e.element.contentWindow.postMessage){var r={method:t};void 0!==n&&(r.value=n);var o=parseFloat(navigator.userAgent.toLowerCase().replace(/^.*msie (\d+).*$/,"$1"));o>=8&&o<10&&(r=JSON.stringify(r)),e.element.contentWindow.postMessage(r,e.origin)}}function y(e,t){t=p(t);var o=[],i=void 0;if(t.event){if("error"===t.event)for(var a=n(e,t.data.method),u=a,s=Array.isArray(u),c=0,u=s?u:u[Symbol.iterator]();;){var f;if(s){if(c>=u.length)break;f=u[c++]}else{if(c=u.next(),c.done)break;f=c.value}var l=f,h=new Error(t.data.message);h.name=t.data.name,l.reject(h),r(e,t.data.method,l)}o=n(e,"event:"+t.event),i=t.data}else t.method&&(o=n(e,t.method),i=t.value,r(e,t.method));for(var d=o,v=Array.isArray(d),y=0,d=v?d:d[Symbol.iterator]();;){var m;if(v){if(y>=d.length)break;m=d[y++]}else{if(y=d.next(),y.done)break;m=y.value}var g=m;try{if("function"==typeof g){g.call(e,i);continue}g.resolve(i)}catch(e){}}}var m="undefined"!=typeof Array.prototype.indexOf,g="undefined"!=typeof window.postMessage;if(!m||!g)throw new Error("Sorry, the Vimeo Player API is not available in this browser.");var w="undefined"!=typeof window?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:{},b=(e(function(e,t){!function(e){function t(e,t){function r(e){return this&&this.constructor===r?(this._keys=[],this._values=[],this._itp=[],this.objectOnly=t,void(e&&n.call(this,e))):new r(e)}return t||w(e,"size",{get:y}),e.constructor=r,r.prototype=e,r}function n(e){this.add?e.forEach(this.add,this):e.forEach(function(e){this.set(e[0],e[1])},this)}function r(e){return this.has(e)&&(this._keys.splice(g,1),this._values.splice(g,1),this._itp.forEach(function(e){g<e[0]&&e[0]--})),-1<g}function o(e){return this.has(e)?this._values[g]:void 0}function i(e,t){if(this.objectOnly&&t!==Object(t))throw new TypeError("Invalid value used as weak collection key");if(t!=t||0===t)for(g=e.length;g--&&!b(e[g],t););else g=e.indexOf(t);return-1<g}function a(e){return i.call(this,this._values,e)}function u(e){return i.call(this,this._keys,e)}function s(e,t){return this.has(e)?this._values[g]=t:this._values[this._keys.push(e)-1]=t,this}function c(e){return this.has(e)||this._values.push(e),this}function f(){(this._keys||0).length=this._values.length=0}function l(){return v(this._itp,this._keys)}function h(){return v(this._itp,this._values)}function d(){return v(this._itp,this._keys,this._values)}function p(){return v(this._itp,this._values,this._values)}function v(e,t,n){var r=[0],o=!1;return e.push(r),{next:function(){var i,a=r[0];return!o&&a<t.length?(i=n?[t[a],n[a]]:t[a],r[0]++):(o=!0,e.splice(e.indexOf(r),1)),{done:o,value:i}}}}function y(){return this._values.length}function m(e,t){for(var n=this.entries();;){var r=n.next();if(r.done)break;e.call(t,r.value[1],r.value[0],this)}}var g,w=Object.defineProperty,b=function(e,t){return e===t||e!==e&&t!==t};"undefined"==typeof WeakMap&&(e.WeakMap=t({delete:r,clear:f,get:o,has:u,set:s},!0)),"undefined"!=typeof Map&&"function"==typeof(new Map).values&&(new Map).values().next||(e.Map=t({delete:r,has:u,get:o,set:s,keys:l,values:h,entries:d,forEach:m,clear:f})),"undefined"!=typeof Set&&"function"==typeof(new Set).values&&(new Set).values().next||(e.Set=t({has:a,add:c,delete:r,clear:f,keys:h,values:h,entries:p,forEach:m})),"undefined"==typeof WeakSet&&(e.WeakSet=t({delete:r,add:c,clear:f,has:a},!0))}("undefined"!=typeof t&&"undefined"!=typeof w?w:window)}),e(function(e){!function(t,n,r){n[t]=n[t]||r(),"undefined"!=typeof e&&e.exports?e.exports=n[t]:"function"=="function"&&__webpack_require__(19)&&!(__WEBPACK_AMD_DEFINE_RESULT__ = function(){return n[t]}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__))}("Promise","undefined"!=typeof w?w:w,function(){function e(e,t){h.add(e,t),l||(l=p(h.drain))}function t(e){var t,n=typeof e;return null==e||"object"!=n&&"function"!=n||(t=e.then),"function"==typeof t&&t}function n(){for(var e=0;e<this.chain.length;e++)r(this,1===this.state?this.chain[e].success:this.chain[e].failure,this.chain[e]);this.chain.length=0}function r(e,n,r){var o,i;try{n===!1?r.reject(e.msg):(o=n===!0?e.msg:n.call(void 0,e.msg),o===r.promise?r.reject(TypeError("Promise-chain cycle")):(i=t(o))?i.call(o,r.resolve,r.reject):r.resolve(o))}catch(e){r.reject(e)}}function o(r){var a,s=this;if(!s.triggered){s.triggered=!0,s.def&&(s=s.def);try{(a=t(r))?e(function(){var e=new u(s);try{a.call(r,function(){o.apply(e,arguments)},function(){i.apply(e,arguments)})}catch(t){i.call(e,t)}}):(s.msg=r,s.state=1,s.chain.length>0&&e(n,s))}catch(e){i.call(new u(s),e)}}}function i(t){var r=this;r.triggered||(r.triggered=!0,r.def&&(r=r.def),r.msg=t,r.state=2,r.chain.length>0&&e(n,r))}function a(e,t,n,r){for(var o=0;o<t.length;o++)!function(o){e.resolve(t[o]).then(function(e){n(o,e)},r)}(o)}function u(e){this.def=e,this.triggered=!1}function s(e){this.promise=e,this.state=0,this.triggered=!1,this.chain=[],this.msg=void 0}function c(t){if("function"!=typeof t)throw TypeError("Not a function");if(0!==this.__NPO__)throw TypeError("Not a promise");this.__NPO__=1;var r=new s(this);this.then=function(t,o){var i={success:"function"!=typeof t||t,failure:"function"==typeof o&&o};return i.promise=new this.constructor(function(e,t){if("function"!=typeof e||"function"!=typeof t)throw TypeError("Not a function");i.resolve=e,i.reject=t}),r.chain.push(i),0!==r.state&&e(n,r),i.promise},this.catch=function(e){return this.then(void 0,e)};try{t.call(void 0,function(e){o.call(r,e)},function(e){i.call(r,e)})}catch(e){i.call(r,e)}}var f,l,h,d=Object.prototype.toString,p="undefined"!=typeof setImmediate?function(e){return setImmediate(e)}:setTimeout;try{Object.defineProperty({},"x",{}),f=function(e,t,n,r){return Object.defineProperty(e,t,{value:n,writable:!0,configurable:r!==!1})}}catch(e){f=function(e,t,n){return e[t]=n,e}}h=function(){function e(e,t){this.fn=e,this.self=t,this.next=void 0}var t,n,r;return{add:function(o,i){r=new e(o,i),n?n.next=r:t=r,n=r,r=void 0},drain:function(){var e=t;for(t=n=l=void 0;e;)e.fn.call(e.self),e=e.next}}}();var v=f({},"constructor",c,!1);return c.prototype=v,f(v,"__NPO__",0,!1),f(c,"resolve",function(e){var t=this;return e&&"object"==typeof e&&1===e.__NPO__?e:new t(function(t,n){if("function"!=typeof t||"function"!=typeof n)throw TypeError("Not a function");t(e)})}),f(c,"reject",function(e){return new this(function(t,n){if("function"!=typeof t||"function"!=typeof n)throw TypeError("Not a function");n(e)})}),f(c,"all",function(e){var t=this;return"[object Array]"!=d.call(e)?t.reject(TypeError("Not an array")):0===e.length?t.resolve([]):new t(function(n,r){if("function"!=typeof n||"function"!=typeof r)throw TypeError("Not a function");var o=e.length,i=Array(o),u=0;a(t,e,function(e,t){i[e]=t,++u===o&&n(i)},r)})}),f(c,"race",function(e){var t=this;return"[object Array]"!=d.call(e)?t.reject(TypeError("Not an array")):new t(function(n,r){if("function"!=typeof n||"function"!=typeof r)throw TypeError("Not a function");a(t,e,function(e,t){n(t)},r)})}),c})})),E=b&&"object"==typeof b&&"default"in b?b.default:b,T=new WeakMap,_=["id","url","width","maxwidth","height","maxheight","portrait","title","byline","color","autoplay","autopause","loop","responsive"],k=function(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")},x=new WeakMap,j=new WeakMap,Player=function(){function Player(e){var t=this,n=arguments.length<=1||void 0===arguments[1]?{}:arguments[1];if(k(this,Player),__webpack_provided_window_dot_jQuery&&e instanceof jQuery&&(e.length>1&&window.console&&console.warn&&console.warn("A jQuery object with multiple elements was passed, using the first element."),e=e[0]),"string"==typeof e&&(e=document.getElementById(e)),!a(e))throw new TypeError("You must pass either a valid element or a valid id.");if("IFRAME"!==e.nodeName){var r=e.querySelector("iframe");r&&(e=r)}if("IFRAME"===e.nodeName&&!s(e.getAttribute("src")||""))throw new Error("The player element passed isn’t a Vimeo embed.");if(x.has(e))return x.get(e);this.element=e,this.origin="*";var i=new E(function(r,i){var a=function(e){if(s(e.origin)&&t.element.contentWindow===e.source){"*"===t.origin&&(t.origin=e.origin);var n=p(e.data),o="event"in n&&"ready"===n.event,i="method"in n&&"ping"===n.method;return o||i?(t.element.setAttribute("data-ready","true"),void r()):void y(t,n)}};if(window.addEventListener?window.addEventListener("message",a,!1):window.attachEvent&&window.attachEvent("onmessage",a),"IFRAME"!==t.element.nodeName){var u=f(e,n),d=c(u);l(d,u).then(function(n){var r=h(n,e);return t.element=r,o(e,r),n}).catch(function(e){return i(e)})}});return j.set(this,i),x.set(this.element,this),"IFRAME"===this.element.nodeName&&v(this,"ping"),this}return Player.prototype.then=function(e){var t=arguments.length<=1||void 0===arguments[1]?function(){}:arguments[1];return this.ready().then(e,t)},Player.prototype.callMethod=function(e){var n=this,r=arguments.length<=1||void 0===arguments[1]?{}:arguments[1];return new E(function(o,i){return n.ready().then(function(){t(n,e,{resolve:o,reject:i}),v(n,e,r)})})},Player.prototype.get=function(e){var n=this;return new E(function(r,o){return e=i(e,"get"),n.ready().then(function(){t(n,e,{resolve:r,reject:o}),v(n,e)})})},Player.prototype.set=function(e,n){var r=this;return E.resolve(n).then(function(n){if(e=i(e,"set"),void 0===n||null===n)throw new TypeError("There must be a value to set.");return r.ready().then(function(){return new E(function(o,i){t(r,e,{resolve:o,reject:i}),v(r,e,n)})})})},Player.prototype.on=function(e,r){if(!e)throw new TypeError("You must pass an event name.");if(!r)throw new TypeError("You must pass a callback function.");if("function"!=typeof r)throw new TypeError("The callback must be a function.");var o=n(this,"event:"+e);0===o.length&&this.callMethod("addEventListener",e).catch(function(){}),t(this,"event:"+e,r)},Player.prototype.off=function(e,t){if(!e)throw new TypeError("You must pass an event name.");if(t&&"function"!=typeof t)throw new TypeError("The callback must be a function.");var n=r(this,"event:"+e,t);n&&this.callMethod("removeEventListener",e).catch(function(e){})},Player.prototype.loadVideo=function(e){return this.callMethod("loadVideo",e)},Player.prototype.ready=function(){var e=j.get(this);return E.resolve(e)},Player.prototype.enableTextTrack=function(e,t){if(!e)throw new TypeError("You must pass a language.");return this.callMethod("enableTextTrack",{language:e,kind:t})},Player.prototype.disableTextTrack=function(){return this.callMethod("disableTextTrack")},Player.prototype.pause=function(){return this.callMethod("pause")},Player.prototype.play=function(){return this.callMethod("play")},Player.prototype.unload=function(){return this.callMethod("unload")},Player.prototype.getAutopause=function(){return this.get("autopause")},Player.prototype.setAutopause=function(e){return this.set("autopause",e)},Player.prototype.getColor=function(){return this.get("color")},Player.prototype.setColor=function(e){return this.set("color",e)},Player.prototype.getCurrentTime=function(){return this.get("currentTime")},Player.prototype.setCurrentTime=function(e){return this.set("currentTime",e)},Player.prototype.getDuration=function(){return this.get("duration")},Player.prototype.getEnded=function(){return this.get("ended")},Player.prototype.getLoop=function(){return this.get("loop")},Player.prototype.setLoop=function(e){return this.set("loop",e)},Player.prototype.getPaused=function(){return this.get("paused")},Player.prototype.getTextTracks=function(){return this.get("textTracks")},Player.prototype.getVideoEmbedCode=function(){return this.get("videoEmbedCode")},Player.prototype.getVideoId=function(){return this.get("videoId")},Player.prototype.getVideoTitle=function(){return this.get("videoTitle")},Player.prototype.getVideoWidth=function(){return this.get("videoWidth")},Player.prototype.getVideoHeight=function(){return this.get("videoHeight")},Player.prototype.getVideoUrl=function(){return this.get("videoUrl")},Player.prototype.getVolume=function(){return this.get("volume")},Player.prototype.setVolume=function(e){return this.set("volume",e)},Player}();return d(),Player});
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }()), __webpack_require__(17).setImmediate, __webpack_require__(5), __webpack_require__(5)))

/***/ },
/* 17 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(setImmediate, clearImmediate) {"use strict";
	
	var nextTick = __webpack_require__(18).nextTick;
	var apply = Function.prototype.apply;
	var slice = Array.prototype.slice;
	var immediateIds = {};
	var nextImmediateId = 0;
	
	// DOM APIs, for completeness
	
	exports.setTimeout = function () {
	  return new Timeout(apply.call(setTimeout, window, arguments), clearTimeout);
	};
	exports.setInterval = function () {
	  return new Timeout(apply.call(setInterval, window, arguments), clearInterval);
	};
	exports.clearTimeout = exports.clearInterval = function (timeout) {
	  timeout.close();
	};
	
	function Timeout(id, clearFn) {
	  this._id = id;
	  this._clearFn = clearFn;
	}
	Timeout.prototype.unref = Timeout.prototype.ref = function () {};
	Timeout.prototype.close = function () {
	  this._clearFn.call(window, this._id);
	};
	
	// Does not start the time, just sets up the members needed.
	exports.enroll = function (item, msecs) {
	  clearTimeout(item._idleTimeoutId);
	  item._idleTimeout = msecs;
	};
	
	exports.unenroll = function (item) {
	  clearTimeout(item._idleTimeoutId);
	  item._idleTimeout = -1;
	};
	
	exports._unrefActive = exports.active = function (item) {
	  clearTimeout(item._idleTimeoutId);
	
	  var msecs = item._idleTimeout;
	  if (msecs >= 0) {
	    item._idleTimeoutId = setTimeout(function onTimeout() {
	      if (item._onTimeout) item._onTimeout();
	    }, msecs);
	  }
	};
	
	// That's not how node.js implements it but the exposed api is the same.
	exports.setImmediate = typeof setImmediate === "function" ? setImmediate : function (fn) {
	  var id = nextImmediateId++;
	  var args = arguments.length < 2 ? false : slice.call(arguments, 1);
	
	  immediateIds[id] = true;
	
	  nextTick(function onNextTick() {
	    if (immediateIds[id]) {
	      // fn.call() is faster so we optimize for the common use-case
	      // @see http://jsperf.com/call-apply-segu
	      if (args) {
	        fn.apply(null, args);
	      } else {
	        fn.call(null);
	      }
	      // Prevent ids from leaking
	      exports.clearImmediate(id);
	    }
	  });
	
	  return id;
	};
	
	exports.clearImmediate = typeof clearImmediate === "function" ? clearImmediate : function (id) {
	  delete immediateIds[id];
	};
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(17).setImmediate, __webpack_require__(17).clearImmediate))

/***/ },
/* 18 */
/***/ function(module, exports) {

	'use strict';
	
	// shim for using process in browser
	var process = module.exports = {};
	
	// cached from whatever global is present so that test runners that stub it
	// don't break things.  But we need to wrap it in a try catch in case it is
	// wrapped in strict mode code which doesn't define any globals.  It's inside a
	// function because try/catches deoptimize in certain engines.
	
	var cachedSetTimeout;
	var cachedClearTimeout;
	
	function defaultSetTimout() {
	    throw new Error('setTimeout has not been defined');
	}
	function defaultClearTimeout() {
	    throw new Error('clearTimeout has not been defined');
	}
	(function () {
	    try {
	        if (typeof setTimeout === 'function') {
	            cachedSetTimeout = setTimeout;
	        } else {
	            cachedSetTimeout = defaultSetTimout;
	        }
	    } catch (e) {
	        cachedSetTimeout = defaultSetTimout;
	    }
	    try {
	        if (typeof clearTimeout === 'function') {
	            cachedClearTimeout = clearTimeout;
	        } else {
	            cachedClearTimeout = defaultClearTimeout;
	        }
	    } catch (e) {
	        cachedClearTimeout = defaultClearTimeout;
	    }
	})();
	function runTimeout(fun) {
	    if (cachedSetTimeout === setTimeout) {
	        //normal enviroments in sane situations
	        return setTimeout(fun, 0);
	    }
	    // if setTimeout wasn't available but was latter defined
	    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
	        cachedSetTimeout = setTimeout;
	        return setTimeout(fun, 0);
	    }
	    try {
	        // when when somebody has screwed with setTimeout but no I.E. maddness
	        return cachedSetTimeout(fun, 0);
	    } catch (e) {
	        try {
	            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
	            return cachedSetTimeout.call(null, fun, 0);
	        } catch (e) {
	            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
	            return cachedSetTimeout.call(this, fun, 0);
	        }
	    }
	}
	function runClearTimeout(marker) {
	    if (cachedClearTimeout === clearTimeout) {
	        //normal enviroments in sane situations
	        return clearTimeout(marker);
	    }
	    // if clearTimeout wasn't available but was latter defined
	    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
	        cachedClearTimeout = clearTimeout;
	        return clearTimeout(marker);
	    }
	    try {
	        // when when somebody has screwed with setTimeout but no I.E. maddness
	        return cachedClearTimeout(marker);
	    } catch (e) {
	        try {
	            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
	            return cachedClearTimeout.call(null, marker);
	        } catch (e) {
	            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
	            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
	            return cachedClearTimeout.call(this, marker);
	        }
	    }
	}
	var queue = [];
	var draining = false;
	var currentQueue;
	var queueIndex = -1;
	
	function cleanUpNextTick() {
	    if (!draining || !currentQueue) {
	        return;
	    }
	    draining = false;
	    if (currentQueue.length) {
	        queue = currentQueue.concat(queue);
	    } else {
	        queueIndex = -1;
	    }
	    if (queue.length) {
	        drainQueue();
	    }
	}
	
	function drainQueue() {
	    if (draining) {
	        return;
	    }
	    var timeout = runTimeout(cleanUpNextTick);
	    draining = true;
	
	    var len = queue.length;
	    while (len) {
	        currentQueue = queue;
	        queue = [];
	        while (++queueIndex < len) {
	            if (currentQueue) {
	                currentQueue[queueIndex].run();
	            }
	        }
	        queueIndex = -1;
	        len = queue.length;
	    }
	    currentQueue = null;
	    draining = false;
	    runClearTimeout(timeout);
	}
	
	process.nextTick = function (fun) {
	    var args = new Array(arguments.length - 1);
	    if (arguments.length > 1) {
	        for (var i = 1; i < arguments.length; i++) {
	            args[i - 1] = arguments[i];
	        }
	    }
	    queue.push(new Item(fun, args));
	    if (queue.length === 1 && !draining) {
	        runTimeout(drainQueue);
	    }
	};
	
	// v8 likes predictible objects
	function Item(fun, array) {
	    this.fun = fun;
	    this.array = array;
	}
	Item.prototype.run = function () {
	    this.fun.apply(null, this.array);
	};
	process.title = 'browser';
	process.browser = true;
	process.env = {};
	process.argv = [];
	process.version = ''; // empty string to avoid regexp issues
	process.versions = {};
	
	function noop() {}
	
	process.on = noop;
	process.addListener = noop;
	process.once = noop;
	process.off = noop;
	process.removeListener = noop;
	process.removeAllListeners = noop;
	process.emit = noop;
	process.prependListener = noop;
	process.prependOnceListener = noop;
	
	process.listeners = function (name) {
	    return [];
	};
	
	process.binding = function (name) {
	    throw new Error('process.binding is not supported');
	};
	
	process.cwd = function () {
	    return '/';
	};
	process.chdir = function (dir) {
	    throw new Error('process.chdir is not supported');
	};
	process.umask = function () {
	    return 0;
	};

/***/ },
/* 19 */
/***/ function(module, exports) {

	/* WEBPACK VAR INJECTION */(function(__webpack_amd_options__) {module.exports = __webpack_amd_options__;
	
	/* WEBPACK VAR INJECTION */}.call(exports, {}))

/***/ },
/* 20 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _jquery = __webpack_require__(5);
	
	var _jquery2 = _interopRequireDefault(_jquery);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var YOUTUBE_PLAYER_ID = 'jdbga-youtube-video-player';
	
	(function () {
	  // jshint ignore: line
	  'use strict';
	
	  var youtube = null;
	
	  var player = {
	    type: 'youtube',
	    movieIdRegex: /youtu.*(?:v=|\/)([\w-]+)/i
	  };
	
	  player._createYoutubeApiPlayer = function (playerId, config) {
	    player.getYT().then(function (YT) {
	      YT.ready(function () {
	        new YT.Player(playerId, config); //jshint ignore:line
	      });
	    });
	  };
	
	  player.mute = function (cb) {
	    youtube.mute();
	    cb();
	  };
	
	  player.play = function () {
	    youtube.playVideo();
	  };
	
	  player.seekTo = function (seekPosition, cb) {
	    youtube.seekTo(seekPosition || 0, true);
	    cb();
	  };
	
	  player.init = function (config) {
	    //refs #103893 - Script is needed to repair some weird loading behaviour for safari
	    _jquery2.default.getScript('https://www.youtube.com/iframe_api');
	
	    var playerId = YOUTUBE_PLAYER_ID + '_' + new Date().getTime();
	
	    var iframe = '<iframe id="' + playerId + '" +' + 'type="text/html" ' + 'src="https://www.youtube-nocookie.com/embed/' + config.movieId + "?enablejsapi=1&version=3&autoplay=1&loop=1&mute=1&playlist=" + config.movieId + '"' + 'frameborder="0"></iframe>';
	
	    config.$playerWrapper.append(iframe);
	
	    player._createYoutubeApiPlayer(playerId, {
	      playerVars: {
	        // this is needed for proper looping, because loop always references a playlist
	        'playlist': config.movieId,
	        'loop': 1,
	        'autoplay': 1,
	        'controls': 0,
	        'modestbranding': 1,
	        'showinfo': 0,
	        'enablejsapi': 1,
	        // disable annotations
	        'iv_load_policy': 3,
	        // force usage of html5 player
	        'html5': 1,
	        // ... but if flash is used, respect the z-index (i.e. background images)
	        'wmode': 'transparent',
	        'mute': 1,
	        'version': 3
	      },
	      events: {
	        onReady: function onReady(ev) {
	          youtube = ev.target;
	          if (typeof config.onReady === 'function') {
	            config.onReady(config.$playerWrapper, (0, _jquery2.default)(youtube.getIframe()));
	          }
	        }
	      }
	    });
	  };
	
	  player.getSeekPosition = function (callback) {
	    callback(youtube.getCurrentTime());
	  };
	
	  player.getYT = function () {
	    var d = _jquery2.default.Deferred();
	
	    if (!!window.YT) {
	      d.resolve(window.YT);
	    } else {
	      d.reject(new Error('Youtube api script is not loaded'));
	    }
	
	    return d.promise();
	  };
	
	  player.getPreviewImageUrl = function (movieId) {
	    var d = _jquery2.default.Deferred();
	    d.resolve('https://img.youtube.com/vi/' + movieId + '/sddefault.jpg');
	
	    return d.promise();
	  };
	
	  _jquery2.default.backgroundArea.videoPlayers[player.type] = player;
	})();

/***/ },
/* 21 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _jquery = __webpack_require__(5);
	
	var _jquery2 = _interopRequireDefault(_jquery);
	
	var _BackgroundArea = __webpack_require__(22);
	
	var _BackgroundArea2 = _interopRequireDefault(_BackgroundArea);
	
	var _constants = __webpack_require__(10);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	_jquery2.default.fn[_constants.PLUGIN_NAME] = function (options) {
	  'use strict';
	
	  var CLASS_CONTAINER = 'jqbga-container';
	  var $els = this;
	
	  /* Method call: */
	  if (typeof options === 'string') {
	    var methodName = options;
	    var originalMethodName = methodName;
	    var args = Array.prototype.slice.call(arguments, 1);
	    var methodTokens = methodName.split('.');
	
	    $els.each(function () {
	      var instance = _jquery2.default.data(this, _constants.PLUGIN_DATA_ATTR);
	      var target = instance;
	
	      if (!instance) {
	        throw new Error('Cannot call methods on ' + _constants.PLUGIN_NAME + ' prior to initialization; attempted to call method "' + methodName + '".');
	      }
	
	      if (methodTokens.length > 1) {
	        target = _jquery2.default.backgroundArea.types[methodTokens[0]];
	        methodName = methodTokens[1];
	      }
	
	      if (methodName.charAt(0) === '_' || !_jquery2.default.isFunction(target[methodName])) {
	        throw new Error('No such method "' + originalMethodName + '" for "' + _constants.PLUGIN_NAME + '" instance.');
	      } else {
	        target[methodName].apply(instance, args);
	      }
	    });
	    /* Instantiation: */
	  } else {
	    $els.each(function () {
	      var $el = (0, _jquery2.default)(this);
	      var instance = $el.data(_constants.PLUGIN_DATA_ATTR);
	      if (instance) {
	        throw new Error('already instantiated "' + _constants.PLUGIN_NAME + '" on element.');
	      } else {
	        instance = new _BackgroundArea2.default($el, options);
	        instance.destroy = function () {
	          $el.trigger('destroy' + _jquery2.default.backgroundArea.CONSTANTS.EVENT_NAMESPACE);
	          delete $el.data()[_constants.PLUGIN_DATA_ATTR];
	        };
	        $el.addClass(CLASS_CONTAINER);
	        $el.data(_constants.PLUGIN_DATA_ATTR, instance);
	      }
	    });
	  }
	
	  return $els;
	};

/***/ },
/* 22 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = BackgroundArea;
	
	var _jquery = __webpack_require__(5);
	
	var _jquery2 = _interopRequireDefault(_jquery);
	
	__webpack_require__(4);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var DEFAULTS = {};
	
	function BackgroundArea($el, options) {
	  var self = this;
	
	  self.$el = $el;
	  self.options = _jquery2.default.extend({}, DEFAULTS, options);
	  self.currentBackground = null;
	
	  self.$el.on('destroy' + _jquery2.default.backgroundArea.CONSTANTS.EVENT_NAMESPACE, function () {
	    self.clear();
	  });
	}
	
	BackgroundArea.prototype.show = function (background) {
	  var self = this;
	
	  self.clear();
	
	  if (background) {
	    _jquery2.default.backgroundArea.types[background.type].init(self.$el, background, self.options);
	
	    self.currentBackground = background;
	  }
	};
	
	BackgroundArea.prototype.clear = function () {
	  var self = this;
	
	  if (self.currentBackground) {
	    _jquery2.default.backgroundArea.types[self.currentBackground.type].destroy(self.$el);
	
	    self.currentBackground = null;
	  }
	};

/***/ }
/******/ ])
});
;
//# sourceMappingURL=jquery-background-area.js.map


/*****************
 ** WEBPACK FOOTER
 ** ./~/@jimdo/jquery-background-area/jquery-background-area.js
 ** module id = 698
 ** module chunks = 96 232 233 234
 **/