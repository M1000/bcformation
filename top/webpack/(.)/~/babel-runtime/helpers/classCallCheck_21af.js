"use strict";

exports.__esModule = true;

exports.default = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};


/*****************
 ** WEBPACK FOOTER
 ** ./~/babel-runtime/helpers/classCallCheck.js
 ** module id = 8
 ** module chunks = 96 97 98 227 229 230 232 233 234 235
 **/