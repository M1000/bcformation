// 19.1.2.9 Object.getPrototypeOf(O)
var toObject = require('./_to-object');
var $getPrototypeOf = require('./_object-gpo');

require('./_object-sap')('getPrototypeOf', function () {
  return function getPrototypeOf(it) {
    return $getPrototypeOf(toObject(it));
  };
});



/*****************
 ** WEBPACK FOOTER
 ** ./~/babel-runtime/~/core-js/library/modules/es6.object.get-prototype-of.js
 ** module id = 496
 ** module chunks = 98 228 229 230
 **/