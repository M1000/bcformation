// 19.1.3.19 Object.setPrototypeOf(O, proto)
var $export = require('./_export');
$export($export.S, 'Object', { setPrototypeOf: require('./_set-proto').set });



/*****************
 ** WEBPACK FOOTER
 ** ./~/babel-runtime/~/core-js/library/modules/es6.object.set-prototype-of.js
 ** module id = 497
 ** module chunks = 98 228 229 230
 **/