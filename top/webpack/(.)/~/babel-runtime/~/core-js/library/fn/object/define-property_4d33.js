require('../../modules/es6.object.define-property');
var $Object = require('../../modules/_core').Object;
module.exports = function defineProperty(it, key, desc) {
  return $Object.defineProperty(it, key, desc);
};



/*****************
 ** WEBPACK FOOTER
 ** ./~/babel-runtime/~/core-js/library/fn/object/define-property.js
 ** module id = 253
 ** module chunks = 96 97 98 227 229 230 232 233 234 235
 **/