var $export = require('./_export');
// 19.1.2.4 / 15.2.3.6 Object.defineProperty(O, P, Attributes)
$export($export.S + $export.F * !require('./_descriptors'), 'Object', { defineProperty: require('./_object-dp').f });



/*****************
 ** WEBPACK FOOTER
 ** ./~/babel-runtime/~/core-js/library/modules/es6.object.define-property.js
 ** module id = 303
 ** module chunks = 96 97 98 228 229 230
 **/