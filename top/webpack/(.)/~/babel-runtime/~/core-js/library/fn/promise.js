require('../modules/es6.object.to-string');
require('../modules/es6.string.iterator');
require('../modules/web.dom.iterable');
require('../modules/es6.promise');
require('../modules/es7.promise.finally');
require('../modules/es7.promise.try');
module.exports = require('../modules/_core').Promise;



/*****************
 ** WEBPACK FOOTER
 ** ./~/babel-runtime/~/core-js/library/fn/promise.js
 ** module id = 284
 ** module chunks = 96 97 227 229 230 232 233 234 235
 **/