module.exports = function (exec) {
  try {
    return { e: false, v: exec() };
  } catch (e) {
    return { e: true, v: e };
  }
};



/*****************
 ** WEBPACK FOOTER
 ** ./~/babel-runtime/~/core-js/library/modules/_perform.js
 ** module id = 462
 ** module chunks = 96 97 228 229 230
 **/