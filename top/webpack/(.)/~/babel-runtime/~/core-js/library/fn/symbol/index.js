require('../../modules/es6.symbol');
require('../../modules/es6.object.to-string');
require('../../modules/es7.symbol.async-iterator');
require('../../modules/es7.symbol.observable');
module.exports = require('../../modules/_core').Symbol;



/*****************
 ** WEBPACK FOOTER
 ** ./~/babel-runtime/~/core-js/library/fn/symbol/index.js
 ** module id = 321
 ** module chunks = 98 227 229 230 232 233 234 235
 **/