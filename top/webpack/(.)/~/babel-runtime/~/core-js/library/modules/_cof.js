var toString = {}.toString;

module.exports = function (it) {
  return toString.call(it).slice(8, -1);
};



/*****************
 ** WEBPACK FOOTER
 ** ./~/babel-runtime/~/core-js/library/modules/_cof.js
 ** module id = 118
 ** module chunks = 96 97 98 228 229 230
 **/