require('../../modules/es6.object.create');
var $Object = require('../../modules/_core').Object;
module.exports = function create(P, D) {
  return $Object.create(P, D);
};



/*****************
 ** WEBPACK FOOTER
 ** ./~/babel-runtime/~/core-js/library/fn/object/create.js
 ** module id = 318
 ** module chunks = 98 227 229 230 232 233 234 235
 **/