(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else {
		var a = factory();
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _targetBackgroundAreas = __webpack_require__(1);
	
	var _targetBackgroundAreas2 = _interopRequireDefault(_targetBackgroundAreas);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	exports.default = _targetBackgroundAreas2.default;

/***/ },
/* 1 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.default = targetBackgroundAreas;
	function targetBackgroundAreas(document) {
	    var supported = [];
	    var backgroundAreas = document.querySelectorAll('[background-area]');
	
	    for (var i = 0, l = backgroundAreas.length; i < l; i++) {
	        var node = backgroundAreas[i];
	
	        if (node && node.attributes['background-area'] && node.attributes['background-area'].value.length) {
	            var backgroundArea = {
	                type: node.attributes['background-area'].value,
	                node: node
	            };
	
	            if (node.attributes['background-area-default']) {
	                backgroundArea.default = true;
	            }
	
	            supported.push(backgroundArea);
	        }
	    }
	
	    if (supported.length === 0 && document.querySelector('[background-area]')) {
	        supported.push({ type: undefined, node: document.querySelector('[background-area]') });
	    }
	
	    if (supported.length === 0) {
	        supported.push({ type: undefined, node: document.querySelector('body') });
	    }
	
	    return supported.sort(function (a, b) {
	        if (a.default) {
	            return -1;
	        } else if (b.default) {
	            return 1;
	        }
	
	        return 0;
	    });
	}

/***/ }
/******/ ])
});
;
//# sourceMappingURL=index.js.map


/*****************
 ** WEBPACK FOOTER
 ** ./~/target-background-areas/index.js
 ** module id = 1297
 ** module chunks = 96 234
 **/